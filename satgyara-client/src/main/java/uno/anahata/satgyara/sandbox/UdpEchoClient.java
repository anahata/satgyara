package uno.anahata.satgyara.sandbox;

import java.net.*;
import java.util.Random;

public class UdpEchoClient {

    public static void main(String args[]) throws Exception {
        DatagramSocket clientSocket = new DatagramSocket();
        System.out.println(clientSocket.supportedOptions());
        //InetAddress IPAddress = InetAddress.getByName("27.34.90.99");
        InetAddress IPAddress = InetAddress.getByName("213.60.215.6");
        byte[] sendData = new byte[50 * 1024];
        Random r = new Random();
        r.nextBytes(sendData);
        
        int payloadLength = 100;
        while (true) {
            payloadLength += 100;
            
            DatagramPacket sendPacket = new DatagramPacket(sendData, payloadLength, IPAddress, 11711);
            System.out.println("Sending " + payloadLength);
            clientSocket.send(sendPacket);
            System.out.println("Sent " + payloadLength);

            DatagramPacket receivePacket = new DatagramPacket(sendData, sendData.length);
            clientSocket.setSoTimeout(5000);
            try {
                clientSocket.receive(receivePacket);
                System.out.println(" FROM SERVER:" + receivePacket.getLength() + " addr=" + receivePacket.getSocketAddress());
            } catch (Exception e) {
                System.out.println("TIMEOUT A");
            }
            
            try {
                clientSocket.receive(receivePacket);
                System.out.println(" FROM SERVER:" + receivePacket.getLength() + " addr=" + receivePacket.getSocketAddress());
            } catch (Exception e) {
                System.out.println("TIMEOUT B");
            }
        }

        //clientSocket.close();
    }
}
