package uno.anahata.satgyara.sandbox;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;

/**
 *
 * @author priyadarshi
 */
public class Repeater  {

    private DatagramSocket socket;
    private SocketAddress sa1;
    private SocketAddress sa2;
    
    private byte[] buf = new byte[64000];

    public Repeater() throws Exception {
        System.out.println("Creating datagram socket on 711");
        socket = new DatagramSocket(711);
         DatagramPacket packet1 = new DatagramPacket(buf, buf.length);
        socket.receive(packet1);
        sa1 = packet1.getSocketAddress();

        System.out.println("Received Packet 1 from " + sa1);

        DatagramPacket packet2 = new DatagramPacket(buf, buf.length);
        socket.receive(packet2);
        sa2 = packet2.getSocketAddress();

        System.out.println("Received Packet 2 from " + sa2);
        run();
    }

    public void run() {
       

        while (true) {

            try {

                DatagramPacket receivedPacket = new DatagramPacket(buf, buf.length);
                socket.receive(receivedPacket);
                System.out.println("Received payload from " + receivedPacket.getSocketAddress());

                SocketAddress target;

                if (receivedPacket.getSocketAddress().equals(sa1)) {
                    target = sa2;
                } else if (receivedPacket.getSocketAddress().equals(sa2)) {
                    target = sa1;
                } else {
                    System.out.println("Received packet from unknow address " + receivedPacket.getSocketAddress());
                    continue;
                }

                System.out.println("Sending packet to " + target);

                DatagramPacket deliveryPacket = new DatagramPacket(buf, buf.length, target);
                socket.send(deliveryPacket);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
