package uno.anahata.satgyara.sandbox;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author priyadarshi
 */
public class UdpHolePunchClient {

    private static byte[] buff = new byte[64000];

    public static void main(String[] args) throws Exception {

        DatagramSocket socket = new DatagramSocket();
        System.out.println("Created local socket " + socket);
        DatagramPacket dp = new DatagramPacket(buff, 0, new InetSocketAddress("p.anahata.uno", 11711));
        socket.send(dp);
        dp = new DatagramPacket(buff, buff.length);
        socket.receive(dp);
        System.out.println("Got response from server: " + dp.getSocketAddress() + " " + dp.getLength());
        ByteBuffer bb = ByteBuffer.wrap(buff, 0, dp.getLength());
        int port = bb.getInt();
        byte[] addr = new byte[4];
        bb.get(addr);
        InetSocketAddress otherHostSA = new InetSocketAddress(Inet4Address.getByAddress(addr), port);

        DatagramSocket newSocket = socket; //new DatagramSocket();

        Thread t = new Thread() {
            public void run() {
                System.out.println("Listening on " + newSocket);
                try {
                    DatagramPacket p = new DatagramPacket(buff, buff.length);
                    newSocket.receive(p);
                    System.out.println("Got " + new String(p.getData(), 0, p.getLength()) + " from " + p.getSocketAddress());
                } catch (Exception e) {
                    System.out.println(e);
                }

            }
        };
        t.start();
        
        
        while (true) {
            String payload = new Date().toString();
            DatagramPacket p = new DatagramPacket(payload.getBytes(), payload.getBytes().length, otherHostSA);
            System.out.println("Sending " + payload + " to " + p.getSocketAddress() + " from " + newSocket);
            newSocket.send(p);
            System.out.println("SENT " + payload + " to " + p.getSocketAddress() + " from " + newSocket);
            //newSocket.setSoTimeout(1000);
        }

    }
}
