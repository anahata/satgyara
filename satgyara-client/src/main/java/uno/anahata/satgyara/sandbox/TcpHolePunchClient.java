package uno.anahata.satgyara.sandbox;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author priyadarshi
 */
public class TcpHolePunchClient {
    
    private static InetSocketAddress toSocketAddress(byte[] barr) throws Exception {
        ByteBuffer bb = ByteBuffer.wrap(barr);
        int port = bb.getInt();
        
        byte[] ip = new byte[4];
        bb.get(ip);
        
        return new InetSocketAddress(Inet4Address.getByAddress(ip), port);
    }
    
    SocketAddress prana = new InetSocketAddress("p.anahata.uno", 11711);
    
    public static void main(String[] args) throws Exception {
        Thread t = new Thread() {
            public void run() {
                try {
                    launch1();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        //t.start();
        launch2();
        
    }
    
    public static void launch2() throws Exception {
        
        SocketAddress sa = new InetSocketAddress("0.0.0.0", 11711);
        
        Socket s = new Socket();        
        s.setReuseAddress(true);
        s.bind(sa);
        s.connect(new InetSocketAddress("p.anahata.uno", 11711));
        System.out.println("launch2 Connected to prana from " + sa);
        byte[] s2addr = s.getInputStream().readNBytes(8);
        
        System.out.println(sa + " got address from prana " + Arrays.toString(s2addr));
        s.close();
        
        Thread.sleep(4000);
        
        System.out.println("launch 2 connecting to launch1 on " + toSocketAddress(s2addr));
        s = new Socket();        
        s.setReuseAddress(true);
        s.bind(sa);
        s.connect(toSocketAddress(s2addr));
        System.out.println("Connected to " + toSocketAddress(s2addr));
        
    }
    
    public static void launch1() throws Exception {
        
        SocketAddress sa = new InetSocketAddress("localhost", 11711);
        
        Socket s = new Socket();        
        s.setReuseAddress(true);
        s.bind(sa);
        s.connect(new InetSocketAddress("p.anahata.uno", 11711));
        System.out.println("launch1 Connected to prana from " + sa);
        byte[] s2addr = new byte[8];
        s.getInputStream().read(s2addr);
        System.out.println(sa + " got address from prana " + Arrays.toString(s2addr));
        s.close();
        
        System.out.println("Binding server socket");
        ServerSocket ss = new ServerSocket();
        ss.setReuseAddress(true);
        ss.bind(sa);
        s = ss.accept();
        System.out.println("Received connection from " + s);
    }
    
    
}
