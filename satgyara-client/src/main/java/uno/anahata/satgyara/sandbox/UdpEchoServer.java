package uno.anahata.satgyara.sandbox;

import java.net.*;
import java.util.Random;

class UdpEchoServer {

    public static void main(String args[]) throws Exception {
        DatagramSocket serverSocket = new DatagramSocket(11711);
        byte[] receiveData = new byte[50 * 1024];
        //byte[] sendData = new byte[50 * 1024];
        while (true) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            System.out.println("Listening on " + serverSocket.getLocalSocketAddress());
            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData(), 0 , receivePacket.getLength());
            System.out.println("RECEIVED: " + receiveData.length + " from " + receivePacket.getSocketAddress());
            InetAddress sourceIp = receivePacket.getAddress();
            int sourcePort = receivePacket.getPort();
            
//            Random r = new Random();
//            byte[] payload = new byte[512];
//            r.nextBytes(payload);
            //for (int i = 0; i < 100; i++) {
                //String capitalizedSentence = "Reply 1 " + sentence.toUpperCase() + i++;
            
            DatagramPacket sendPacket
                    = new DatagramPacket(receivePacket.getData(), 0, receivePacket.getLength(), sourceIp, sourcePort);
            serverSocket.send(sendPacket);
            System.out.println("Replied : " + receivePacket.getLength() + "b to" + receivePacket.getSocketAddress() + " ");
                
                //Thread.sleep(1);
            //}
        }
    }
}
