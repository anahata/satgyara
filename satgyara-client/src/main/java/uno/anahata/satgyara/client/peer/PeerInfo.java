/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.client.peer;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.seq.UUIDUtils;

/**
 *
 * @author priyadarshi
 */
public class PeerInfo extends VBox {

    private final RemotePeer peer;
    private Label uuid = new Label();    
    private Label type = new Label();
    private Label rssa = new Label();    
    private Label transports = new Label();
    private Label ping = new Label();
    private Label inboundPeers = new Label();

    public PeerInfo(RemotePeer peer) {
        this.peer = peer;
        init();
        update();
    }

    public final void init() {
        getChildren().add(uuid);
        getChildren().add(type);
        getChildren().add(rssa);
        getChildren().add(transports);
        getChildren().add(ping);
    }

    public final void update() {
        String uuidStr = peer.getUuid() != null ? UUIDUtils.tail(peer.getUuid()) : "<waiting>";
        uuid.setText("UUID: " + uuidStr);
        type.setText("Type: " + peer.getClass().getSimpleName());
        String rssaStr = peer.getTcpServerSocketAddress() != null ? peer.getTcpServerSocketAddress().toString() : "<unknwon>";        
        rssa.setText("Adddress: " + rssaStr);
        if (peer.getPingClient() != null) {
            ping.setText("Ping: " + peer.getPingClient().getTotal());
        }
        transports.setText("Transports: " + peer.getTransports().size());
        //connections.setText("Connections: " + peer.getConnections().size());
        //ping.setText("Ping: " + peer.getPing());

//        sendQueue.setText("Send queue: " + peer.getSendQueue().size());
//        outgoingSeqNo.setText("Outgoing Seq No: " + peer.getOutgoingSeqNo());
//        serializeQueue.setText("Serialize Queue: " + peer.getSerializeQueue().size());
//        serializeThreads.setText("Serializer Threads: " + peer.getSerializerThreads());
//        
//        incomingSeqNo.setText("Incoming Sequence No: " + peer.getIncomingSeqNo());
//        unprocessedInbound.setText("Unprocessed inbound: " + peer.getUnprocessedInboundPackets().size());
//        handlerQueue.setText("Handler Queue: " + peer.getHandlerQueue().size());
    }
}
