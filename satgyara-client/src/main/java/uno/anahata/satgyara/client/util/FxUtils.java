/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.client.util;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import javafx.application.Platform;
import static javafx.application.Platform.isFxApplicationThread;
import javafx.stage.Screen;

/**
 * General JavaFX utilities
 *
 * @author hendrikebbers
 *
 */
public class FxUtils {
    
    public static Screen getScreenForId(String id) {
        
        if (id == null) {
            return Screen.getPrimary();
        }
        
        for (Screen s: Screen.getScreens()) {
            if (getScreenId(s).equals(id)) {
                return s;
            }
        }
        
        throw new RuntimeException("No Screen for id " + id);
    }
    
    public static String getScreenId(Screen screen) {
        if (screen == null) {
            screen = Screen.getPrimary();
        }
        return "Screen-" + Screen.getScreens().indexOf(screen) + "@" + screen.hashCode();
    }
    
    public static class FxThreadException extends RuntimeException {
        
        public FxThreadException(Throwable cause) {
            super(cause);
        }
    }
    
    private static class ExceptionOrValueWrapper<V> {
        Exception e;
        V v;
    }
    
    public static <V> V runAndWait(final Callable<V> run)
            throws FxThreadException {
        if (isFxApplicationThread()) {
            try {
                return run.call();
            } catch (Exception e) {
                e.printStackTrace();
                throw new FxThreadException(e);
            }
            
        } else {
            final CountDownLatch doneLatch = new CountDownLatch(1);
            ExceptionOrValueWrapper<V> ew = new ExceptionOrValueWrapper<V>();
            Platform.runLater(() -> {
                try {
                    ew.v = run.call();
                } catch (Exception e){
                    e.printStackTrace();
                    ew.e = e;
                }finally {
                    doneLatch.countDown();
                }
            });
            
            try {
                doneLatch.await();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            
            if (ew.e != null) {
                throw new FxThreadException(ew.e);                
            } else {
                return ew.v;
            }
        }
    }
}
