/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.client;

import java.net.Inet4Address;
import uno.anahata.satgyara.peer.LocalPeer;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.client.peer.PeerInfo;
import uno.anahata.satgyara.concurrent.NamedThreadFactory;
import uno.anahata.satgyara.desktop.DesktopClient;
import uno.anahata.satgyara.peer.OutboundPeer;
import uno.anahata.satgyara.peer.RelayedPeer;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.seq.UUIDUtils;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class Main extends Application {

    private ExecutorService threadPool = Executors.newCachedThreadPool(new NamedThreadFactory("UIThread", true));

    private LocalPeer localServer;
    
    private Label headerLabel = new Label();
    VBox peersVBox = new VBox();
    Map<RemotePeer, PeerInfo> peer2Info= new HashMap<>();

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle(("Satgyara " + UUIDUtils.tail(LocalPeer.getInstance().getUuid())));
        stage.setWidth(800);
        stage.setHeight(600);

        VBox root = new VBox();
        root.getChildren().add(headerLabel);

        HBox actions = new HBox();
        TextField tf = new TextField();
        tf.setText("ci.anahata.uno:11711");
        //tf.setText("localhost:11711");
        Label error = new Label();
        Button connect = new Button("Connect");
        actions.getChildren().addAll(tf, connect, error);
        connect.setOnAction(eh -> {

            try {
                String[] chunks = tf.getText().split(":");
                if (chunks.length == 1) {
                    connect(Inet4Address.getByName(chunks[0]), 11711);
                } else {
                    connect(Inet4Address.getByName(chunks[0]), Integer.parseInt(chunks[1]));
                }
            } catch (Exception e) {
                log.error("Exception connectiong to: " + tf.getText(), e);
                error.setText(e.toString());
            }

        });
        root.getChildren().add(actions);

        root.getChildren().add(peersVBox);

        Scene s = new Scene(root);
        stage.setScene(s);

        Timeline oncePerSecond = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updatePeers();
            }
        }));
        oncePerSecond.setCycleCount(Timeline.INDEFINITE);
        oncePerSecond.play();

        
        //at.start();

        //iv.setImage(new Image());
        //ImageView iv = new ImageView();
        //Scene s = new Scene();
        //s.setS
        //stage.setScene(scene);
        //stage.show();
        //Toolkit.getToolkit().
        stage.show();
    }
    
    private void updatePeers() {
        
        List<RemotePeer> latest = LocalPeer.getInstance().getAllPeers();
        System.out.println("updatePeers: latest = " + latest);
        Set<RemotePeer> uiCurrent = new HashSet<>(peer2Info.keySet());
        //remove stale
        for (RemotePeer remotePeer : uiCurrent) {
            PeerInfo pi = peer2Info.get(remotePeer);
            if (!latest.contains(remotePeer)) {
                peersVBox.getChildren().remove(pi);
                peer2Info.remove(remotePeer);
            } else {
                pi.update();
            }
        }
        //add new
        for (RemotePeer remotePeer : latest) {
            if (!peer2Info.containsKey(remotePeer)) {
                PeerInfo pi = new PeerInfo(remotePeer);
                peer2Info.put(remotePeer,  pi);
                peersVBox.getChildren().add(pi);
            }
        }
        
        //for (RemotePeer remotePeer : latest) {
    }

    private void connect(InetAddress add, int port) {

        try {

            threadPool.submit(() -> {
                try {
                    //SocketAddress sa = new InetSocketAddress("ci.anahata.uno", 11711);
                    //SocketAddress sa = new InetSocketAddress("localhost", 11711);
                    SocketAddress sa = new InetSocketAddress(add, port);
                    System.out.println("aaaaaaaaa");
                    OutboundPeer op = new OutboundPeer(sa);
                    System.out.println("bbbbbbb");
                    Set<UUID> inbound = op.getRemoteRelay().getInboundPeers();
                    System.out.println("cccccccccc");
                    inbound.remove(LocalPeer.getInstance().getUuid());
                    System.out.println("Inbound = " + inbound);
                    if (!inbound.isEmpty()) {
                        RelayedPeer rp = op.relayTo(inbound.iterator().next());
                        DesktopClient dc = new DesktopClient(rp);
                        dc.openRemoteDesktop();
                    } else {
                        System.out.println("xxxxxxxxxxx");
                        DesktopClient dc = new DesktopClient(op);
                        dc.openRemoteDesktop();
                    }

                } catch (Exception e) {
                    log.error("Exception opening remote desktop", e);
                }
            });

            //SocketAddress sa = new InetSocketAddress("127.0.0.1", 11711);
            //SocketAddress sa = new InetSocketAddress("192.168.8.158", 11711);
            //RemotePeer rp = LocalPeer.getInstance().connectTo(sa);
            //RemoteDesktopService rdp = new RemoteDesktopService(rp);
            //rdp.startStreaming();
            //rp.sendRequestSpeedTest(1 * 1024);
            //rp.sendRequestStream(false);
            System.out.println("rp.sendRequestStream();");

            //SocketAddress sa = new InetSocketAddress("27.34.91.106", LocalServer.SATGYARA_PORT);
            //RemotePeer rp = new RemotePeer(sa);
            //TcpSocketConnection tsh = new TcpSocketConnection(sa);
            //tsh.sendRequestStream();
            //tsh.sendRequestSpeedTest(100 * 1024);
            //TCPReceiver receiver = new TCPReceiver(s);
            //RemoteDisplay rd = new RemoteDisplay(receiver);
            //RemoteDisplay rd = new RemoteDisplay(new InetSocketAddress("119.156.92.71", LocalServer.SATGYARA_PORT));
            //RemoteDisplay rd = new RemoteDisplay(new InetSocketAddress("27.34.90.99", LocalServer.SATGYARA_PORT));
            //rd.localServer = localServer;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Application.launch(args);
//        System.out.println("public static void main(String[] args) ");
//        System.out.println("public static void main(String[] args) starting LocalServer ");
//        LocalPeer.getInstance();
//        System.out.println("public static void main(String[] args) calling launch(" + Arrays.toString(args));
//        new Thread(() -> Application.launch(Main.class, args)).start();
    }

}
