/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.client.transport;

import java.net.Socket;
import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import uno.anahata.satgyara.transport.tcp.TcpConnection;

/**
 *
 * @author priyadarshi
 */
public class TcpConnectionStats extends Stage {

    private TcpConnection connection;
    private Label socketLabel = new Label();
    private Label downloadSpeedLabel = new Label();
    private Label uploadSpeedLabel = new Label();

    public TcpConnectionStats(TcpConnection connection) {
        this.connection = connection;
        init();
    }

    private void init() {
        setTitle(connection.getSocket().toString());
        setWidth(600);
        setHeight(400);

        VBox root = new VBox();
        root.setFillWidth(true);

        Scene scene = new Scene(root);
        setScene(scene);

        HBox uploadDownload = new HBox();
        uploadDownload.getChildren().add(downloadSpeedLabel);
        uploadDownload.getChildren().add(uploadSpeedLabel);        
        root.getChildren().add(uploadDownload);
        root.getChildren().add(socketLabel);

        AnimationTimer updateStatsTimer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                //System.out.println("captureScreenTimer " + l);
                updateGui();
            }
        };
        updateStatsTimer.start();

        sizeToScene();
    }

    private void updateGui() {

        Socket s = connection.getSocket();

        try {
            socketLabel.setText("Local Address " + connection.getSocket().getLocalAddress()
                    + "\nRemote Address " + s.getRemoteSocketAddress()
                    + "\nSend Buffer Size " + s.getSendBufferSize()
                    + "\nReceive Buffer Size " + s.getReceiveBufferSize()
                    + "\nTCP No Delay " + s.getTcpNoDelay()
                    + "\nTraffic Class " + s.getTrafficClass()
                    + "\nSO_LINGER " + s.getSoLinger()
                    + "\nSO_TIMEOUT " + s.getSoTimeout()
                    + "\nResuse Address " + s.getReuseAddress()
            );
        } catch (Exception e) {
            socketLabel.setText(e.toString());
        }

        downloadSpeedLabel.setText(
                "Average Download Speed: " + connection.getReader().getAverageDownloadSpeedMbpsFormatted() + " Mbps. "
                + "\nAverage Read Speed: " + connection.getReader().getAverageReadSpeedMbpsFormatted() + " Mbps."
                + "\nPackets Received: " + connection.getReader().getPacketsReceived() + " "
                + "\nData Received: " + connection.getReader().getTotalMBReceived() + " MB"
                + "\n"
                + "\nPacket Size: " + connection.getReader().getPacketSizeKBFormatted() + " KB"
                + "\nPacket Read Time: " + connection.getReader().getPacketReadTime() + " ms"
                //+ "\nPacket Read Speed: " + connection.getReader().getPacketR + " Mbps"
                + "\nPacket Read TPS: " + connection.getReader().getTps().getContiniousTicksPerSecondBigDecimal(1) + " (" + connection.getReader().getTps().getTicksPerSecondBigDecimal(1) + ")"
                + "\n"
//                + "\n"
//                + "\nHandler Queue size: " + connection.getReader().getHandlerThread().getQueueSize()
//                + "\nHandler Queue Empty Wait Time: " + connection.getReader().getHandlerThread().getQueueEmptyWaitTime()
//                + "\nHandler Handle Time: " + connection.getReader().getHandlerThread().getPacketHandleTime() + " ms"
        );

        uploadSpeedLabel.setText("Average Upload Speed: " + connection.getWriter().getAverageUploadSpeedMbpsFormatted() + " Mbps. "
                + "\nAverage Write Speed: " + connection.getWriter().getAverageWriteSpeedMbpsFormatted() + " Mbps."
                + "\nPackets Sent: " + connection.getWriter().getPacketsSent() + " "
                + "\nData Sent: " + connection.getWriter().getTotalMBSent() + " MB"
                + "\n"
                //+ "\nSend Queue Size: " + connection.getWriter().getSendQueueSize()
                //+ "\nSend Queue Empty Wait Time: " + connection.getWriter().getSendQueueEmptyWaitTime() + " ms"
                + "\nPacket Size: " + connection.getWriter().getPacketSizeKBFormatted() + " KB"
                //+ "\nPacket Write Time: " + connection.getWriter().getPacketWriteTime() + " ms"
                + "\nPacket Write Speed: " + connection.getWriter().getPacketWriteSpeedMbpsFormatted() + " Mbps"
//                + "\n"
//                + "\nSerializer Queue Size: " + connection.getWriter().getSerializerThread().getSerializationQueueSize() + " "
//                + "\nSerializer Queue Empty Wait Time: " + connection.getWriter().getSerializerThread().getSerializationQueueEmptyWaitTime() + " ms"
//                + "\nSerializer Compression Level: " + connection.getWriter().getSerializerThread().getCompressionLevel() + " "
//                + "\nSerializer Compress-Serialize Time: " + connection.getWriter().getSerializerThread().getCompressSerializeTime() + " ms"
//                + "\nSerializer Send Queue Full Wait Time: " + connection.getWriter().getSerializerThread().getSendQueueFullWaitTime() + " ms"
        );

        sizeToScene();
    }

}
