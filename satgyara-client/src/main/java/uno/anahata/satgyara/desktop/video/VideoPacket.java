/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video;

import java.util.Date;
import javafx.geometry.Point2D;
import uno.anahata.satgyara.desktop.DesktopPacket;

/**
 *
 * @author priyadarshi
 */
public class VideoPacket<T> extends DesktopPacket {
    public Date screenCaptureTimestamp = new Date();
    
    public String screenId;    
    public int displayWidth;
    public int displayHeight;
    
    public Double mouseX;
    public Double mouseY;    
    
    public boolean keyFrame;
    public int width;
    public int height;
    
    public T frame;
    
    /**
     * The mouse location in {@link #frame} terms
     * @return 
     */
    public Point2D getTransformedMouseLocation() {
        return new Point2D(((double)width / displayWidth) * mouseX, ((double)height / displayHeight) * mouseY);
    }
    
    /**
     * The mouse location in real coordinates.
     * 
     * @return 
     */
    public Point2D getMouseLocation() {
        return new Point2D(mouseX, mouseY);
    }

    @Override
    public String toString() {        
        return getClass().getSimpleName() + " screenCaptureTimestamp=" + screenCaptureTimestamp + ", screenId=" + screenId + ", displayWidth=" + displayWidth + ", displayHeight=" + displayHeight + ", mouseX=" + mouseX + ", mouseY=" + mouseY + ", keyFrame=" + keyFrame + ", width=" + width + ", height=" + height;
    }

    
    
}