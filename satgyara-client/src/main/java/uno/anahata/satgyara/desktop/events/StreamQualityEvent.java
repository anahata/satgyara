/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.events;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author priyadarshi
 */
@Getter
public class StreamQualityEvent implements Serializable{
    private int width;
    private int height;
    @Setter
    @Getter
    private boolean oneToOne;    
    private int bitsPerByte;

    public StreamQualityEvent(int width, int height, int bitsPerByte) {
        this.width = width;
        this.height = height;
        this.bitsPerByte = bitsPerByte;
    }


    @Override
    public String toString() {
        return "StreamQualityEvent{" + "width=" + width + ", height=" + height + ", bitsPerByte=" + bitsPerByte + '}';
    }
    
    
    
    
    
    
}
