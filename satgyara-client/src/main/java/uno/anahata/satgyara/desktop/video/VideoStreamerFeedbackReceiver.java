/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.desktop.DesktopPacket;
import uno.anahata.satgyara.desktop.events.UserInputEvents;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class VideoStreamerFeedbackReceiver extends AbstractProcessorThread<DesktopPacket, DesktopPacket>{

    private final AbstractVideoStreamer streamer;
    
    @Getter
    private VideoPacketAckPacket lastAck;
    
    public VideoStreamerFeedbackReceiver(AbstractVideoStreamer streamer) {
        super(5, 0);
        this.streamer = streamer;
        super.inQueue = streamer.getTransport().getInQueue();
    }
    
    @Override
    public DesktopPacket process(DesktopPacket t) throws Exception {
        log.trace("Feedback received: {}", t);
        
        if (t instanceof VideoPacketAckPacket) {
            lastAck = (VideoPacketAckPacket) t;
            //System.out.println("LastAck= " + lastAck);
        } else if (t instanceof UserInputEvents) {
            streamer.handleUserInputEvents((UserInputEvents)t);
        } else {
            log.warn("UFO {} " + t);
        }
        
        return t;
    }
}
