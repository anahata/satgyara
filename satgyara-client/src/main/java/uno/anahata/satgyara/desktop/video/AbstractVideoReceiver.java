/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video;

import javafx.application.Platform;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.image.WritableImage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.desktop.DesktopClient;
import uno.anahata.satgyara.desktop.DesktopPacket;
import uno.anahata.satgyara.desktop.render.RemoteDesktopScreen;
import uno.anahata.satgyara.transport.Transport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class AbstractVideoReceiver<V extends VideoPacket> extends AbstractProcessorThread<V, WritableImage> {
    private String displayId;
    
    protected WritableImage master;

    private Dimension2D displayDimension;
    private Point2D mouseLocation;
    private Point2D transformedMouseLocation;

    private long latency;
    
    private long minLatency = Long.MAX_VALUE;
    private long maxLatency = Long.MIN_VALUE;
    private long compressedSize;

    @Getter
    private RemoteDesktopScreen display;
    @Getter
    private DesktopClient client;
    @Getter
    private Transport transport;
    
    public AbstractVideoReceiver(DesktopClient client, Transport t)  {
        super(0,0);
        this.client = client;
        this.transport = t;
        inQueue = t.getInQueue();
    }

    @Override
    protected void onStartup() {
        Platform.runLater(() -> display = new RemoteDesktopScreen(this));
    }
    
    @Override
    protected void doOutput(WritableImage o) throws Exception {
        this.master = o; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public WritableImage process(V rdp) throws Exception {
        //System.out.println("received " + rdp);
        VideoPacketAckPacket ack = new VideoPacketAckPacket(rdp);
        ack.receiverTps = tps.getTicksPerSecond();
        transport.send(ack);
        //System.out.println("sent " + ack);
        
        displayId = rdp.screenId;
        
        displayDimension = new Dimension2D(rdp.displayWidth, rdp.displayHeight);
        //System.out.println("DisplayDimension=" + displayDimension);
        compressedSize = rdp.compressedSize;
        latency = System.currentTimeMillis() - rdp.screenCaptureTimestamp.getTime();
        if (latency < minLatency) {
            minLatency = latency;
        }
        
        if (latency > maxLatency) {
            maxLatency = latency;
        }
        
        handleMousePointer(rdp);
        
        if (rdp.frame == null) {
            return master;
        } else {
            handleImageIO(rdp);
        }
        
        Platform.runLater(() -> display.updateUI());
        
        return master;

    }

    private void handleMousePointer(VideoPacket diffPacket) {
        if (diffPacket.mouseX != null) {
            transformedMouseLocation = diffPacket.getTransformedMouseLocation();
            mouseLocation = diffPacket.getMouseLocation();
        } else {
            transformedMouseLocation = null;
            mouseLocation = null;
        }
    }
    
    public WritableImage getImage() {
        return master;
    }

    public long getCompressedSize() {
        return compressedSize;
    }

    public Point2D getMouseLocation() {
        return mouseLocation;
    }

    public Point2D getTransformedMouseLocation() {
        return transformedMouseLocation;
    }

    public long getLatency() {
        return latency;
    }
    
    public long getJitter() {
        return latency - minLatency;
    }

    public String getDisplayId() {
        return displayId;
    }

    public Dimension2D getDisplayDimension() {
        return displayDimension;
    }

    public int getQueueSize() {
        return getInQueue().size();
    }

    protected abstract void handleImageIO(V packet) throws Exception;
}
