/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import lombok.Getter;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import uno.anahata.satgyara.concurrent.NamedThreadFactory;

/**
 *
 * @author priyadarshi
 */
public abstract class Frame implements Serializable {
    
    private static final ExecutorService WORKERS = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new NamedThreadFactory("FrameEncoder-w#", true));
    
    public static final byte[] MASK = new byte[]{
        (byte)0b00000000,
        (byte)0b10000000,
        (byte)0b11000000,
        (byte)0b11100000,
        (byte)0b11110000,
        (byte)0b11111000,
        (byte)0b11111100,
        (byte)0b11111110,
        (byte)0b11111111,
    };
    
    @Getter
    protected int bitsPerByte;
    
    @Getter
    protected int planeLength;
    
    
    /**
     * The bgra representation of the frame
     */
    @Getter
    protected transient byte[] bgra;
    
    /**
     * The source for encoding and the output of the decoding
     */
    @Getter
    List<Plane> planes = new ArrayList<>();
    
    
    transient private List<Future> futures;
    
    
    public byte getMask() {
        return MASK[bitsPerByte];
    }
    
    public synchronized void startEncoding() {
        
        if (!isEncodable()) {
            //nothing to encode
            return;
        }
        
        if (futures != null) {
            //encoding already started
            return;
        }
        
        futures = new ArrayList<>();
        
        //decodedPlaneLen = decoded.length / 4;
        
        for (int i = 0; i < planes.size(); i++) {
            final int planeIdx = i;

            futures.add(WORKERS.submit(() -> {
                try {
                    planes.get(planeIdx).encode();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
        }
    }
    
    public synchronized void finishEncoding() throws InterruptedException, ExecutionException {
        if (!isEncodable()) {
            //nothing to encode
            return;
        }
        
        if (futures == null) {
            startEncoding();
        }
        
        for (Future f : futures) {
            f.get();
        }
        
        futures = null;
    }
    
//    public byte getDecoded(int pixelIndex, int plane) {
//        return decoded[(pixelIndex * 4) + plane];
//    }
    

    public synchronized void startDecoding() throws InterruptedException, ExecutionException {

        if (!isEncodable()) {
            //nothing to encode
            return;
        }
        

        futures = new ArrayList<>();
        
        //add the planes
        for (int i = 0; i < planes.size(); i++) {
            final int planeIdx = i;
            futures.add(WORKERS.submit(() -> {
                try {
                    //could stream this                     
                    //planes[planeIdx] = (byte[]) CompressionUtils.decompressDeserialize(compressedPlanes[planeIdx]);
                    planes.get(planeIdx).decode();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
        }
        
        
    }

    
    
    public synchronized void finishDecoding() throws InterruptedException, ExecutionException {
        
        if (!isEncodable()) {
            //nothing to encode
            return;
        }
        
        if (futures == null) {
            startDecoding();
        }
        
        for (Future f : futures) {
            f.get();
        }
        
        futures = null;
    }

    
    
    public int getEncodedPlaneLength() {
        int reqBits = planeLength * bitsPerByte;
        int reqBytes = (int) Math.ceil(reqBits / 8f);
        return reqBytes;
    }
    
    
    public int getCompressedPlanesSize() {
        int total = 0;
        for (int i = 0; i < planes.size(); i++) {
            total+= planes.get(i).getCompressedEncoded().length;
        }
        return total;
    }
    
    public abstract boolean isKeyFrame();
    
    public abstract boolean isEncodable();
    
    
    
}
