/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.capture;

import java.nio.ByteBuffer;
import static java.util.Arrays.fill;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.PixelFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.NamedThreadFactory;
import uno.anahata.satgyara.desktop.video.VideoPacket;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class AbstractScreenCapture<T, U> {

    //private static final ExecutorService WORKERS = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new NamedThreadFactory("ScreenCaptureToRGBAConverter-w#", true));
    private final Date captureTime = new Date();

    private T source;
    private final String sourceId;
    /**
     * Screen dimension at the time of the capture
     */
    private Rectangle2D sourceBounds;

    /**
     * Mouse position relative to screenDimension
     */
    private final Point2D mousePosition;

    @Getter
    @Setter
    private U capture;

    @Setter
    @Getter
    protected ByteBuffer bgraBuffer;

    //private Future bgraConversion;
    private Dimension2D captureDimension;

    @Getter
    @Setter
    private ByteBuffer scaledBuffer;

    @Getter
    @Setter
    private Dimension2D scaledDimension;

    public AbstractScreenCapture(
            T screen,
            String screenId,
            Rectangle2D screenBounds,
            U capture,
            Dimension2D captureDimension,
            Point2D mousePosition) {
        this.sourceId = screenId;
        this.sourceBounds = screenBounds;
        this.capture = capture;
        this.captureDimension = captureDimension;
        this.mousePosition = mousePosition;
    }

    public ByteBuffer getBgraBuffer() throws Exception {
        return bgraBuffer;
    }

    public ByteBuffer setBgraBuffer(ByteBuffer bb) throws Exception {
        int bgraBuffLen = (int) getCaptureDimension().getWidth() * (int) getCaptureDimension().getHeight() * 4;

        if (bb == null || bb.capacity() != bgraBuffLen) {
            log.info("Creating new bgraBuffer, len = {}", bgraBuffLen);
            bgraBuffer = ByteBuffer.allocate(bgraBuffLen);
        } else {
            bgraBuffer = bb;
        }
        return bgraBuffer;
    }

    public ByteBuffer setScaledBuffer(Dimension2D targetDimension, ByteBuffer bb) {

        if (targetDimension == null) {
            return bb;
        }
        
        final double sourceWidthHeightRatio = captureDimension.getWidth() / captureDimension.getHeight();
        final int sourceWidth = (int) captureDimension.getWidth();
        final int sourceHeight = (int) captureDimension.getHeight();

        //ensure we always downscale 
        int width = (int) targetDimension.getWidth();
        int height = (int) targetDimension.getHeight();

        if (width > sourceWidth) {
            width = sourceWidth;
        }

        if (height > sourceHeight) {
            height = sourceHeight;
        }

        //preserve ratio
        //800 x 600
        if (width > height) {
            height = (int) (width / sourceWidthHeightRatio);
        } else {
            width = (int) (height * sourceWidthHeightRatio);
        }

        final double scaleX = (double) width / sourceWidth;
        final double scaleY = (double) height / sourceHeight;

        if (scaleX > 0.75 && scaleY > 0.75) {
            scaledDimension = null;
            scaledBuffer = null;
            return bb;
        } else {
            //final PixelReader sourcePixelReader = source.getPixelReader();

            scaledDimension = new Dimension2D(width, height);

            int scaledBufferLen = width * height * 4;

            if (bb == null || bb.capacity() != scaledBufferLen) {
                log.info("Creating new scaledBuffer {}, old buff capacity = {} ", scaledBufferLen);
                scaledBuffer = ByteBuffer.allocate(scaledBufferLen);
            } else {
                scaledBuffer = bb;
            }
            return scaledBuffer;
        }

    }
    
    public boolean isScaled() {
        return scaledDimension != null;
    }
    
    public ByteBuffer getBuffer() {
        return isScaled() ? scaledBuffer : bgraBuffer;
    }
    
    public Dimension2D getDimension() {
        return isScaled() ? scaledDimension : captureDimension;
    }

    public String getScreenId() {
        return sourceId;
    }

    public Rectangle2D getScreenBounds() {
        return sourceBounds;
    }

    public Dimension2D getScreenDimension() {
        return new Dimension2D(sourceBounds.getWidth(), sourceBounds.getHeight());
    }

    public Point2D getMousePosition() {
        return mousePosition;
    }

    public Date getCaptureTime() {
        return captureTime;
    }

    public T getSource() {
        return source;
    }

    public Dimension2D getCaptureDimension() {
        return captureDimension;
    }

    public <T extends VideoPacket> void populateVideoPacket(T vp) {
        vp.screenId = sourceId;
        vp.displayWidth = (int) getScreenDimension().getWidth();
        vp.displayHeight = (int) getScreenDimension().getHeight();
        vp.width = (int) getDimension().getWidth();
        vp.height = (int) getDimension().getHeight();
        vp.mouseX = getMousePosition().getX();
        vp.mouseY = getMousePosition().getY();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + System.identityHashCode(this) + " {" + "captureTime=" + captureTime + ", source=" + source + ", sourceId=" + sourceId + ", sourceBounds=" + sourceBounds + ", mousePosition=" + mousePosition + ", capture=" + capture + ", captureDimension=" + captureDimension + '}';
    }

    /**
     * Encode the image to {@link #bgraBuffer}.
     */
    public abstract void encode();

}
