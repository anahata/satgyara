/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.events;

import java.util.ArrayList;
import java.util.List;
import uno.anahata.satgyara.desktop.DesktopPacket;

/**
 *
 * @author priyadarshi
 */
public class UserInputEvents extends DesktopPacket {
    public String displayId;
    public List<Object> events = new ArrayList<>();
    
    @Override
    public String toString() {
        return "UserInputEvents{" + "displayId=" + displayId + ", events=" + events + '}';
    }
    
}
