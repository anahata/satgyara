/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.capture.awt;

import java.awt.GraphicsDevice;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.nio.ByteBuffer;
import java.util.stream.IntStream;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import uno.anahata.satgyara.desktop.capture.AbstractScreenCapture;

/**
 *
 * @author priyadarshi
 */
public class AWTScreenCapture extends AbstractScreenCapture<GraphicsDevice, BufferedImage> {

    private final GraphicsDevice device;

    public AWTScreenCapture(
            GraphicsDevice gd,
            Rectangle deviceBounds,
            BufferedImage capture,
            Point mousePosition) {
        super(gd,
                gd.getIDstring(),
                new Rectangle2D(deviceBounds.getX(), deviceBounds.getY(), deviceBounds.getWidth(), deviceBounds.getHeight()),
                capture,
                new Dimension2D(capture.getWidth(), capture.getHeight()),
                new Point2D(mousePosition.getX(), mousePosition.getY()));
        this.device = gd;
    }

    public GraphicsDevice getGraphicsDevice() {
        return device;
    }

    @Override
    public void encode() {

        BufferedImage bi = getCapture();
        DataBuffer db = bi.getRaster().getDataBuffer();
        ///bi.getColorModel().

        final int[] pixels = ((DataBufferInt) db).getData();

        final int blueMask = ((DirectColorModel) bi.getColorModel()).getBlueMask();
        final int greenMask = ((DirectColorModel) bi.getColorModel()).getGreenMask();
        final int redMask = ((DirectColorModel) bi.getColorModel()).getRedMask();

        long ts = System.currentTimeMillis();
        for (int i = 0; i < pixels.length; i++) {

            //IntStream.range(0, pixels.length).parallel().forEach((i) -> {
            int base = i * 4;

            byte red = (byte) ((pixels[i] & redMask) >> 16);
            byte green = (byte) ((pixels[i] & greenMask) >> 8);
            byte blue = (byte) (pixels[i] & blueMask);

            bgraBuffer.put(base, blue);
            bgraBuffer.put(base + 1, green);
            bgraBuffer.put(base + 2, red);
            bgraBuffer.put(base + 3, (byte) 0xFF);
        }
        //});
        ts = System.currentTimeMillis() - ts;

        //System.out.println("encode bgra: " + ts);
        //even thought this is not going anywhere

    }

}
