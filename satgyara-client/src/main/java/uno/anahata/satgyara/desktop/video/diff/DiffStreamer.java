/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import javafx.application.Platform;
import javafx.geometry.Dimension2D;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.desktop.DesktopPeerlet;
import uno.anahata.satgyara.desktop.capture.AbstractScreenCapture;
import uno.anahata.satgyara.desktop.capture.AbstractScreenRecorder;
import uno.anahata.satgyara.desktop.capture.AbstractScreenRecorder;
import uno.anahata.satgyara.desktop.capture.awt.CaptureEncoder;
import uno.anahata.satgyara.desktop.capture.awt.AWTScreenRecorder;

import uno.anahata.satgyara.desktop.video.AbstractVideoStreamer;

import uno.anahata.satgyara.transport.packet.InMemoryPacketTransport;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.packet.PacketTransport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class DiffStreamer extends AbstractVideoStreamer<AbstractScreenCapture, DiffPacket> {

    private DiffPacket previousPacket;
    
    private AbstractScreenRecorder screenRecorder;
    private CaptureEncoder captureEncoder;
    private CaptureScaler captureScaler;
    
    
    public DiffStreamer(DesktopPeerlet peerlet, Transport t, String screenId) {
        super(peerlet, t, screenId);
    }

    @Override
    public void setTargetDimension(Dimension2D targetDimension) {
        super.setTargetDimension(targetDimension); //To change body of generated methods, choose Tools | Templates.
        captureScaler.setTargetDimension(targetDimension);
    }
    
    @Override
    protected void onStartup() throws Exception {
        super.onStartup();
        
        //inQueue = new LinkedBlockingQueue<>(1);
        //TODO have one recorder per screen                
        screenRecorder = new AWTScreenRecorder();
        
        captureEncoder = new CaptureEncoder();
        captureEncoder.setInQueue(screenRecorder.getOutQueue());
        
        captureScaler = new CaptureScaler();
        captureScaler.setTargetDimension(targetDimension);
        captureScaler.setInQueue(captureEncoder.getOutQueue());
        //captureScaler.setTargetDimension(targetDimension);
        //captureScaler.setInQueue(fxScreenRecorder.getOutQueue());

//        captureEncoder = new FxCaptureEncoder();
//        captureEncoder.setInQueue(captureScale.getOutQueue());
//        captureEncoder.setBitsPerColor(bitsPerColorComponent);
        //screenRecorder.setOutQueue(inQueue);
        //screenRecorder2.setOutQueue(inQueue);
        setInQueue(captureScaler.getOutQueue());

        screenRecorder.start();
        //Thread.sleep(20);
        captureEncoder.start();
        captureScaler.start();
        //captureEncoder.start();

    }

    @Override
    public AbstractScreenRecorder getScreenRecorder() {
        return screenRecorder;
    }

    @Override
    public CaptureEncoder getCaptureEncoder() {
        return captureEncoder;
    }

    @Override
    public CaptureScaler getCaptureScaler() {
        return captureScaler;
    }
    
    @Override
    protected AbstractScreenCapture getInput() throws Exception {        
        
        AbstractScreenCapture cap = super.getInput();
        
        
        //captureEncoder.getInQueue().clear();
        //captureEncoder.getInQueue().put(cap);
        
        
        //captureScaler.getInQueue().clear();
        //captureScaler.getInQueue().put(cap);
        
        
        
//        captureScale.getInQueue().clear();
//        captureScale.getInQueue().put(cap);
        return cap;
    }

    
    @Override
    public DiffPacket process(AbstractScreenCapture capture) throws Exception {

        DiffPacket currentPacket = new DiffPacket();

        capture.populateVideoPacket(currentPacket);
        
        
        int bpc = getBitsPerColorComponent();
        //currentPacket.capture = capture;
        //currentPacket.masterFrame = frame;

        //log.debug("Encoded capture length = {}", currentPacket.encodedCapture.length);
        currentPacket.keyFrame
                = previousPacket == null
                || !Objects.equals(previousPacket.screenId,capture.getScreenId())
                || previousPacket.frame.getBitsPerByte() < bpc
                || previousPacket.width != currentPacket.width                
                || previousPacket.height != currentPacket.height;

        
        //currentPacket.keyFrame = true;
            
        if (previousPacket != null && !currentPacket.keyFrame) {
            currentPacket.frame = new DiffFrame(capture.getBuffer(), previousPacket.frame, bpc);
        } else {
            log.info("Sending keyFrame\n\tlast capture ={} \n\tthis capture={}b ", previousPacket, currentPacket);
            currentPacket.frame = new MasterFrame(capture.getBuffer(), bpc);
        }
        
        
        return currentPacket;

    }

    @Override
    protected void doOutput(DiffPacket packet) throws Exception {
        if (outQueue != null) {
            //packet.frame.startCompression();;
            if (!packet.isChanged(previousPacket)) {
                log.debug("DiffPacket {} discarded, no change ", packet);
                discardedNoChange++;
            } if (getInFlightPackets() > getMaxInFlight()) {
                discardedInFlight++;
                log.debug("DiffPacket discarded {} in flight: {} maxInFlight", packet, getInFlightPackets(), getMaxInFlight());
            } else if (outQueue.offer(packet)) {
                sent++;
                packet.frame.startEncoding();
                previousPacket = packet;
            } else {
                discardedTransportFull++;
                log.debug("Transport Queue full, discarded{} ", packet);
            }
        }
    }


    public static void main(String[] args) {

        Platform.startup(() -> {
            try {

                PacketTransport pt1 = new InMemoryPacketTransport();
                PacketTransport pt2 = new InMemoryPacketTransport();
                pt1.getDeserializer().setInQueue(pt2.getSerializer().getOutQueue());
                pt2.getDeserializer().setInQueue(pt1.getSerializer().getOutQueue());

                DiffReceiver receiver = new DiffReceiver(null, pt1);
                DiffStreamer streamer = new DiffStreamer(null, pt2, null);
                //streamer.outQueue = receiver.getInQueue();
                streamer.start();
                receiver.start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
