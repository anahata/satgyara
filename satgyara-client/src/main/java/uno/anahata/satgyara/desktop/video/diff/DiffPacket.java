/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.util.zip.Deflater;
import uno.anahata.satgyara.desktop.capture.fx.FxScreenCapture;
import uno.anahata.satgyara.desktop.video.VideoPacket;

/**
 *
 * @author priyadarshi
 */
public class DiffPacket extends VideoPacket<Frame> {

    //public transient FxScreenCapture capture;
    //public transient DiffFrame masterFrame;

    @Override
    public void preSerialize() throws Exception {
        super.preSerialize(); //To change body of generated methods, choose Tools | Templates.
        frame.finishEncoding();
    }

    @Override
    public void postDeserialize() throws Exception {
        super.postDeserialize(); //To change body of generated methods, choose Tools | Templates.
        frame.startDecoding();
    }
    
    public boolean isMouseMoved(DiffPacket previous) {
        
        return previous == null || !previous.getMouseLocation().equals(getMouseLocation());
    }
    
    public boolean isChanged(DiffPacket previous) {
        return    previous == null 
                || isMouseMoved(previous) 
                || keyFrame
                || frame.isEncodable();
        
    }
    

    /**
     * Setting compression to best speed as the DiffFrame is already encoded.
     *
     * @return
     */
    @Override
    public int getCompressionLevel() {
        return Deflater.BEST_SPEED;
    }

    @Override
    public String toString() {
        //String ecLength = masterFrame != null ? masterFrame.getTotalPixels() + " pixels" : "null";
        return getClass().getSimpleName() + " encodedCapture=" + frame;
    }
}
