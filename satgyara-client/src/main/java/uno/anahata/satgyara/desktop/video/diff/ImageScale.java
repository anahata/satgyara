/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javafx.geometry.Dimension2D;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.paint.Color;

/**
 *
 * @author priyadarshi
 */
public class ImageScale {

    final static ExecutorService workerThreads = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public static WritableImage scale(WritableImage source, WritableImage target, PixelScaler sa) throws Exception {
        //final WritableImage target = new WritableImage((int) dim.getWidth(), (int) dim.getHeight());

        final int sourceWidth = (int) source.getWidth();
        final int sourceHeight = (int) source.getHeight();
        final int sourceScanLine = sourceWidth * 4;
        //final PixelReader sourcePixelReader = source.getPixelReader();

        final int targetWidth = (int) target.getWidth();
        final int targetHeight = (int) target.getHeight();
        final int targetScanLine = targetWidth * 4;

        final double scaleX = source.getWidth() / target.getWidth();
        final double scaleY = source.getHeight() / target.getHeight();

        javafx.scene.image.WritablePixelFormat<ByteBuffer> pixelFormat = PixelFormat.getByteBgraPreInstance();

        ByteBuffer sourceBb = ByteBuffer.allocate((int) source.getWidth() * (int) source.getHeight() * 4);
        source.getPixelReader().getPixels(0, 0, sourceWidth, sourceHeight, pixelFormat, sourceBb, sourceScanLine);

        ByteBuffer targetBb = ByteBuffer.allocate((int) target.getWidth() * (int) target.getHeight() * 4);

        List<Future> futures = new ArrayList<>();

        for (int y = 0; y < targetHeight; y++) {
            final double sourceYstart = y * scaleY;
            final double sourceYend = (y + 1) * scaleY;
            final int finalY = y;
            final int yOffset = (y * targetScanLine);
            for (int x = 0; x < targetWidth; x++) {
                double sourceXstart = x * scaleX;
                double sourceXend = (x + 1) * scaleX;
                byte[] comps = sa.getColor(sourceBb, 4, sourceScanLine, sourceXstart, sourceXend, sourceYstart, sourceYend);
                //System.out.println("x=" + x + " y=" + y + ":" + Arrays.toString(comps));
                int pos = yOffset + (x * 4);
                targetBb.put(pos, comps);
            }

        }
        
        targetBb.rewind();
        
        PixelWriter pw = target.getPixelWriter();
        pw.setPixels(0, 0, targetWidth, targetHeight, pixelFormat, targetBb, targetScanLine);
        return target;
    }
    
    
    
    public static void main(String[] args) {
        
    }
}

    
