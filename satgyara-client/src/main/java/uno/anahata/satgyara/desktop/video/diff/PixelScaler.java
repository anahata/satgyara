/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import static java.lang.Math.PI;
import static java.lang.Math.sin;
import java.nio.ByteBuffer;

/**
 *
 * @author priyadarshi
 */
public abstract class PixelScaler {

    public abstract byte[] getColor(ByteBuffer bb, int comps, int scanLineStr, double sourceXstart, double sourceXend, double sourceYstart, double sourceYend);

    public static class NearestNeighbour extends PixelScaler {

        @Override
        public byte[] getColor(ByteBuffer bb, int comps, int scanLineStr, double sourceXstart, double sourceXend, double sourceYstart, double sourceYend) {
            byte[] ret = new byte[comps];
            int y = (int) ((int) sourceYstart * scanLineStr);
            int x = (int) ((int) sourceXstart * 4);
            int pos = y + x;
            //System.out.println("pos = " + pos);
            //System.out.println("comps = " + comps);
            //System.out.println("cap = " + bb.capacity());

            ret[0] = bb.get(pos);
            ret[1] = bb.get(pos + 1);
            ret[2] = bb.get(pos + 2);
            ret[3] = bb.get(pos + 3);

            return ret;
            //return pixelReader.getArgb((int) sourceXstart, (int) sourceYstart);
        }
    }

    public static class Box extends PixelScaler {

        @Override
        public byte[] getColor(ByteBuffer bb, int comps, int scanLineStr, double sourceXstart, double sourceXend, double sourceYstart, double sourceYend) {

            byte[] ret = new byte[comps];
            double[] byteAggregate = new double[comps - 1];

            double totalWeight = 0;

            for (double y0 = sourceYstart; y0 < sourceYend; y0++) {

                int yInt = (int) y0;
                int yOffset = yInt * scanLineStr;
                double yWeight = 1;

                if (yInt < sourceYstart) {
                    yWeight = sourceYstart - yInt;
                } else if ((y0 + 1) > sourceYend) {
                    yWeight = sourceYend - y0;
                }

                //System.out.println("yWeight=" + yWeight);
                for (double x0 = sourceXstart; x0 < sourceXend; x0++) {
                    int xInt = (int) x0;
                    double xWeight = 1;
                    if (xInt < sourceXstart) {
                        xWeight = sourceXstart - xInt;
                    } else if ((x0 + 1) > sourceXend) {
                        xWeight = sourceXend - x0;
                    }

                    double xyWeight = xWeight + yWeight;
                    //System.out.println("xWeight=" + xWeight);
                    totalWeight += xyWeight;

                    int offSet = yOffset + (xInt * comps);
                    for (int i = 0; i < 3; i++) {
                        int pos = offSet + i;
                        int byteVal = bb.get(pos) & 0xFF;
                        //System.out.println("pos=" + pos + " val=" + compVal + " " + xyWeight);
                        byteAggregate[i] += byteVal * xyWeight;

                        //System.out.println("i= " + i + " byteVal=" + byteVal + " agg=" + byteAggregate[i]);
                    }
                }
            }

            for (int i = 0; i < 3; i++) {
                //double val = byteAggregate[i] / totalPixels;
                int val = (int) (byteAggregate[i] / totalWeight);
                ret[i] = (byte) val;
            }

            ret[3] = (byte) 0xFF;

            return ret;
        }

    }

    public static class Bunga extends PixelScaler {

        private double getWeight(double from, double to, double val) {
            double yWidth = to - from;
            double yCenter = from + (yWidth / 2);
            double distanceToCenter = Math.abs(yCenter - val);
            double yWeight = 1 - (distanceToCenter / (yWidth / 2));
            return yWeight;
        }

        @Override
        public byte[] getColor(ByteBuffer bb, int comps, int scanLineStr, double sourceXstart, double sourceXend, double sourceYstart, double sourceYend) {

            byte[] ret = new byte[comps];
            double[] byteAggregate = new double[comps - 1];

            double totalWeight = 0;

            for (double y0 = sourceYstart; y0 < sourceYend; y0++) {

                double yWeight = getWeight(sourceYstart, sourceYend, y0);

                int yInt = (int) y0;

                int yOffset = yInt * scanLineStr;

                //System.out.println("yWeight=" + yWeight);
                for (double x0 = sourceXstart; x0 < sourceXend; x0++) {
                    int xInt = (int) x0;
                    double xWeight = getWeight(sourceXstart, sourceXend, x0);

                    double xyWeight = xWeight + yWeight;
                    //System.out.println("xWeight=" + xWeight);
                    totalWeight += xyWeight;

                    int offSet = yOffset + (xInt * comps);
                    for (int i = 0; i < 3; i++) {
                        int pos = offSet + i;
                        int byteVal = bb.get(pos) & 0xFF;
                        //System.out.println("pos=" + pos + " val=" + compVal + " " + xyWeight);
                        byteAggregate[i] += byteVal * xyWeight;

                        //System.out.println("i= " + i + " byteVal=" + byteVal + " agg=" + byteAggregate[i]);
                    }
                }
            }

            for (int i = 0; i < 3; i++) {
                //double val = byteAggregate[i] / totalPixels;
                int val = (int) (byteAggregate[i] / totalWeight);
                ret[i] = (byte) val;
            }

            ret[3] = (byte) 0xFF;

            return ret;
        }
    }

    public static class BungaSin extends PixelScaler {

        private double getWeight(double from, double to, double val) {
            double windowWidth = to - from;
            double windowCenter = from + (windowWidth / 2);
            double distanceToCenter = Math.abs(windowCenter - val);
            double distanceToCenterCeroToOne = 1 - (distanceToCenter / (windowWidth / 2));
            double piValRad = distanceToCenterCeroToOne * Math.PI / 2;
            double sinVal = Math.sin(piValRad);
            double ret = 1 + sinVal;

            return ret;

        }

        @Override
        public byte[] getColor(ByteBuffer bb, int comps, int scanLineStr, double sourceXstart, double sourceXend, double sourceYstart, double sourceYend) {

            byte[] ret = new byte[comps];
            double[] byteAggregate = new double[comps - 1];

            double totalWeight = 0;

            for (double y0 = sourceYstart; y0 < sourceYend; y0++) {

                double yWeight = getWeight(sourceYstart, sourceYend, y0);

                int yInt = (int) y0;

                int yOffset = yInt * scanLineStr;

                //System.out.println("yWeight=" + yWeight);
                for (double x0 = sourceXstart; x0 < sourceXend; x0++) {
                    int xInt = (int) x0;
                    double xWeight = getWeight(sourceXstart, sourceXend, x0);

                    double xyWeight = xWeight + yWeight;
                    //System.out.println("xWeight=" + xWeight);
                    totalWeight += xyWeight;

                    int offSet = yOffset + (xInt * comps);
                    for (int i = 0; i < 3; i++) {
                        int pos = offSet + i;
                        int byteVal = bb.get(pos) & 0xFF;
                        //System.out.println("pos=" + pos + " val=" + compVal + " " + xyWeight);
                        byteAggregate[i] += byteVal * xyWeight;

                        //System.out.println("i= " + i + " byteVal=" + byteVal + " agg=" + byteAggregate[i]);
                    }
                }
            }

            for (int i = 0; i < 3; i++) {
                //double val = byteAggregate[i] / totalPixels;
                int val = (int) (byteAggregate[i] / totalWeight);
                ret[i] = (byte) val;
            }

            ret[3] = (byte) 0xFF;

            return ret;
        }
    }

//     /**
//     * Return Lanczos kernel filter value.
//     *
//     * @param t0 interpolation position from first pixel use to interpolate.
//     * @param t interpolation instant.
//     * @return Lanczos kernel filter value.
//     */
//    private double getLCZt(double t0, double t) {
//        double x = t-t0;
//        if (x == 0) return 1;
//        if (Math.abs(x) > lanczosWindow) return 0;
//        final double pix = PI*x;
//        return (lanczosWindow * sin(pix) * sin(pix/lanczosWindow))/(pix*pix);
//    }
    public static void main(String[] args) {

        byte b1 = (byte) 0xFF;
        System.out.println(unsignedToBytes((byte) -1));
        String s1 = String.format("%8s", Integer.toBinaryString(b1 & 0xFF)).replace(' ', '0');
        System.out.println(s1); // 10000001

    }

    public static int unsignedToBytes(byte b) {
        return b & 0xFF;
    }

}
