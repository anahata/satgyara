/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.events;

import java.io.Serializable;
import javafx.scene.input.ScrollEvent;
import lombok.ToString;

/**
 *
 * @author priyadarshi
 */
@ToString
public class ScrollUserInputEvent implements Serializable {
    public int deltaY;
    public ScrollUserInputEvent(ScrollEvent se) {
        this.deltaY = (int) se.getDeltaY();
    }
}
