/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video.h264;

import java.awt.AWTException;
import java.nio.ByteBuffer;
import java.util.Random;
import javafx.application.Platform;
import org.jcodec.codecs.h264.H264Encoder;
import org.jcodec.common.VideoEncoder.EncodedFrame;
import org.jcodec.common.model.Picture;
import uno.anahata.satgyara.desktop.DesktopPeerlet;
import uno.anahata.satgyara.desktop.DesktopPacket;
import uno.anahata.satgyara.desktop.capture.AbstractScreenRecorder;

import uno.anahata.satgyara.desktop.capture.awt.AWTCaptureToH264PictureCaptureTransformerThread;
import uno.anahata.satgyara.desktop.capture.awt.AWTScreenRecorder;
import uno.anahata.satgyara.desktop.capture.awt.CaptureEncoder;
import uno.anahata.satgyara.desktop.capture.picture.PictureScreenCapture;
import uno.anahata.satgyara.desktop.video.AbstractVideoStreamer;
import uno.anahata.satgyara.desktop.video.VideoPacket;
import uno.anahata.satgyara.desktop.video.diff.CaptureScaler;
import uno.anahata.satgyara.transport.Transport;


/**
 *
 * @author priyadarshi
 */
public class H264Streamer extends AbstractVideoStreamer<PictureScreenCapture, H264Packet> {

    private AWTScreenRecorder screenRecorder;
    private AWTCaptureToH264PictureCaptureTransformerThread resizerThread;
    private H264Encoder encoder = H264Encoder.createH264Encoder();
    private ByteBuffer bb;

    public H264Streamer(DesktopPeerlet peer, Transport t, String screenId) throws AWTException {
        super(peer, t, screenId);
        //todo get graphicsDeviceId for screenId and have one recorder per screen
        //if (windows / linux / mac) {
        screenRecorder = new AWTScreenRecorder();
        resizerThread = new AWTCaptureToH264PictureCaptureTransformerThread(screenRecorder, this, 1);
        inQueue = resizerThread.getOutQueue();
        outQueue = t.getOutQueue();

        //} else {
        //javafx capture 
        // resizing + bits per byte thread
        //}
    }

    @Override
    public AbstractScreenRecorder getScreenRecorder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CaptureEncoder getCaptureEncoder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CaptureScaler getCaptureScaler() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    @Override
    protected void onStartup() throws Exception {
        super.onStartup(); 
        screenRecorder.start();
        resizerThread.start();
    }

    @Override
    public H264Packet process(PictureScreenCapture psc) {

        Picture resizedPicture = psc.getCapture();
        
        //System.out.println("Encoder resized picture size " + resizedPicture + " targetDimension= " + targetDimension);
        //check buff size
        int approxBbSize = encoder.estimateBufferSize(resizedPicture);
        if (bb == null || bb.capacity() < approxBbSize) {
            bb = ByteBuffer.allocate(approxBbSize);
        }
        EncodedFrame ef = encoder.encodeFrame(resizedPicture, bb);
        
        H264Packet dp = new H264Packet();
        psc.populateVideoPacket(dp);        
        dp.keyFrame = ef.isKeyFrame();
        dp.frame = new byte[ef.getData().remaining()];
        ef.getData().get(dp.frame);
        
        return dp;

    }

    public static void main(String[] args) {
        Random r = new Random();
        Platform.startup(() -> {
            try {
//                H264Receiver receiver = new H264Receiver(null, -1);
//                H264Streamer streamer = new H264Streamer(null, null) {
//                    @Override
//                    protected void putOutput(RemoteDesktopPacket o) throws Exception {
//                        //if (r.nextBoolean()) {
////                        byte[] barr = o.data;
////                        ByteBuffer bb = ByteBuffer.wrap(barr);
////                        int randomCut = r.nextInt(barr.length);
////                        byte[] barr1 = new byte[randomCut];
////                        byte[] barr2 = new byte[barr.length - randomCut];
////                        System.out.println("Cuttin " + 0 + " " + randomCut);
////                        bb.get(barr1, 0, randomCut);                        
////                        System.out.println("Cuttin " + randomCut + " " + barr.length);
////                        bb.get(barr2, 0, barr.length - randomCut);
////                        o.data = barr1;
////                        
////                        VideoPacket vp2 = new VideoPacket();
//                        receiver.getInQueue().put(o);
//                        //}
//                    }
//                };
//                //streamer.outQueue = receiver.getInQueue();
//                streamer.start();
//                receiver.start();
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        ;
    }

}
