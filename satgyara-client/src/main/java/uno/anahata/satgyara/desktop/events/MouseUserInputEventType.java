/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.events;

import java.io.Serializable;

/**
 *
 * @author priyadarshi
 */
public enum MouseUserInputEventType implements Serializable{
    MOUSE_MOVED,
    MOUSE_PRESSED,
    MOUSE_RELEASED,
    MOUSE_CLICKED
}
