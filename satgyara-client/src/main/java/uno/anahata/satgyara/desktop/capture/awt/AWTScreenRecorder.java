/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.capture.awt;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.desktop.capture.AbstractScreenRecorder;

/**
 * @author priyadarshi
 */
@Slf4j
public class AWTScreenRecorder extends AbstractScreenRecorder<AWTScreenCapture> {

    //private Map<Screen, ScreenCapture> screenCapture = Collections.synchronizedMap(new HashMap<>());
    //private Robot fxRobot;
    private java.awt.Robot robot;

    private final GraphicsDevice graphicsDevice;

    public AWTScreenRecorder() throws AWTException{
        this(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice());
    }
    
    public AWTScreenRecorder(GraphicsDevice gd) throws AWTException{
        this.graphicsDevice = gd;  
        robot = new java.awt.Robot(graphicsDevice);
        setName(getClass().getSimpleName() + "-" + gd.getIDstring());
        setPriority(MAX_PRIORITY);
    }

    
    @Override
    public AWTScreenCapture process(Void t) throws Exception {
        
        Rectangle screenBounds = new Rectangle(graphicsDevice.getDefaultConfiguration().getBounds());
        
        Rectangle captureBounds = new Rectangle(graphicsDevice.getDisplayMode().getWidth(),
                graphicsDevice.getDisplayMode().getHeight());
        
        BufferedImage bi = robot.createScreenCapture(captureBounds);
        
        Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
        
        
        //TODO check mouseLocation and bounds with several screens
        return new AWTScreenCapture(graphicsDevice, screenBounds, bi, mouseLocation);
        
    }

    public static void main(String[] args) throws Exception {

        new AWTScreenRecorder().start();
//        
//        java.awt.Robot r = new java.awt.Robot();
//        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
//        System.out.println("Screen size " + d);
//        while (true) {
//            long ts = System.currentTimeMillis();
//            r.createScreenCapture(new Rectangle(d));
//            ts = System.currentTimeMillis() - ts;
//            
//            System.out.println("Screen cap took " + ts);
//        }

    }

}
