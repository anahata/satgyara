/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video;

import javafx.application.Platform;
import javafx.geometry.Dimension2D;
import javafx.scene.robot.Robot;
import javafx.stage.Screen;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.client.util.FxUtils;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.desktop.DesktopPeerlet;
import uno.anahata.satgyara.desktop.capture.AbstractScreenRecorder;
import uno.anahata.satgyara.desktop.capture.awt.CaptureEncoder;
import uno.anahata.satgyara.desktop.events.KeyUserEventType;
import uno.anahata.satgyara.desktop.events.KeyUserInputEvent;
import uno.anahata.satgyara.desktop.events.MouseUserInputEvent;
import uno.anahata.satgyara.desktop.events.MouseUserInputEventType;
import uno.anahata.satgyara.desktop.events.ScrollUserInputEvent;
import uno.anahata.satgyara.desktop.events.StreamQualityEvent;
import uno.anahata.satgyara.desktop.events.UserInputEvents;
import uno.anahata.satgyara.desktop.video.diff.CaptureScaler;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.packet.PacketTransport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class AbstractVideoStreamer<T, U extends VideoPacket> extends AbstractProcessorThread<T, U> {

    @Getter
    protected Screen screen;
    @Getter
    protected String screenId;

    @Getter
    @Setter
    protected Dimension2D targetDimension = new Dimension2D(800, 600);

    @Getter
    @Setter
    protected int bitsPerColorComponent = 4;

    /**
     * Max number of unackowledged packets
     */
    @Getter
    @Setter
    protected int maxInFlight = 4;

    /**
     * This is for handling user input
     */
    protected Robot fxRobot;

    @Getter
    protected DesktopPeerlet peerlet;

    @Getter
    protected Transport transport;

    @Getter
    protected VideoStreamerFeedbackReceiver feedbackReceiver;

    protected VideoStreamerMonitorThread monitor;
    
    protected volatile long sent;
    
    protected volatile long discardedInFlight;
    
    protected volatile long discardedNoChange;
    
    protected volatile long discardedTransportFull;

    protected AbstractVideoStreamer(DesktopPeerlet service, Transport transport, String screenId) {
        super(0, 0);
        this.outQueue = transport.getOutQueue();
        this.peerlet = service;
        this.transport = transport;
        this.screen = FxUtils.getScreenForId(screenId);
        this.screenId = FxUtils.getScreenId(screen);
        this.feedbackReceiver = new VideoStreamerFeedbackReceiver(this);
        setName(getClass().getSimpleName() + "|" + screenId + "|" + service);
        monitor = new VideoStreamerMonitorThread(this, transport.getLastSent());
    }
    
    @Override
    protected void onStartup() throws Exception {
        super.onStartup();
        feedbackReceiver.start();
        monitor.start();
    }

    public VideoPacketAckPacket getLastAck() {
        VideoPacketAckPacket lastAck = feedbackReceiver.getLastAck();
        return lastAck;
    }

    public long getReceiverLag() {
        VideoPacketAckPacket lastAck = getLastAck();
        if (lastAck == null) {
            return -1;
        } else {
            long now = System.currentTimeMillis();
            return now - lastAck.screenCaptureTimestamp.getTime();
        }
    }

    public int getReceiverTps() {
        VideoPacketAckPacket lastAck = getLastAck();
        if (lastAck != null) {
            return (int) lastAck.receiverTps;
        } else {
            return -1;
        }

    }

    public long getInFlightPackets() {

        int quequedElements = ((PacketTransport) transport).getSerializer().getInQueue().size();
        U lastInput = (U) ((PacketTransport) transport).getSerializer().getLastInput();
        //System.out.println("getInFlight() " + quequedElements + " queued");
        if (lastInput != null) {
            //System.out.println("getInFlight() " + lastInput.id + " lastInput");
            VideoPacketAckPacket lastAck = getLastAck();
            if (lastAck != null) {
                //System.out.println("getInFlight() " + lastAck.id + " lastAck");
                return quequedElements + lastInput.id - lastAck.packetId;
            } else {
                return quequedElements + lastInput.id;
            }
        } else {
            return quequedElements;
        }
    }
    
    public abstract AbstractScreenRecorder getScreenRecorder();
    
    public abstract CaptureEncoder getCaptureEncoder();   
    
    public abstract CaptureScaler getCaptureScaler();

    protected Robot getRobot() {
        if (fxRobot == null) {
            fxRobot = FxUtils.runAndWait(Robot::new);
        }
        return fxRobot;
    }
    
    
    public void handleUserInputEvents(UserInputEvents uie) {

        if (!isInterrupted()) {
            System.out.println("aaaaaa " + uie.events);
            Platform.runLater(() -> {
                System.out.println("--------------------");
                for (Object ev : uie.events) {

                    System.out.println("--> " + ev);

                    if (ev instanceof ScrollUserInputEvent) {
                        ScrollUserInputEvent se = (ScrollUserInputEvent) ev;
                        getRobot().mouseWheel(-1 * se.deltaY / 40);
                    } else if (ev instanceof KeyUserInputEvent) {
                        KeyUserInputEvent ke = (KeyUserInputEvent) ev;
                        if (ke.type == KeyUserEventType.KEY_PRESSED) {
                            getRobot().keyPress(ke.keyCode);
                        } else if (ke.type == KeyUserEventType.KEY_RELEASED) {
                            getRobot().keyRelease(ke.keyCode);
                        } else {
                            log.warn("Unknown key event type " + ke);
                        }
                    }
                    if (ev instanceof MouseUserInputEvent) {
                        MouseUserInputEvent me = (MouseUserInputEvent) ev;
                        if (me.type == MouseUserInputEventType.MOUSE_CLICKED) {
                            getRobot().mouseMove(me.x, me.y);
                            getRobot().mouseClick(me.button);
                        } else if (me.type == MouseUserInputEventType.MOUSE_MOVED) {
                            getRobot().mouseMove(me.x, me.y);
                        } else if (me.type == MouseUserInputEventType.MOUSE_PRESSED) {
                            getRobot().mouseMove(me.x, me.y);
                            getRobot().mousePress(me.button);
                        } else if (me.type == MouseUserInputEventType.MOUSE_RELEASED) {
                            getRobot().mouseMove(me.x, me.y);
                            getRobot().mouseRelease(me.button);
                        } else {
                            log.warn("Unknown mouse event type " + me);
                        }
                    } else if (ev instanceof StreamQualityEvent) {

                        StreamQualityEvent sqe = (StreamQualityEvent) ev;
                        if (sqe.isOneToOne()) {
                            setTargetDimension(null);
                        } else {
                            setTargetDimension(new Dimension2D(Math.max(1, sqe.getWidth()), Math.max(1, sqe.getHeight())));
                        }
                        setBitsPerColorComponent(sqe.getBitsPerByte());
                        log.debug(" RECEIVED " + ev + " targetDimension=" + targetDimension + " bitsPerByte:" + bitsPerColorComponent);

                    }
                }
            });
        }
    }

}
