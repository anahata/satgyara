/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class DiffFrame extends Frame {

    @Getter
    private long noise;

    public DiffFrame(ByteBuffer bgra, final Frame previous, final int bitsPerByte) {
        byte[] previousBgra = previous.bgra;
        this.bitsPerByte = bitsPerByte;
        this.bgra = new byte[bgra.capacity()];
        bgra.get(0, this.bgra);
        this.planeLength = this.bgra.length / 4;

        planes = new ArrayList<>(3);
        planes.add(new DiffPlane(0, this));
        planes.add(new DiffPlane(1, this));
        planes.add(new DiffPlane(2, this));

        byte mask = getMask();

        for (int i = 0; i < planeLength; i++) {
            int base = i * 4;
            planes.get(0).decoded[i] = (byte) ((this.bgra[base + 0] ^ previousBgra[base + 0]) & mask);
            planes.get(1).decoded[i] = (byte) ((this.bgra[base + 1] ^ previousBgra[base + 1]) & mask);
            planes.get(2).decoded[i] = (byte) ((this.bgra[base + 2] ^ previousBgra[base + 2]) & mask);
            //this.bgra[base + 0] = (byte) (this.bgra[base + 0] & mask);
            //this.bgra[base + 1] = (byte) (this.bgra[base + 1] & mask);
            //this.bgra[base + 2] = (byte) (this.bgra[base + 2] & mask);

            //this.bgra[base + 0] = (byte) (previousBgra[base + 0] ^ planes.get(0).decoded[i]);
            //this.bgra[base + 1] = (byte) (previousBgra[base + 1] ^ planes.get(1).decoded[i]);
            //this.bgra[base + 2] = (byte) (previousBgra[base + 2] ^ planes.get(2).decoded[i]);
            //this.bgra[base + 1] = (byte) (this.bgra[base + 1] & mask);
            //this.bgra[base + 2] = (byte) (this.bgra[base + 2] & mask);
        }

        noise = planes.get(0).getNoise() + planes.get(1).getNoise() + planes.get(2).getNoise();

        if (getNoisePct() > 10) {
            int offset = 0;
            if (previous instanceof DiffFrame) {
                DiffFrame diffFrame = (DiffFrame) previous;
                if (diffFrame.isSinglePlane()) {
                    InterweavedPlane previousPlane = (InterweavedPlane) diffFrame.planes.get(0);
                    System.out.println("previous offset=" + previousPlane.offset);
                    offset = (previousPlane.offset + 1) % 3;
                }
            }
            System.out.println("offset=" + offset);
            InterweavedPlane iw = new InterweavedPlane(offset, this);
            for (int i = 0; i < planeLength; i++) {
                int plane = (i + offset) % 3;
                iw.decoded[i] = planes.get(plane).decoded[i];
            }
            planes.clear();
            planes.add(iw);

            this.bgra = new byte[bgra.capacity()];
            System.arraycopy(previousBgra, 0, this.bgra, 0, this.bgra.length);
            iw.applyDiff(this.bgra);

        } else {
            System.out.println("previous frame " + previous);
            if (previous instanceof DiffFrame) {
                DiffFrame diffFrame = (DiffFrame) previous;
                if (diffFrame.isSinglePlane()) {
                    System.out.println("previous frame single " + diffFrame.getPlanes());                
                } else {
                    System.out.println("previous frame multi " + diffFrame.getPlanes());                
                }

            }

//        if (getNoisePct() > 10) {
//            int planeToKeep = 0;
//            if (previous instanceof DiffFrame) {
//                DiffFrame diffFrame = (DiffFrame)previous;
//                if (diffFrame.isSinglePlane()) {
//                    planeToKeep = (diffFrame.planes.get(0).planeIdx + 1) % 3;
//                }
//            }
//             
//            DiffPlane plane = planes.get(planeToKeep);
//            planes.clear();
//            planes.add(plane);
//            
//            //now, rebuild this.bgra with just the modifications from the kept plane
//            this.bgra = new byte[bgra.capacity()];            
//            System.arraycopy(previousBgra, 0, this.bgra, 0, this.bgra.length);
//            plane.applyDiff(this.bgra);
//        }
        }
    }

    

    public boolean isSinglePlane() {
        return planes.size() == 1;
    }

    private long getMaxNoise() {
        return planeLength * 3;
    }

    public void applyDiff(byte[] barr) {
        if (!isEncodable()) {
            return;
        }

        //byte mask = getMask();
        //int singlePlaneIdx = planes.get(0).planeIdx;
        //for (int i = 0; i < planes.size(); i++) {
        for (Plane plane : planes) {
            plane.applyDiff(barr);
        }
        //}

    }

    public double getNoisePct() {
        return 100d * noise / getMaxNoise();
    }

//    private DiffPlane getPlane(int comp) {
//        return planes.size() == 1 ? planes.get(0) : planes.get(comp);
//    }
    @Override
    public boolean isKeyFrame() {
        return false;
    }

    @Override
    public boolean isEncodable() {
        return getNoise() > 0;
    }

}
