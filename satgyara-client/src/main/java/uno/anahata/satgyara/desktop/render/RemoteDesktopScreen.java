/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.render;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Affine;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.desktop.events.KeyUserInputEvent;
import uno.anahata.satgyara.desktop.video.AbstractVideoReceiver;
import uno.anahata.satgyara.desktop.events.MouseUserInputEvent;
import uno.anahata.satgyara.desktop.events.ScrollUserInputEvent;
import uno.anahata.satgyara.desktop.events.StreamQualityEvent;
import uno.anahata.satgyara.desktop.events.UserInputEvents;
import uno.anahata.satgyara.desktop.video.VideoPacket;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class RemoteDesktopScreen extends Stage implements EventHandler<InputEvent> {

    //RemoteScreenRefresher refresher;
    private ImageView remoteScreenImageView = new ImageView();

    private ToolBar actions = new ToolBar();
    private ToggleButton showRemoteMouse = new ToggleButton("Show Mouse");
    private ToggleButton oneToOne = new ToggleButton("1:1");
    private Button swapSides = new Button("Swap sides");

    private Slider bpc = new Slider();

    private Canvas canvas = new Canvas();

    private Label receiverQueueSize = new Label("Queue");

    private Label receiverOPS = new Label("OPS");

    double imageScaleX = 0;
    double imageScaleY = 0;

    private Image mouse = new Image(getClass().getResourceAsStream("/cursor.png"));

    AbstractVideoReceiver<? extends VideoPacket> receiver;

    private final BlockingQueue<java.util.EventObject> inputEventQueue = new LinkedBlockingQueue<java.util.EventObject>();

    public RemoteDesktopScreen(AbstractVideoReceiver<? extends VideoPacket> receiver) {
        this.receiver = receiver;
        init();
    }

    Timer resizeTimer;

    private void fireDelayedQualityEvent() {
        log.debug("fireDelayedQualityEvent " + getWidth() + " " + getHeight());

        if (resizeTimer != null) {
            resizeTimer.cancel();
        }
        resizeTimer = new Timer();
        resizeTimer.schedule(new StreamQualityEventSender(), 1000);

    }

    private class StreamQualityEventSender extends TimerTask {

        @Override
        public void run() {
            StreamQualityEvent sqe = new StreamQualityEvent((int) canvas.getWidth(), (int) canvas.getHeight(), (int) bpc.getValue());
            if (oneToOne.isSelected()) {
                sqe.setOneToOne(true);
            }

            UserInputEvents uie = new UserInputEvents();
            uie.displayId = receiver.getDisplayId();
            uie.events.add(sqe);
            try {
                log.debug("QualityEvent!!! SENDING " + sqe);
                receiver.getClient().getRemoteDesktop().handleUserInputEvents(uie);
                log.info("QualityEvent!!! SENT " + sqe);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class InputEventSender extends Thread {

        @Override
        public void run() {
            setName("InputEventSender|" + receiver);
            while (!isInterrupted() && !receiver.isInterrupted()) {

                try {
                    ArrayList<EventObject> pendingEvents = new ArrayList<>();
                    EventObject head = inputEventQueue.take();//blocks waiting for an event
                    pendingEvents.add(head);
                    inputEventQueue.drainTo(pendingEvents);

                    if (receiver.getDisplayDimension() == null) {
                        log.warn("Discarding {} input events as no video packet has been received yet{} ", pendingEvents.size(), pendingEvents);
                        continue;
                    }

                    UserInputEvents uie = new UserInputEvents();
                    uie.displayId = receiver.getDisplayId();

                    for (EventObject eo : pendingEvents) {
                        if (eo instanceof MouseEvent) {
                            MouseEvent me = (MouseEvent) eo;
                            MouseUserInputEvent converted = new MouseUserInputEvent(me);
                            converted.x = (int) (me.getX() / canvas.getWidth() * receiver.getDisplayDimension().getWidth());
                            converted.y = (int) (me.getY() / canvas.getHeight() * receiver.getDisplayDimension().getHeight());
                            System.out.println("Sending " + me);
                            uie.events.add(converted);
                        } else if (eo instanceof KeyEvent) {
                            KeyEvent ke = (KeyEvent) eo;
                            KeyUserInputEvent converted = new KeyUserInputEvent(ke);
                            uie.events.add(converted);
                        } else if (eo instanceof ScrollEvent) {
                            ScrollUserInputEvent converted = new ScrollUserInputEvent((ScrollEvent) eo);
                            uie.events.add(converted);
                        }
                    }

                    receiver.getTransport().send(uie);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            log.info("InputEventSender for " + receiver + " exited");
        }
    }

    public final void init() {

        bpc.setMin(1);
        bpc.setMax(8);
        bpc.setBlockIncrement(1);
        bpc.setMajorTickUnit(1);
        bpc.setMinorTickCount(0);
        bpc.setValue(4);
        bpc.setShowTickLabels(true);
        bpc.setShowTickMarks(true);
        bpc.setSnapToTicks(true);

        setTitle(receiver.getName());
        setWidth(800);
        setHeight(600);
        canvas.widthProperty().addListener((o) -> fireDelayedQualityEvent());
        canvas.heightProperty().addListener((o) -> fireDelayedQualityEvent());
        bpc.valueProperty().addListener((o) -> fireDelayedQualityEvent());
        oneToOne.setOnAction((e) -> {
            fireDelayedQualityEvent();
        });
        //oneToOne.addListener((o) -> fireDelayedQualityEvent());

        Pane remoteScreenPane = new Pane();

        remoteScreenPane.getChildren().add(canvas);
        canvas.heightProperty().bind(remoteScreenPane.heightProperty());
        canvas.widthProperty().bind(remoteScreenPane.widthProperty());

        canvas.addEventFilter(MouseEvent.ANY, (e) -> canvas.requestFocus());
        canvas.addEventFilter(KeyEvent.ANY, (event) -> {
            //KeyCode code = event.getCode();
            //canvas.requestFocus();

            event.consume();

            KeyEvent newEvent
                    = new KeyEvent(event.getSource(),
                            event.getTarget(), event.getEventType(),
                            event.getCharacter(), event.getText(),
                            event.getCode(), event.isShiftDown(),
                            true, event.isAltDown(),
                            event.isMetaDown());

            handle(newEvent);

        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                this);
        //canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED,
                this);
        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED,
                this);
        canvas.addEventHandler(MouseEvent.MOUSE_MOVED,
                this);
        canvas.addEventHandler(ScrollEvent.ANY,
                this);
        //canvas.addEventHandler(MouseEvent.MOUSE_MOVED, this);
        canvas.addEventHandler(KeyEvent.KEY_PRESSED,
                this);
        canvas.addEventHandler(KeyEvent.KEY_RELEASED,
                this);

        

        swapSides.setOnAction(
                (event) -> {
                    try {
                        //receiver.getPeer().sendSwapSides(receiver.getDisplayId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );

        actions.getItems()
                .addAll(showRemoteMouse, swapSides, oneToOne, bpc, receiverOPS, receiverQueueSize);

        HBox stats = new HBox(receiverOPS, receiverQueueSize);
        VBox root = new VBox(actions, remoteScreenPane, stats);

        VBox.setVgrow(remoteScreenPane, Priority.ALWAYS);

        root.setFillWidth(
                true);

        Scene scene = new Scene(root);

        setScene(scene);

        //canvas.addE
        //refresher = new RemoteScreenRefresher();
        //refresher.start();
        new InputEventSender()
                .start();

        System.out.println(
                "Showing " + this);
        show();

    }

    public void updateUI() {
        updateStats();
        updateCanvas();
    }

    private void updateStats() {
        long compressedSizeKb = receiver.getCompressedSize() / 1024;
        String lastReceived = receiver.getLastInput() != null ? receiver.getLastInput().id + "" : "none";
        receiverOPS.setText(compressedSizeKb + " KB "
                + "TPS: " + receiver.getTps().getTicksPerSecondBigDecimal(0) + " "
                + " Jitter: " + receiver.getJitter() + " ms."
                + " Last received :" + lastReceived);
        //receiverQueueSize.setText("Queue: " + receiver.getQueueSize());
    }

    private void updateCanvas() {
        WritableImage wi = receiver.getImage();
        Point2D mouseLocation = receiver.getMouseLocation();
        if (wi != null) {
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setImageSmoothing(true);

            imageScaleX = canvas.getWidth() / wi.getWidth();
            imageScaleY = canvas.getHeight() / wi.getHeight();

            //System.out.println("imageScalex=" + imageScaleX);
            //System.out.println("imageScaleY=" + imageScaleY);
            //System.out.println("Image Scale " + imageScaleX + " " + imageScaleY);
            Affine calculatedTransform = new Affine();
            calculatedTransform.appendScale(imageScaleX, imageScaleY);

            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            gc.setTransform(calculatedTransform);
            gc.drawImage(wi, 0, 0);

            if (mouseLocation != null && showRemoteMouse.isSelected()) {
                double mouseX = receiver.getTransformedMouseLocation().getX();
                double mouseY = receiver.getTransformedMouseLocation().getY();
                //System.out.println("Mouse location " + mouseLocation);
                gc.drawImage(mouse, mouseX, mouseY);
            }
        }
    }

    @Override
    public void handle(InputEvent event) {
        System.out.println(">" + event);
        //event.consume();
        inputEventQueue.add(event);
        //System.out.println("<" + event);

        //System.out.println("Got InputEvent " + inputEventQueue.size());
    }

}
