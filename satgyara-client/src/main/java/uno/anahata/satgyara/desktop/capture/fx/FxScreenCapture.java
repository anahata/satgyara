/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.capture.fx;

import java.nio.ByteBuffer;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.stage.Screen;
import lombok.Getter;
import lombok.Setter;
import uno.anahata.satgyara.client.util.FxUtils;
import uno.anahata.satgyara.desktop.capture.AbstractScreenCapture;

/**
 *
 * @author priyadarshi
 */
public class FxScreenCapture extends AbstractScreenCapture<Screen, WritableImage> {
    
    
    public FxScreenCapture(Screen screen, Rectangle2D screenBounds, WritableImage capture, Point2D mousePosition) {
        super(screen, 
                FxUtils.getScreenId(screen), 
                screenBounds, 
                capture, 
                new Dimension2D(capture.getWidth(), capture.getHeight()),
                mousePosition);
    
    }

    @Override
    public void encode() {
        getCapture().getPixelReader().getPixels(
                0, 0, 
                (int)getCapture().getWidth(), 
                (int)getCapture().getHeight(), 
                PixelFormat.getByteBgraPreInstance(), 
                bgraBuffer, 
                (int) getCapture().getWidth() * 4);
    }
    

    
}
