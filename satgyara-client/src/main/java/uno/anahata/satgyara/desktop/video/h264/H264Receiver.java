/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video.h264;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import javafx.embed.swing.SwingFXUtils;
import org.jcodec.codecs.h264.H264Decoder;
import org.jcodec.codecs.h264.io.model.Frame;
import org.jcodec.scale.AWTUtil;
import uno.anahata.satgyara.desktop.DesktopClient;
import uno.anahata.satgyara.desktop.video.AbstractVideoReceiver;
import uno.anahata.satgyara.desktop.video.VideoPacket;
import uno.anahata.satgyara.transport.Transport;

/**
 *
 * @author priyadarshi
 */
public class H264Receiver extends AbstractVideoReceiver<H264Packet> {

    private H264Decoder decoder = new H264Decoder();
    
    public H264Receiver(DesktopClient service, Transport t)  {
        super(service, t);
        inQueue = t.getInQueue();
    }
    
//    @Override
//    protected H264Packet getInput() throws Exception {
//        System.out.println("H264Receiver getInput()");
//        H264Packet ret = (H264Packet) service.getUdpTransport().getInQueue().take();
//        System.out.println("H264Receiver getInput()... " + ret);
//        return ret;
//    }

    @Override
    protected void handleImageIO(H264Packet frame) throws Exception {
        ByteBuffer bb = ByteBuffer.wrap(frame.frame);
        
        byte[][] buffer = new byte
                    [3]
                    [(int)frame.width * (int)frame.height * 4];
        
        try {
            Frame f = decoder.decodeFrame(bb, buffer);
            if (f != null) {
                BufferedImage decodedImage = AWTUtil.toBufferedImage(f);
                master = SwingFXUtils.toFXImage(decodedImage, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

}
