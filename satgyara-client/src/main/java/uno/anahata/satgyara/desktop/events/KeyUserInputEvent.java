/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.events;

import java.io.Serializable;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author priyadarshi
 */
@Slf4j
@ToString
public class KeyUserInputEvent implements Serializable {

    public KeyUserEventType type;
    public KeyCode keyCode;

    public KeyUserInputEvent(KeyEvent ke) {
        keyCode = ke.getCode();
        if (ke.getEventType() == KeyEvent.KEY_PRESSED) {
            type = KeyUserEventType.KEY_PRESSED;
        } else if (ke.getEventType() == KeyEvent.KEY_RELEASED) {
            type = KeyUserEventType.KEY_RELEASED;
        } else {
            log.warn("Unknown key event type: {}", ke);
        }
    }

    @Override
    public String toString() {
        return "KeyUserInputEvent{" + "type=" + type + ", keyCode=" + keyCode + '}';
    }

}
