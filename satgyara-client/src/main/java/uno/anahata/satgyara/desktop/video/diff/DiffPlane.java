/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.io.Serializable;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import lombok.Getter;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import static uno.anahata.satgyara.desktop.video.diff.Frame.MASK;

/**
 *
 * @author priyadarshi
 */
public class DiffPlane extends Plane {

    @Getter
    public int planeIdx;
    
    
    public DiffPlane(int planeIdx, Frame parent) {
        super(parent);
        this.planeIdx = planeIdx;
    }
    
    
    public void applyDiff(byte[] barr) {
        byte mask = parent.getMask();
        for (int i = 0; i < decoded.length; i++) {
            int base = i * 4;
            barr[base + planeIdx] = (byte) ((barr[base + planeIdx] ^ decoded[i]) & mask);
        }
    }
    
    /**
     * 
     * @param planeNo
     * @param imageChannel
     * @return 
     */
    public void writeToImage(WritableImage imageChannel) {
        //byte[] barr = decoded;
        byte[] copy = new byte[decoded.length * 4];
        //add the alpha
        
        int imageLength = parent.planeLength * 4;

        for (int i = 0; i < decoded.length; i ++) {
            int pos = (i * 4) + planeIdx;
            int alphaPos = (i * 4) + 3;
            copy[pos] = decoded[i];
            copy[alphaPos] = (byte) 0xFF;
        }

        imageChannel.getPixelWriter().setPixels(
                0, 0,
                (int)imageChannel.getWidth(), (int)imageChannel.getHeight(),
                PixelFormat.getByteBgraInstance(),
                copy,
                0,
                (int)imageChannel.getWidth() * 4);
    }

    
}
