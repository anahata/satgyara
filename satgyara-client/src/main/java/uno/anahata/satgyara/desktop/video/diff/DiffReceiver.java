/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.desktop.DesktopClient;
import uno.anahata.satgyara.desktop.video.AbstractVideoReceiver;
import uno.anahata.satgyara.transport.Transport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class DiffReceiver extends AbstractVideoReceiver<DiffPacket> {

    private byte[] masterFrameBgra;

    public DiffReceiver(DesktopClient service, Transport transport) throws IOException {
        super(service, transport);
    }

    @Override
    protected void handleImageIO(DiffPacket diffPacket) throws Exception {

        //BufferedImage bi = ImageIO.read(new ByteArrayInputStream(frame.data));
        //System.out.println("Received color model " + bi.getColorModel().);
        diffPacket.frame.finishDecoding();
        
        //WritableImage wi = new WritableImage

        //WritableImage newMaster = new WritableImage(diffPacket.width, diffPacket.height);
        if (masterFrameBgra == null || diffPacket.keyFrame) {
            masterFrameBgra = diffPacket.frame.bgra;
            master = new WritableImage(diffPacket.width, diffPacket.height);
            //System.out.println(getName() + " Got keyFrame");
        } else if (!diffPacket.keyFrame && diffPacket.frame.isEncodable()) {
            
            DiffFrame df = (DiffFrame) diffPacket.frame;
            df.applyDiff(masterFrameBgra);
            
        } else {
            log.trace("No Image I/O work for receiver");
        }

        master.getPixelWriter().setPixels(
                0, 0, 
                diffPacket.width, diffPacket.height, 
                PixelFormat.getByteBgraPreInstance(), 
                masterFrameBgra, 
                0, 
                diffPacket.width * 4);
        
        //master = newMaster;

//        try {
//            BufferedImage bImage = SwingFXUtils.fromFXImage(master, null);
//            File file = new File("c:\\users\\pablo\\desktop\\satgyara\\" + diffPacket.seqNo + ".png");
//            file.mkdirs();
//            ImageIO.write(bImage, "png", file);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
    }

}
