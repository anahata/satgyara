/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javafx.stage.Screen;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.client.util.FxUtils;
import uno.anahata.satgyara.desktop.events.UserInputEvents;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.peerlet.PeerletContext;
import uno.anahata.satgyara.desktop.video.AbstractVideoStreamer;
import uno.anahata.satgyara.desktop.video.diff.DiffStreamer;
import uno.anahata.satgyara.transport.Transport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class DesktopPeerlet implements RemoteDesktopPeerlet{
    
    public AbstractVideoStreamer streamer;
    
    @Override
    public Collection<String> getScreenIds() {
        return Screen.getScreens().stream().map(s -> FxUtils.getScreenId(s)).collect(Collectors.toList());
    }
    
    @Override
    public void startStreaming(UUID transportUUID) {
        log.debug("startStreaming {}", transportUUID);
        RemotePeer caller = PeerletContext.getCaller();
        log.debug("startStreaming {}", caller);
        Transport t = caller.getTransports().get(transportUUID);
        log.debug("startStreaming {}", t);
        streamer = new DiffStreamer(this, t, null);
        log.debug("startStreaming {}", streamer);
        //avs.put(transportUUID, streamer);
        streamer.start();
        log.debug("startStreaming {}", streamer);
        log.debug("startStreaming EXITED {}", transportUUID);
    }
    
    @Override
    public void handleUserInputEvents(UserInputEvents events) {
        streamer.handleUserInputEvents(events);
    }
}
