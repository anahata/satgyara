/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javafx.application.Platform;
import javafx.geometry.Dimension2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.client.util.FxUtils;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.concurrent.NamedThreadFactory;
import uno.anahata.satgyara.desktop.capture.AbstractScreenCapture;
import uno.anahata.satgyara.desktop.capture.awt.CaptureEncoder;
import uno.anahata.satgyara.desktop.capture.fx.FxScreenRecorder2;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class CaptureScaler<T extends AbstractScreenCapture> extends AbstractProcessorThread<T, T> {

    @Getter
    @Setter
    private PixelScaler pixelScaler = new PixelScaler.Bunga();

    @Getter
    @Setter
    private Dimension2D targetDimension = new Dimension2D(800, 600);

    private final ExecutorService resizeThreads = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new NamedThreadFactory("FxCaptureScaler-w#", true));

    private ByteBuffer scaledBuffer;

    public CaptureScaler() {
        super(1, 1);
    }

    @Override
    public T process(T capture) throws Exception {

        scaledBuffer = capture.setScaledBuffer(targetDimension, scaledBuffer);
        
        if (!capture.isScaled()) {
            //no scaling needed
            return capture;
        }
        
        ByteBuffer sourceBb = capture.getBgraBuffer();
        Dimension2D sourceDimension = capture.getCaptureDimension();
        final int sourceWidth = (int) sourceDimension.getWidth();
        final int sourceHeight = (int) sourceDimension.getHeight();
        final int sourceScanLine = sourceWidth * 4;

        final ByteBuffer targetBb = capture.getScaledBuffer();
        Dimension2D scaledDimension = capture.getScaledDimension();        
        final int targetWidth = (int) scaledDimension.getWidth();
        final int targetHeight = (int) scaledDimension.getHeight();
        final int targetScanLine = targetWidth * 4;

        final double scaleX = (double) sourceWidth / targetWidth;
        final double scaleY = (double) sourceHeight / targetHeight;        

        //System.out.println("sourceBB " + sourceBb + " " + targetBb);
        final PixelScaler finalPixelScaler = pixelScaler;

        List<Future> lineResizeTasks = new ArrayList();
        for (int y = 0; y < targetHeight; y++) {
            final double sourceYstart = y * scaleY;
            final double sourceYend = (y + 1) * scaleY;

            final int yOffset = (y * targetScanLine);
            //final int diffOffset = (y * diffScanLine);
            lineResizeTasks.add(resizeThreads.submit(() -> {

                for (int x = 0; x < targetWidth; x++) {
                    double sourceXstart = x * scaleX;
                    double sourceXend = (x + 1) * scaleX;
                    byte[] comps = finalPixelScaler.getColor(sourceBb, 4, sourceScanLine, sourceXstart, sourceXend, sourceYstart, sourceYend);
                    //System.out.println("x=" + x + " y=" + y + ":" + Arrays.toString(comps));
                    int pixelStart = yOffset + (x * 4);
                    for (int i = 0; i < 4; i++) {
                        byte compVal = comps[i];
                        int compPos = pixelStart + i;
                        targetBb.put(compPos, (byte) compVal);
                    }
                }
            }));
        }

        for (Future f : lineResizeTasks) {
            f.get();
        }

        return capture;
    }

    @Override
    protected void doOutput(T o) throws Exception {
        
        if (outQueue != null) {
            outQueue.clear();
            outQueue.put(o);
        }

    }

    public static void main(String[] args) {

        CaptureScaler cs = new CaptureScaler();

        final ImageView monitor = new ImageView();
        monitor.setSmooth(false);
        monitor.addEventHandler(ScrollEvent.SCROLL, (e) -> {
            
            if (e.getDeltaY() > 0) {
                //monitor.setScaleX(monitor.getScaleX() + 0.25);
                //monitor.setScaleY(monitor.getScaleY() + 0.25);
                monitor.setFitWidth(monitor.getImage().getWidth() * 20);
                monitor.setFitHeight(monitor.getImage().getHeight() * 20);
            } else {
                monitor.setFitWidth(monitor.getImage().getWidth() / 4);
                monitor.setFitHeight(monitor.getImage().getHeight() / 4);
                //monitor.setScaleX(monitor.getScaleX() - 0.25);
                //monitor.setScaleY(monitor.getScaleY() - 0.25);
            }
            
        });
        Platform.startup(() -> {

            monitor.setSmooth(false);
            
            Button nn = new Button("NN");
            nn.setOnAction((e) -> {cs.pixelScaler = new PixelScaler.NearestNeighbour();});
            Button box = new Button("Box");
            box.setOnAction((e) -> {cs.pixelScaler = new PixelScaler.Box();});
            Button bunga = new Button("Bunga");
            bunga.setOnAction((e) -> {cs.pixelScaler = new PixelScaler.Bunga();});
            Button bungaSin = new Button("BungaSin");
            bungaSin.setOnAction((e) -> {cs.pixelScaler = new PixelScaler.BungaSin();});
            
            ToolBar tb = new ToolBar(nn, box, bunga, bungaSin);
            
            ScrollPane sp = new ScrollPane(monitor);
            var scene = new Scene(new VBox(tb, sp));
            var stage = new Stage();
            stage.setScene(scene);
            stage.show();

            FxScreenRecorder2 rec = new FxScreenRecorder2(null);
            rec.start();

            CaptureEncoder ce = new CaptureEncoder();
            ce.setInQueue((BlockingQueue) rec.getOutQueue());
            ce.start();

            cs.setInQueue((BlockingQueue) ce.getOutQueue());
            cs.start();

            stage.widthProperty().addListener((o) -> {
                cs.setTargetDimension(new Dimension2D(stage.getWidth(), stage.getHeight()));
            });

            //Sta
        });

        while (true) {
            try {
                AbstractScreenCapture o = (AbstractScreenCapture) cs.getOutQueue().take();
                //o.getScaledBuffer().rewind();

                //System.out.println(o.getScaledBuffer());

                WritableImage wi = new WritableImage((int) o.getDimension().getWidth(), (int) o.getDimension().getHeight());
                wi.getPixelWriter().setPixels(
                        0, 0,
                        (int) o.getDimension().getWidth(), (int) o.getDimension().getHeight(),
                        PixelFormat.getByteBgraPreInstance(),
                        o.getBuffer(),
                        (int) o.getDimension().getWidth() * 4);

                FxUtils.runAndWait(() -> {
                    monitor.setImage(wi);
                    return null;
                }
                );
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
