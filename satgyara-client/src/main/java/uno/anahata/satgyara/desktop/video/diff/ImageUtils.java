/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.BitSet;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;

/**
 *
 * @author priyadarshi
 */
public class ImageUtils {

    static byte compressFactor = 16;

    public static WritableImage fromArray(int width, int height, int[] arr) {
        WritablePixelFormat<IntBuffer> f = PixelFormat.getIntArgbInstance();
        WritableImage fullImage = new WritableImage(width, height);
        fullImage.getPixelWriter().setPixels(0, 0, width, height, f, arr, 0, width);
        return fullImage;
    }

    public static WritableImage fromArray(int width, int height, byte[] arr) {
        
        int pixels = width * height;
        byte[] bgra = new byte[pixels * 4];
        
        for (int i =0; i< pixels; i++) {
            byte blue = arr[i];
            byte green = arr[i + pixels];
            byte red = arr[i + pixels + pixels];
            
            int bgraIdx = i * 4;
            bgra[bgraIdx] = blue;
            bgra[bgraIdx + 1] = green;
            bgra[bgraIdx + 2] = red;
            bgra[bgraIdx + 3] = -1;
            //bgra[i + ] = arr[i];
        }
        
        
        WritablePixelFormat<ByteBuffer> f = PixelFormat.getByteBgraInstance();
        WritableImage fullImage = new WritableImage(width, height);
        fullImage.getPixelWriter().setPixels(0, 0, width, height, f, bgra, 0, width * 4);
        return fullImage;
    }
    
    public static WritableImage fromArrayNew(int width, int height, byte[] arr) {
        WritablePixelFormat<IntBuffer> f = PixelFormat.getIntArgbInstance();
        int[] newPixels = new int[arr.length / 3];
        for (int i = 0, j = 0; i < arr.length; i += 3, j++) {
            byte[] newBytes = {-1,
                (byte) (arr[i] * 1),
                (byte) (arr[i + 1] * 1),
                (byte) (arr[i + 2] * 1)};
            newPixels[j] = ByteBuffer.wrap(newBytes).getInt();
        }
        WritableImage fullImage = new WritableImage(width, height);
        fullImage.getPixelWriter().setPixels(0, 0, width, height, f, newPixels, 0, width);
        return fullImage;
    }

    // #1 RGB #2 RGB #3 RGB
    // #1R#2R#1G#2G#1B#1B
    public static byte[] toArray(int i) {
        byte[] result = new byte[3];
        result[0] = (byte) (i >> 16);
        result[1] = (byte) (i >> 8);
        result[2] = (byte) (i);
        return result;
    }

    public static int[] toArray(WritableImage i) {
        PixelReader pr = i.getPixelReader();
        WritablePixelFormat<IntBuffer> f = PixelFormat.getIntArgbInstance();
        int[] buffer = new int[(int) i.getWidth() * (int) i.getHeight()];
        pr.getPixels(0, 0, (int) i.getWidth(), (int) i.getHeight(), f, buffer, 0, (int) i.getWidth());
        return buffer;
    }

    public static byte fistroByte(byte b, int bitsPerByte) {        
        int insignificantBits = 8 - bitsPerByte;
        return (byte) ((b >> insignificantBits) << insignificantBits);
    }

    public static byte[] toArrayBytes(WritableImage i, int bitsPerByte) {
        long ts = System.currentTimeMillis();
        PixelReader pr = i.getPixelReader();

        WritablePixelFormat<ByteBuffer> f = PixelFormat.getByteBgraInstance();
        byte[] buffer = new byte[(int) i.getWidth() * (int) i.getHeight() * 4];
        pr.getPixels(0, 0, (int) i.getWidth(), (int) i.getHeight(), f, buffer, 0, (int) i.getWidth() * 4);
        byte[] bufferProcessed = new byte[(int) i.getWidth() * (int) i.getHeight() * 3];
        int pixels = (int) i.getWidth() * (int) i.getHeight();
        for (int j = 0; j < pixels; j++) {
            int bufferIdx = j * 4;
            byte blue = buffer[bufferIdx];
            byte green = buffer[bufferIdx + 1];
            byte red = buffer[bufferIdx + 2];
            byte alpha = buffer[bufferIdx + 3];
            
            bufferProcessed[j] = fistroByte(blue, bitsPerByte);
            bufferProcessed[j + pixels] = fistroByte(green, bitsPerByte);
            bufferProcessed[j + pixels + pixels] = fistroByte(red, bitsPerByte);
        }
        
//        int[] buffer = new int[(int)i.getWidth() * (int)i.getHeight()];
//        pr.getPixels(0, 0, (int)i.getWidth(), (int) i.getHeight(), f, buffer, 0, (int)i.getWidth());
        ts = System.currentTimeMillis() - ts;
        System.out.println("toArrayBytes took {}" + ts);
        return bufferProcessed;
    }
    
    

    public static int[] applyDiff(int[] oldImgArr, int[] diffArr) {
        int[] newImgArr = new int[oldImgArr.length];
        long ts = System.currentTimeMillis();
        for (int i = 0; i < oldImgArr.length; i++) {
            //System.out.println("i= " + i);
            newImgArr[i] = diffArr[i] + oldImgArr[i];
        }
        ts = System.currentTimeMillis() - ts;
        //System.out.println("applyDiff took " + ts + " ms.");

        return newImgArr;
    }

    public static byte[] applyDiff(byte[] oldImgArr, byte[] diffArr) {
        byte[] newImgArr = new byte[oldImgArr.length];
        long ts = System.currentTimeMillis();
        for (int i = 0; i < oldImgArr.length; i++) {
            //System.out.println("i= " + i);
            newImgArr[i] = (byte) (diffArr[i] + oldImgArr[i]);
        }
        ts = System.currentTimeMillis() - ts;
        //System.out.println("applyDiff took " + ts + " ms.");

        return newImgArr;
    }

    public static int[] makeDiff(int[] oldImgArr, int[] newImgArr) {

        int[] diffImgArr = new int[oldImgArr.length];
        long ts = System.currentTimeMillis();
        int diffCount = 0;
        for (int i = 0; i < oldImgArr.length; i++) {
            //System.out.println("i= " + i);
            diffImgArr[i] = newImgArr[i] - oldImgArr[i];
            if (diffImgArr[i] != 0) {
                diffCount++;
                //System.out.println("!!! " + diffImgArr[i] + " old= " + oldImgArr[i] + " new " + newImgArr[i]);
            }
        }
        ts = System.currentTimeMillis() - ts;
        float pctChange = (100 * ((float) diffCount / diffImgArr.length));
        //System.out.println("makeDiff took " + ts + " bytes changed = " + pctChange + "%");

        return diffCount == 0 ? null : diffImgArr;

    }

    public static byte[] makeDiff(byte[] oldImgArr, byte[] newImgArr) {

        byte[] diffImgArr = new byte[oldImgArr.length];
        long ts = System.currentTimeMillis();
        int diffCount = 0;
        for (int i = 0; i < oldImgArr.length; i++) {
//            diffImgArr[i]  = new byte[3];
            //System.out.println("i= " + i);
//            int newInt = {newImgArr[i][0]};
//            int oldInt = newImgArr[i][0];
            diffImgArr[i] = (byte) (newImgArr[i] - oldImgArr[i]);
//            diffImgArr[i][0] = (byte) (newImgArr[i][0] - oldImgArr[i][0]);
//            diffImgArr[i][1] = (byte) (newImgArr[i][1] - oldImgArr[i][1]);
//            diffImgArr[i][2] =  (byte) (newImgArr[i][2] - oldImgArr[i][2]);
            
            if (diffImgArr[i] != 0) {
                diffCount++;
//                //System.out.println("!!! " + diffImgArr[i] + " old= " + oldImgArr[i] + " new " + newImgArr[i]);
            }
        }
        ts = System.currentTimeMillis() - ts;
        float pctChange = (100 * ((float) diffCount / diffImgArr.length));
        System.out.println("makeDiff took " + ts + " bytes changed = " + pctChange + "%");

        return diffCount == 0 ? null : diffImgArr;

    }
    
    public static WritableImage resampleJavaFX(Image input, WritableImage output) {
        final int W = (int) input.getWidth();
        final int H = (int) input.getHeight();
        final double S_W = output.getWidth() / input.getWidth();
        final double S_H = output.getHeight() / input.getHeight();
        //System.out.println("S_W " + S_W);
        //System.out.println("S_H " + S_H);

        PixelReader reader = input.getPixelReader();
        PixelWriter writer = output.getPixelWriter();

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                final int argb = reader.getArgb(x, y);
                for (int dy = 0; dy < S_H; dy++) {
                    for (int dx = 0; dx < S_W; dx++) {
                        writer.setArgb((int)(x * S_W) + dx, (int)(y * S_H) + dy, argb);
                    }
                }
            }
        }

        return output;
    }
    
    
    
    
    public static void main(String[] args) {
        BitSet bs = new BitSet(1900 * 1440 * 4);
        
        byte b = (byte)-1;
        int intFromByte = b;
        
        System.out.println("Intfrombyte " + intFromByte);
        byte b2 = (byte) (b / 2);
        
        byte b3 = (byte) (b2 * 2);
        System.out.println(b + " " + b2 + " " + b3);
        //Integer.parseInt("4").
    }
}
