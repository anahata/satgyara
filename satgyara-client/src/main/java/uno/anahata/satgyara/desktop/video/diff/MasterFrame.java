/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Decompressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import uno.anahata.satgyara.concurrent.NamedThreadFactory;
import uno.anahata.satgyara.transport.packet.CompressionUtils;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class MasterFrame extends Frame {

    public MasterFrame(ByteBuffer bgra, final int bitsPerByte) {

        this.bitsPerByte = bitsPerByte;
        this.bgra = new byte[bgra.capacity()];
        bgra.get(0, this.bgra);
        this.planeLength = this.bgra.length / 4;

        planes = new ArrayList<>(3);
        planes.add(new DiffPlane(0, this));
        planes.add(new DiffPlane(1, this));
        planes.add(new DiffPlane(2, this));

        byte mask = getMask();
        for (int i = 0; i < planeLength; i++) {
            int base = i * 4;
            planes.get(0).decoded[i] = (byte) (this.bgra[base] & mask);
            planes.get(1).decoded[i] = (byte) (this.bgra[base + 1] & mask);
            planes.get(2).decoded[i] = (byte) (this.bgra[base + 2] & mask);
        }
        //bgra.get(0, this.bgra);
    }

    @Override
    public synchronized void finishDecoding() throws InterruptedException, ExecutionException {
        super.finishDecoding();
        this.bgra = new byte[planeLength * 4];
        for (int i = 0; i < planeLength; i++) {
            int base = i * 4;
            this.bgra[base + 0] = planes.get(0).decoded[i];
            this.bgra[base + 1] = planes.get(1).decoded[i];
            this.bgra[base + 2] = planes.get(2).decoded[i];
            this.bgra[base + 3] = (byte) 0xFF;
        }

    }

    @Override
    public boolean isKeyFrame() {
        return true;
    }

    @Override
    public boolean isEncodable() {
        return true;
    }

    public static void main(String[] args) {
        System.out.println(144 / 4);
        //byte val = (byte) 0B10000000;
        int val = -32 & 0xFF;
        System.out.println(String.format("%8s", Integer.toBinaryString(val & 0xFF)).replace(' ', '0'));
        val = (val >>> 7);
        int valInt = (byte) (val >>> 7);
        System.out.println(String.format("%32s", Integer.toBinaryString(val & 0xFFFFFFFF)).replace(' ', '0'));
        System.out.println(String.format("%32s", Integer.toBinaryString(valInt & 0xFFFFFFFF)).replace(' ', '0'));
    }

}
