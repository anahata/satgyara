/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.capture.awt;

import java.nio.ByteBuffer;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.desktop.capture.AbstractScreenCapture;

/**
 *
 * @author priyadarshi
 */
public class CaptureEncoder extends AbstractProcessorThread<AbstractScreenCapture, AbstractScreenCapture>{

    private ByteBuffer bgraBuffer;
    
    public CaptureEncoder() {
        super(1, 1);
    }

    @Override
    public AbstractScreenCapture process(AbstractScreenCapture t) throws Exception {
        bgraBuffer = t.setBgraBuffer(bgraBuffer);
        t.encode();
        return t;
    }
    
    @Override
    protected void doOutput(AbstractScreenCapture o) throws Exception {
        if (outQueue != null) {
            outQueue.clear();
            outQueue.put(o);
        }
        
    }
    
}
