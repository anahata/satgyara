/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video.diff;

import java.io.Serializable;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import lombok.Getter;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import static uno.anahata.satgyara.desktop.video.diff.Frame.MASK;

/**
 *
 * @author priyadarshi
 */
public abstract class Plane implements Serializable {

    private static LZ4Factory factory = LZ4Factory.fastestInstance();

    @Getter
    public Frame parent;
    
    @Getter
    public transient byte[] decoded;
    
    @Getter
    public transient byte[] encoded;
    
    @Getter
    public byte[] compressedEncoded;

    protected Plane(Frame parent) {
        this.parent = parent;
        decoded = new byte[parent.planeLength];
    }
    
    public long getNoise() {
        long noise = 0;
        for (int i = 0; i < decoded.length; i++) {
            if (decoded[i] != 0) {
                noise++;
            }
             //Math.abs(decoded[i]);
        }
        return noise;
    }
            
    public void encode() {
        long ts = System.currentTimeMillis();

        //int compLengrg = source.length / 4;
        //int bitPerByte = 4;
        int reqBits = parent.planeLength * parent.bitsPerByte;
        int reqBytes = (int) Math.ceil(reqBits / 8f);
        encoded = new byte[reqBytes];
        final byte mask = parent.getMask();
        //final byte[] plane = encoded;

        int bitPos = 0;
        for (int pixelIndex = 0; pixelIndex < parent.planeLength; pixelIndex++) {
            byte val = (byte) (mask & decoded[pixelIndex]);
            if (val != 0) {
                write(encoded, bitPos, val, parent.bitsPerByte);
            }

            bitPos += parent.bitsPerByte;
        }

        ts = System.currentTimeMillis() - ts;
        //System.out.println("encoded plane " + planeIdx + " in " + ts);

        compress();

    }
    
    private static void write(byte[] target, int fromBit, byte val, int bitsPerByte) {
        //7 bits per byte
        //bitpos 49
        //val 10000011

        //01010 000
        //00100-010 00-100000
        //00100-010 10-100000
        int fromByte = fromBit / 8;//0
        int fromBitInFromByte = fromBit - (fromByte * 8);//7
        target[fromByte] |= (val & 0xFF) >> fromBitInFromByte; // val = 00000001

        int endBit = fromBit + bitsPerByte;
        int endByte = endBit / 8; //14/8 = 1

        if (endByte > fromByte) {
            int overflow = endBit - (endByte * 8);// 6
            int startBitInVal = (8 - fromBitInFromByte);
            int endBitInVal = startBitInVal + overflow;

            if (overflow > 0) {
                val = (byte) (val << startBitInVal);
                //not needed as val was already masked 
                val = (byte) (val & MASK[overflow]);
                //00001000
                target[endByte] |= val;
            }
        }

    }

    public void decode() {
        
        uncompress();
        
        decoded = new byte[parent.planeLength];
        
        int mask = parent.getMask();

        for (int pixelNumber = 0; pixelNumber < parent.planeLength; pixelNumber++) {

            int sourcePos = pixelNumber;
            //System.out.println("plane = " + planeIdx + " pixelNumber= " + pixelNumber + " sourcePos=" + sourcePos);
            //int pixelNumber = sourcePos / 4;
            int fromBitIdx = pixelNumber * parent.bitsPerByte;
            int fromByte = fromBitIdx / 8;
            int fromByteBitIdx = (fromByte * 8);
            int fromBitInFromByte = fromBitIdx - fromByteBitIdx;

            decoded[sourcePos] |= mask & (byte) (encoded[fromByte] << fromBitInFromByte);

            //000-010-011-101
            int toBit = fromBitIdx + parent.bitsPerByte;
            int toByte = toBit / 8;
            int toBitInToByte = toBit - (toByte * 8);
            if (toByte > fromByte && toBitInToByte > 0) {
                int bitsInFromByte = 8 - fromBitInFromByte;
                int bitsToAdd = ((encoded[toByte] & 0xFF) >>> bitsInFromByte) & 0xFF;
                //bitsToAdd = (bitsToAdd & mask & 0xFF) & 0xFF;
//                            bitsToAdd = (byte) (bitsToAdd << bitsInFromByte);
                decoded[sourcePos] |= bitsToAdd;
            }

        }
    }

    private void compress() {
        long ts = System.currentTimeMillis();
        LZ4Compressor compressor = factory.highCompressor(17);
        compressedEncoded = compressor.compress(encoded);
        ts = System.currentTimeMillis() - ts;
        //System.out.println(compressor + " " + ts + " ms " + compressedPlanes.length);
    }

    private void uncompress() {
        LZ4FastDecompressor decompressor = factory.fastDecompressor();
        encoded = decompressor.decompress(compressedEncoded, parent.getEncodedPlaneLength());
    }
    
    public abstract void applyDiff(byte[] barr);
    
    public abstract void writeToImage(WritableImage imageChannel);
    
}
