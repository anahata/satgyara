/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.audio;

/**
 *
 * @author Vijay
 */
import javax.sound.sampled.*;
import java.io.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A sample program is to demonstrate how to record sound in Java author:
 * www.codejava.net
 */
public class JavaSoundRecorder implements LineListener {

    // record duration, in milliseconds
    static final long RECORD_TIME = 60000;  // 1 minute

    // path of the wav file
    File wavFile = new File("D:/RecordAudio.wav");

    // format of audio file
    AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;

    // the line from which audio data is captured
    TargetDataLine line;

    boolean playCompleted;

    AudioInputStream ais;

    BlockingQueue<byte[]> audioBytes = new LinkedBlockingQueue<byte[]>();

    /**
     * Defines an audio format
     */
    AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 2;
        boolean signed = true;
        boolean bigEndian = true;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                channels, signed, bigEndian);
        return format;
    }

    /**
     * Captures the sound and record into a WAV file
     */
    void start() {

        Thread player = new Thread(new Runnable() {
            public void run() {
                play();

//                recorder.play(ais);
            }
        });
        player.setDaemon(true);
        player.start();

        try {
            AudioFormat format = getAudioFormat();
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

            // checks if system supports the data line
            if (!AudioSystem.isLineSupported(info)) {
                System.out.println("Line not supported");
                System.exit(0);
            }
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(format);
            line.addLineListener(new LineListener() {
                @Override
                public void update(LineEvent event) {
                    System.out.println("event.getType()  " + event.getType());
                }
            });
            line.start();   // start capturing

            System.out.println("Start capturing...");

            ais = new AudioInputStream(line);

            long currentTime = System.currentTimeMillis();
            while (line.isOpen()) {
                byte[] tempBytes = ais.readNBytes(4096);
                audioBytes.add(tempBytes);
                System.out.println("tempBytes " + tempBytes + "  " + (System.currentTimeMillis() - currentTime));
                currentTime = System.currentTimeMillis();

            }

//            System.out.println("Start recording...");
//            
// 
//            // start recording
//            AudioSystem.write(ais, fileType, wavFile);
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(JavaSoundRecorder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void play() {
//        File audioFile = new File(audioFilePath);
        final AudioInputStream stream = new AudioInputStream(
                new DynamicInputStream(audioBytes),
                getAudioFormat(),
                64000
        );

        System.out.println("play begining");

        try {
//            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);

            AudioFormat format = stream.getFormat();

            DataLine.Info info = new DataLine.Info(Clip.class, format);

            final Clip audioClip = (Clip) AudioSystem.getLine(info);

            audioClip.addLineListener(new LineListener() {
                @Override
                public void update(LineEvent event) {
                    System.out.println("event " + event);
                    if (event.getType() == LineEvent.Type.START) {
                        System.out.println("audio clip");
                    }
                    if (event.getType() == LineEvent.Type.STOP) {
                        play();
                        audioClip.close();
                    }
                    if (event.getType() == LineEvent.Type.CLOSE) {
                        
                    }
                }
            });
            System.out.println("play before open");
            audioClip.open(stream);
            System.out.println("play clip before start");
            audioClip.start();

            System.out.println("play clip start");

        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }

    }

    /**
     * Listens to the START and STOP events of the audio line.
     */
    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
            System.out.println("Playback started.");

        } else if (type == LineEvent.Type.STOP) {
//            playCompleted = true;
            System.out.println("Playback completed.");
        }

    }

    /**
     * Closes the target data line to finish capturing and recording
     */
    void finish() {
        line.stop();
        line.close();
        System.out.println("Finished");
    }

    /**
     * Entry to run the program
     */
    public static void main(String[] args) {
        final JavaSoundRecorder recorder = new JavaSoundRecorder();

        // creates a new thread that waits for a specified
        // of time before stopping
        // start recording
        recorder.start();
    }
}
