/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.events;

import java.io.Serializable;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author priyadarshi
 */
@ToString
@Slf4j
public class MouseUserInputEvent implements Serializable {

    public MouseUserInputEventType type;
    public int x;
    public int y;
    public int scrollTicks;
    public MouseButton button;

    public MouseUserInputEvent(MouseEvent me) {
        button = me.getButton();
        if (me.getEventType() == MouseEvent.MOUSE_PRESSED) {
            type = MouseUserInputEventType.MOUSE_PRESSED;
            button = me.getButton();
        } else if (me.getEventType() == MouseEvent.MOUSE_RELEASED) {
            type = MouseUserInputEventType.MOUSE_RELEASED;
        } else if (me.getEventType() == MouseEvent.MOUSE_MOVED || me.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            type = MouseUserInputEventType.MOUSE_MOVED;
        } else {
            System.out.println("me= " + me);
        }
    }

//    public int getButtonCode() {
//        if (button == MouseButton.PRIMARY) {
//            return Mouse
//        }
//    }
}
