/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.capture.fx;

import java.nio.ByteBuffer;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.WritableImage;
import javafx.scene.robot.Robot;
import javafx.stage.Screen;
import uno.anahata.satgyara.client.util.FxUtils;
import uno.anahata.satgyara.desktop.capture.AbstractScreenRecorder;

/**
 *
 * @author priyadarshi
 */
public class FxScreenRecorder2 extends AbstractScreenRecorder<FxScreenCapture>{
    private Screen screen;
    
    private Robot fxRobot;
    
    public FxScreenRecorder2(Screen screen) {
        super();
        this.screen = screen;
        setName(getName() + "-" + FxUtils.getScreenId(screen));
    }
    
    @Override
    public FxScreenCapture process(Void v) {        
        return doCaptureScreen();
        //return FxUtils.runAndWait(() -> doCaptureScreen());
    }
    
    
    private WritableImage writableImage;
    
    private FxScreenCapture doCaptureScreen() {        
        if (fxRobot == null) {
            fxRobot = FxUtils.runAndWait(Robot::new);
        }
        
        if (screen == null) {
            screen = Screen.getPrimary();
        }
        
//        PixelBuffer pb = new PixelBuffer(MIN_PRIORITY, , t, pf);
//        WritableImage target = new WritableImage();
        
        Rectangle2D bounds = screen.getBounds();
        writableImage = fxRobot.getScreenCapture(writableImage, bounds);
        

        //PixelBuffer pb = new PixelBuffer((int)writableImage.getWidth() * (int)writableImage.getHeight());
        return new FxScreenCapture(screen, bounds, writableImage, fxRobot.getMousePosition());
    }

    
    public static void main(String[] args) {
        Platform.startup(()->new FxScreenRecorder2(null).start());
    }
}
