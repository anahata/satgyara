/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.desktop.capture.awt;

import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import java.awt.image.BufferedImage;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.common.model.Size;
import org.jcodec.scale.AWTUtil;
import org.jcodec.scale.BaseResampler;
import org.jcodec.scale.BicubicResampler;
import uno.anahata.satgyara.desktop.video.AbstractVideoStreamer;
import uno.anahata.satgyara.desktop.capture.awt.AWTScreenCapture;
import uno.anahata.satgyara.desktop.capture.awt.AWTScreenRecorder;
import uno.anahata.satgyara.desktop.capture.picture.PictureScreenCapture;
import uno.anahata.satgyara.desktop.capture.picture.PictureScreenCapture;

/**
 *
 * @author priyadarshi
 */
public class AWTCaptureToH264PictureCaptureTransformerThread extends AbstractProcessorThread<AWTScreenCapture, PictureScreenCapture> {

    private final AWTScreenRecorder recorder;    
    private final AbstractVideoStreamer encoder;
    private AWTScreenCapture last;

    public AWTCaptureToH264PictureCaptureTransformerThread(AWTScreenRecorder recorder, AbstractVideoStreamer encoder, int queueSize) {
        super(0, 1);
        this.recorder = recorder;
        this.encoder = encoder;
        inQueue = recorder.getOutQueue();
    }
    
    

    @Override
    public PictureScreenCapture process(AWTScreenCapture asc) throws Exception {
        BufferedImage capture = asc.getCapture();
        Size originalSize = new Size(capture.getWidth(), capture.getHeight());
        Picture originalPicture = AWTUtil.fromBufferedImage(capture, ColorSpace.YUV420J);

        Dimension2D targetDimension = encoder.getTargetDimension();
        
        int height = (int) targetDimension.getHeight();
        int width = (int) targetDimension.getWidth();
        if (height %2 ==1) {
            height++;//add an extra pixel height to meet YUV420J requirements
        }
        
        Picture scaledPicture = Picture.create(width, height, originalPicture.getColor());
        
        BaseResampler resampler = new BicubicResampler(originalSize,  new Size(width, height));
        resampler.resample(originalPicture, scaledPicture);
        
        //System.out.println("Resizer thread " + originalSize.getWidth() + " -> " + scaledSize.getWidth());
        
        PictureScreenCapture psc = new PictureScreenCapture(
                asc, 
                asc.getScreenId(), 
                asc.getScreenBounds(), 
                scaledPicture, 
                targetDimension, 
                asc.getMousePosition()
        );
        
        return psc;
    }
    
}
