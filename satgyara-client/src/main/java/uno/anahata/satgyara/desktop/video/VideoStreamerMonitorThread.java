/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.desktop.video;

import java.util.concurrent.BlockingQueue;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.io.FileUtils;
import uno.anahata.satgyara.client.util.FxUtils;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.desktop.video.diff.DiffFrame;
import uno.anahata.satgyara.desktop.video.diff.DiffPacket;
import uno.anahata.satgyara.transport.packet.PacketTransport;
import uno.anahata.satgyara.transport.packet.SerializedPacket;
import uno.anahata.satgyara.transport.tcp.SocketWriter;
import uno.anahata.satgyara.transport.tcp.TcpConnection;

/**
 *
 * @author priyadarshi
 */
public class VideoStreamerMonitorThread extends AbstractProcessorThread<SerializedPacket, Void> {

    private Stage monitor;
    private ImageView ivRed;
    private ImageView ivGreen;
    private ImageView ivBlue;
    private WritableImage imageRed;
    private WritableImage imageGreen;
    private WritableImage imageBlue;
    private AbstractVideoStreamer streamer;

    private Label receiverLag;
    private Label inFlight;
    private Label packetSize;
    private Label screenRecorderCPS;
    private Label receiverTPS;

    private Label recorderTPS;
    private Label encoderTPS;
    private Label scalerTPS;
    private Label streamerPPS;
    private Label serializerTPS;
    private Label socketWriterTPS;
    private Label socketWriterMbps;
    private Label totalSentMb;
    private Label pctChange;
    private Label bitsPerColor;
    private Label effectiveDimension;
    private Label scaleMethod;

    public VideoStreamerMonitorThread(AbstractVideoStreamer streamer, BlockingQueue<SerializedPacket> inQueue) {
        super(inQueue, null);
        this.streamer = streamer;
    }

    @Override
    public Void process(SerializedPacket lastSent) throws Exception {

        if (lastSent.packet instanceof DiffPacket) {

            var dp = (DiffPacket) lastSent.packet;

            if (dp.frame.isEncodable()) {//not just mouse moves

                if (imageRed == null || dp.width != imageRed.getWidth() || dp.height != imageRed.getHeight()) {
                    imageRed = new WritableImage(dp.width, dp.height);
                    ivRed.setImage(imageRed);
                    imageGreen = new WritableImage(dp.width, dp.height);
                    ivGreen.setImage(imageGreen);
                    imageBlue = new WritableImage(dp.width, dp.height);
                    ivBlue.setImage(imageBlue);
                }
                
                if (dp.frame.getPlanes().size() == 1) {
                    //dp.frame.getPlanes().get(0).writePlane(0, imageBlue);
                    //dp.frame.getPlanes().get(0).writePlane(1, imageGreen);
                    //dp.frame.getPlanes().get(0).writePlane(2, imageRed);
                } else {
                    //dp.frame.getPlanes().get(0).writePlane(0, imageBlue);
                    //dp.frame.getPlanes().get(1).writePlane(1, imageGreen);
                    //dp.frame.getPlanes().get(2).writePlane(2, imageRed);
                }

                
            }
            
            int pc = 100;
            
            if (dp.frame instanceof DiffFrame) {
                pc = (int)((DiffFrame)dp.frame).getNoisePct();
            }
            int pctChangeFinal = pc;

            final String ps = FileUtils.byteCountToDisplaySize(lastSent.data.length);
            final long ifp = streamer.getInFlightPackets();
            final long rl = streamer.getReceiverLag();
            final long recPps = streamer.getReceiverTps();

            final long recorderTps = (int) streamer.getScreenRecorder().getTps().getTicksPerSecond();
            final long encoderTps = (int) streamer.getCaptureEncoder().getTps().getTicksPerSecond();
            final long scalerTps = (int) streamer.getCaptureScaler().getTps().getTicksPerSecond();
            final long streamerPps = (int) streamer.getTps().getTicksPerSecond();
            PacketTransport transport = ((PacketTransport) streamer.getTransport());
            final long serializerTps = (int) transport.getSerializer().getTps().getTicksPerSecond();

            TcpConnection tcpConn = (TcpConnection) transport.getConnections().get(0);
            SocketWriter writer = tcpConn.getWriter();

            //final long serializerTps = streamer.getTransport()
            final long bpc = dp.frame.getBitsPerByte();
            final long width = dp.width;
            final long height = dp.height;

            FxUtils.runAndWait(() -> {

                //screenRecorderCPS.setText(string);
                packetSize.setText("Packet Size: " + ps);
                inFlight.setText("In Flight Packets : " + ifp);
                receiverLag.setText("Lag: " + rl);
                receiverTPS.setText("Receiver TPS: " + recPps);

                recorderTPS.setText("Recorder TPS: " + recorderTps);
                encoderTPS.setText("Encoder TPS: " + encoderTps);
                scalerTPS.setText("Scaler TPS: " + scalerTps + " \n\t Method: " + streamer.getCaptureScaler().getPixelScaler().getClass().getSimpleName() + "");
                streamerPPS.setText("Streamer TPS: " + streamerPps);
                serializerTPS.setText("Serializer TPS: " + serializerTps);
                socketWriterTPS.setText("Socket Writer TPS: " + writer.getTps().getTicksPerSecondBigDecimal(1) + " (" + writer.getTps().getContiniousTicksPerSecondBigDecimal(1) + ")");
                socketWriterMbps.setText("Socket Writer \n\t Packet Write: " + writer.getPacketWriteSpeedMbpsFormatted() + " Mbps "
                        + "\n\t  Avg Upload: " + writer.getAverageUploadSpeedMbpsFormatted() + " Mbps "
                        + "\n\t Avg Write: " + writer.getAverageWriteSpeedMbpsFormatted() + " Mpbs ");
                totalSentMb.setText("Total Sent: " + writer.getTotalMBSent() + " MB");
                this.pctChange.setText("% change: " + pctChangeFinal);
                bitsPerColor.setText("Bits Per Color: " + bpc);
                effectiveDimension.setText("Capture Size" + width + "x" + height);
                packetSize.setText("Packet Size: " + ps);
                monitor.sizeToScene();

                return null;
            });
            //}

        }

        return null;

    }

    @Override
    protected void onStartup() throws Exception {
        super.onStartup();

        Platform.runLater(() -> {
            initMonitor();
        });

    }

    private void initMonitor() {

        VBox root = new VBox();

        ivRed = new ImageView();
        ivRed.setFitWidth(300);
        ivRed.setPreserveRatio(true);
        ivRed.setSmooth(false);

        ivGreen = new ImageView();
        ivGreen.setFitWidth(300);
        ivGreen.setPreserveRatio(true);
        ivGreen.setSmooth(false);

        ivBlue = new ImageView();
        ivBlue.setFitWidth(300);
        ivBlue.setPreserveRatio(true);
        ivBlue.setSmooth(false);

        Scene s = new Scene(root);
        VBox stats = new VBox();

        packetSize = new Label();
        inFlight = new Label();
        receiverLag = new Label();
        receiverTPS = new Label();

        recorderTPS = new Label();
        encoderTPS = new Label();
        scalerTPS = new Label();
        streamerPPS = new Label();
        serializerTPS = new Label();
        socketWriterTPS = new Label();
        socketWriterMbps = new Label();
        totalSentMb = new Label();
        screenRecorderCPS = new Label();
        pctChange = new Label();
        bitsPerColor = new Label();
        effectiveDimension = new Label();
        scaleMethod = new Label();

        stats.getChildren().addAll(
                packetSize,
                inFlight,
                receiverLag,
                receiverTPS,
                new Label(),
                recorderTPS,
                encoderTPS,
                scalerTPS,
                streamerPPS,
                serializerTPS,
                socketWriterTPS,
                socketWriterMbps,
                totalSentMb,
                pctChange,
                bitsPerColor,
                effectiveDimension,
                scaleMethod);

        root.getChildren().add(stats);
        root.getChildren().add(ivBlue);
        root.getChildren().add(ivGreen);
        root.getChildren().add(ivRed);

        monitor = new Stage(StageStyle.TRANSPARENT);
        monitor.setX(Screen.getPrimary().getBounds().getMaxX() - 300);
        monitor.setY(0);
        monitor.setScene(s);
        monitor.show();
        monitor.sizeToScene();
        monitor.setAlwaysOnTop(true);

    }

}
