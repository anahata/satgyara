
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import uno.anahata.satgyara.desktop.capture.AbstractScreenCapture;
import uno.anahata.satgyara.desktop.capture.awt.AWTScreenRecorder;
import uno.anahata.satgyara.desktop.capture.awt.CaptureEncoder;
import uno.anahata.satgyara.desktop.video.diff.Frame;
import uno.anahata.satgyara.desktop.video.diff.MasterFrame;


/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * @author priyadarshi
 */
public class DiffFrameTest {
    public static void main2(String[] args) throws Exception{
        AWTScreenRecorder rec = new AWTScreenRecorder();
        
        CaptureEncoder ce = new CaptureEncoder();
        ce.setInQueue((BlockingQueue)rec.getOutQueue());
        rec.start();
        ce.start();
        
        while (true) {
            AbstractScreenCapture asc = ce.getOutQueue().take();
            
            int bbc = 5;
            //byte mask = DiffFrame2.MASK[bbc];
            
            MasterFrame df = new MasterFrame(asc.getBuffer(), 5);
            //byte bgra = df.get
            long ts = System.currentTimeMillis();
            df.finishEncoding();
            
            ts = System.currentTimeMillis() - ts;
            System.out.println("Total " + ts + " size: " + (df.getCompressedPlanesSize() / 1024) + " kb");
        }
        
        
        
        /*
        Random r = new Random(108);
        int len = 1480 * 900 * 4;    
        byte[] barr = new byte[len];
        r.nextBytes(barr);
*/
        
  
        
    }

    public static void main(String[] args) throws Exception{
        byte[] barr = new byte[] {
            (byte)0x10,//b
            (byte)0x20,//g
            (byte)0x30,//r
            (byte)0xFF,//a
            
            (byte)0x40,//b
            (byte)0x50,//g
            (byte)0x60,//r
            (byte)0xFF,//a
            
            (byte)0x70,//b
            (byte)0x80,//g
            (byte)0x90,//r
            (byte)0xFF,//a
            
            (byte)0x10,//b
            (byte)0x20,//g
            (byte)0x30,//r
            (byte)0xFF,//a
            
            (byte)0x40,//b
            (byte)0x50,//g
            (byte)0x60,//r
            (byte)0xFF,//a
            
            (byte)0x70,//b
            (byte)0x80,//g
            (byte)0x90,//r
            (byte)0xFF,//a
        };
        
        int bbc = 5;
        byte mask = Frame.MASK[bbc];
        MasterFrame mf = new MasterFrame(ByteBuffer.wrap(barr), 5);
        mf.finishEncoding();
        
        //last 3 of first byte
        //00100-010 00-100000
        //00100-010 10-100000
        
        for (int i = 0; i <3; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j< mf.getPlanes().get(i).decoded.length; j++) {
                sb.append(format(mf.getPlanes().get(i).decoded[j]));
                sb.append(" ");
            }
            System.out.println(sb.toString());
            
        }        
        
        mf.finishDecoding();
//        byte[] decoded = mf
//        System.out.println("Decoded = " + decoded);
//        for (int i = 0; i < barr.length; i++) {
//            byte b1 = barr[i];
//            byte b2 = decoded[i];
//            System.out.println(((b1 & mask) == (b2 & mask)) + " " + format(b1) + " " + format(b2));
//        }
        
        
        //df.finishTasks();
    }
    
    public static String format(byte b1) {
        String formatted1 = String.format("%8s", Integer.toBinaryString(b1 & 0xFF)).replace(' ', '0');
        return formatted1;
    }
    
    
}
