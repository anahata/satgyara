/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.ping;

import static java.lang.Math.log;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.Tps;
import uno.anahata.satgyara.peer.RemotePeer;

/**
 *
 * @author priyadarshi
 */
@Setter
@Getter
@Slf4j
public class PingClient extends Thread {

    private final RemotePingPeerlet server;
    private final Tps tps = new Tps();
    @Getter
    private double up;
    @Getter
    private double down;
    @Getter
    private double total;

    public PingClient(RemotePeer peer) {
        this.server = peer.getPeerletService().createClient(RemotePingPeerlet.class);
        setName("PingClient|" + peer);
    }

    @Override
    public void run() {
        while (!isInterrupted()) {

            try {
                Thread.sleep(5000);
            } catch (Exception e) {
            }
            
            tps.startTick();
            Date pingSent = new Date();
            Date pingReceived = server.ping(pingSent);
            Date pongReceived = new Date();
            up = pingReceived.getTime() - pingSent.getTime();
            down = pongReceived.getTime() - pingReceived.getTime();
            total = up + down;
            log.debug("Ping {} ms. ({} ms. up + {} ms. down)",
                    up,
                    down,
                    total);
            tps.endTick();
        }
        
        
    }
}
