/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.peer;

import uno.anahata.satgyara.transport.tcp.TcpServerSocketListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.transport.udp.UdpServerSocketListener;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class LocalPeer extends Peer {

    protected static LocalPeer INSTANCE;

    private static final String[] CHECK_IP_URLS = new String[]{
        "http://checkip.amazonaws.com/"
    };

    @Getter
    private final TcpServerSocketListener tcpServerSocketListener;
    private final UdpServerSocketListener udpServerSocketListener;
    
    @Getter
    private final List<InboundPeer> inbound = Collections.synchronizedList(new ArrayList<>());
    
    @Getter
    private final List<OutboundPeer> outbound = Collections.synchronizedList(new ArrayList<>());

    public static LocalPeer getInstance() {
        if (INSTANCE == null) {
             INSTANCE = new LocalPeer();
        }
        return INSTANCE;
    }

    protected LocalPeer() {
        super(UUID.randomUUID());
        
        tcpServerSocketListener = new TcpServerSocketListener();
        tcpServerSocketListener.start();
        
        udpServerSocketListener = new UdpServerSocketListener();
        //udpServerSocketListener.start();
        INSTANCE = this;
        
        log.debug("{}", this);
    }
    
    
    public List<OutboundPeer> getOutbound() {
        return outbound;
    }


    public List<RemotePeer> getAllPeers() {
        List<RemotePeer> l = new ArrayList<>(outbound);
        l.addAll(inbound);
        for (OutboundPeer outboundPeer : outbound) {
            l.addAll(outboundPeer.getRelayedPeers());
        }
        return l;
    }
    
    public RemotePeer getPeer(UUID uuid) {
        for (RemotePeer remotePeer : getAllPeers()) {
            if (remotePeer.getUuid().equals(uuid)) {
                return remotePeer;
            }
        }
        return null;
    }
    
    public InboundPeer getInbound(UUID uuid) {
        synchronized (inbound) {
            for (InboundPeer rp : inbound) {
                if (rp.getUuid().equals(uuid)) {
                    return rp;
                }
            }
        }
        return null;
    }
    
    public TcpServerSocketListener getTcpServerSocketListener() {
        return tcpServerSocketListener;
    }
    
    /**
     * Get the external IP address. If it can't be found, return a null. This
     * makes calls to external web sites to do the check.
     *
     * @return The external IP address.
     */
    public static String getExternalIpAddress() {
        for (String urlName : CHECK_IP_URLS) {

            try (BufferedReader br = new BufferedReader(new InputStreamReader(new URL(urlName).openStream()))) {
                return br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                // Ignore.
            }
        }

        return null;
    }

    public static void main(String[] args) {
        //Application.launch(Main.class);
        try {
            LocalPeer.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public String getExternalIp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
