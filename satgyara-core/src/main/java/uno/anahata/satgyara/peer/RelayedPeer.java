/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peer;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.transport.packet.PacketTransport;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.ConnectionPool;
import uno.anahata.satgyara.transport.tcp.TcpConnection;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class RelayedPeer extends RemotePeer{

    @Getter
    private OutboundPeer relayor;
    
    @Getter 
    private boolean inbound = false;
    
    public RelayedPeer(UUID uuid, OutboundPeer relayor, Transport coreTransport) throws Exception {
        super(uuid);
        this.relayor = relayor;        
        this.tcpConnectionPool = relayor.tcpConnectionPool;
        relayor.getRelayedPeers().add(this);                
        super.coreTransport = coreTransport;
        createTcpConnection(coreTransport.getId());
        super.initCoreServices();
    }
    
    public RelayedPeer(UUID uuid, OutboundPeer relayor, UUID coreTransportUUID) throws Exception {
        this(uuid, relayor, new PacketTransport(coreTransportUUID, relayor.getTcpConnectionPool()));
        inbound = true;
    }

    @Override
    public TcpConnection createTcpConnection(UUID forTransportId) throws Exception {
        return relayor.createTcpConnection(forTransportId);
    }
    
    public ConnectionPool getConnectionPool() {
        return relayor.getTcpConnectionPool();
    }
    
    @Override
    public PacketTransport createPacketTransport(UUID uuid) {
        PacketTransport pt = new PacketTransport(uuid, relayor.getTcpConnectionPool());
        super.transports.put(pt.getId(), pt);
        try {
            relayor.getRemoteRelay().createRelayedPacketTransport(uuid, getUuid());
        } catch (Exception e) {
            log.error("Exception creating relayed transport", e);
            relayor.transports.remove(uuid);
            transports.remove(uuid);
        }
        
        
        return pt;
    }
    
    
    @Override
    protected void unregister() {
        relayor.getRelayedPeers().remove(this);
    }
    
    public static void main(String[] args) throws Exception {
        InetAddress localhost = Inet4Address.getLocalHost();
        InetSocketAddress sa = new InetSocketAddress("localhost", 11711);
        OutboundPeer p2 = new OutboundPeer(sa);        
        System.out.println("sleeping 5 secs");
        Thread.sleep(5000);        
        System.out.println("Getting inbound peers");
        Set<UUID> inboundPeers = p2.getRemoteRelay().getInboundPeers();
        inboundPeers.remove(LocalPeer.INSTANCE.getUuid());        
        System.out.println("Got inboundPeers = " + inboundPeers);
        Thread.sleep(5000);        
        System.out.println("Calling relayTo= " + inboundPeers);
        RelayedPeer rp = p2.relayTo(inboundPeers.iterator().next());
        System.out.println("Got relayed peer " + rp);
        rp.pingClient.start();
        
    }
}
