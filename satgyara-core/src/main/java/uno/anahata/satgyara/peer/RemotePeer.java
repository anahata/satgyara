/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peerlet.PeerletService;
import uno.anahata.satgyara.ping.PingClient;
import uno.anahata.satgyara.transport.packet.PacketTransport;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.tcp.TcpConnection;
import uno.anahata.satgyara.transport.tcp.TcpConnectionPool;
import uno.anahata.satgyara.transport.TransportPeerlet;
import uno.anahata.satgyara.relay.RemoteRelayPeerlet;
import uno.anahata.satgyara.transport.RemoteTransportPeerlet;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class RemotePeer extends Peer {

    public static final UUID CORE_RPC_TRANSPORT_UUID = new UUID(0, 0);

    /**
     * Holds all TcpConnections to this peer
     */
    @Getter
    public TcpConnectionPool tcpConnectionPool;

    /**
     * All transports with this peer
     */
    @Getter
    protected Map<UUID, Transport> transports = new HashMap<>();

    @Getter
    protected Transport coreTransport;

    //@Getter
    //protected Transport relayTransport = new PacketTransport(CORE_RPC_TRANSPORT_UUID, this.tcpConnectionPool);
    @Getter
    protected PeerletService peerletService;

    @Getter
    protected RemoteRelayPeerlet remoteRelay;

    @Getter
    protected RemoteTransportPeerlet remoteTransport;

    @Getter
    protected PingClient pingClient;

    protected RemotePeer(UUID uuid) {
        super(uuid);
    }

    public void registerTransport(Transport t) {
        transports.put(t.getId(), t);
    }

    public void unregisterTransport(Transport t) {
        log.debug("{} unregistering {}", this, t);
        transports.remove(t.getId(), t);
        if (t.getId() == CORE_RPC_TRANSPORT_UUID) {
            log.debug("{} unregistered core transport, calling disconnect() ", this);
            disconnect();
        }
    }

    public void disconnect() {
        log.debug("{} Disconnecting", this);
        for (Transport t : transports.values()) {
            try {
                t.close();
            } catch (Exception e) {
                log.error("Exception shutting down transport " + t, e);
            }
        }
        log.debug("{} Disconnected, unregistering peer", this);
        unregister();
        
    }
    
    protected abstract void unregister();

    public final void initCoreServices() throws Exception {
        if (peerletService != null) {
            throw new IllegalStateException("PeerletService already created " + peerletService);
        }
        //at this point in time the tcpConenctionPool shuold have exactly one idle connection
        //in bothe the inbound and outbound peers        

        //start the Peerlet service
        peerletService = new PeerletService(coreTransport, this);
        peerletService.start();
        //create the transport service proxy
        remoteRelay = peerletService.createClient(RemoteRelayPeerlet.class);
        //now the outbound can add a second connection to the rpc transport 

        pingClient = new PingClient(this);

        remoteTransport = peerletService.createClient(RemoteTransportPeerlet.class);
        pingClient.start();
    }

    public abstract TcpConnection createTcpConnection(UUID forTransportId) throws Exception;

    public PacketTransport createPacketTransport(UUID uuid) {
        PacketTransport pt = new PacketTransport(uuid, tcpConnectionPool);
        remoteTransport.createPacketTransport(uuid);
        return pt;
    }

}
