/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peer;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.transport.packet.PacketTransport;
import uno.anahata.satgyara.transport.tcp.OutboundTcpConnection;
import uno.anahata.satgyara.transport.tcp.TcpConnectionPool;
import uno.anahata.satgyara.relay.RemoteRelayPeerlet;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class OutboundPeer extends RemotePeer{
    protected SocketAddress tcpServerSocketAddress;
    protected SocketAddress udpServerSocketAddress;
    
    @Getter
    protected RemoteRelayPeerlet remoteRelay;
    
    @Getter
    private List<RelayedPeer> relayedPeers = new ArrayList<RelayedPeer>();
    
    public OutboundPeer(SocketAddress sa) throws Exception {
        super(null);
        this.tcpServerSocketAddress = sa;
        tcpConnectionPool = new TcpConnectionPool(this);
        coreTransport = new PacketTransport(CORE_RPC_TRANSPORT_UUID, this.tcpConnectionPool);
        createTcpConnection(CORE_RPC_TRANSPORT_UUID);
        createTcpConnection(CORE_RPC_TRANSPORT_UUID);
        //coreTransport.addFromPool();
        LocalPeer.getInstance().getOutbound().add(this);
        initCoreServices();
        
        remoteRelay = peerletService.createClient(RemoteRelayPeerlet.class);
        
        //chock another connection for the core transport
        
        
        //and a couple of idle ones for relaying, etc
        //createTcpConnection(null);
        //createTcpConnection(null);
        
        //createNewConnectionForTransport(coreTransport);
    }
    
    public RelayedPeer getRelayedPeer(UUID uuid) {
        for (RelayedPeer rp: relayedPeers) {
            if (rp.getUuid().equals(uuid)) {
                return rp;
            }
        }
        return null;
    }
    
    public Set<UUID> getInboundPeers() {
        return remoteRelay.getInboundPeers();
    }
    
    public RelayedPeer relayTo(UUID peer) throws Exception {
        
        PacketTransport relayedPeerCoreTransport = new PacketTransport(UUID.randomUUID(), tcpConnectionPool);
        log.debug("Requesting remote peer to relay {} to {}", relayedPeerCoreTransport, peer);
        remoteRelay.relayTo(relayedPeerCoreTransport.getId(), peer);
        log.debug("remote peer created relay for {} to {}", relayedPeerCoreTransport, peer);
        
        return new RelayedPeer(peer, this, relayedPeerCoreTransport);
    }
    
    
//    private void createNewConnectionForTransport(Transport t) throws Exception{
//        //add a second connection to the rpc service
//        //we create the connection 
//        TcpConnection conn = createTcpConnection();
//        //connection stablished, idle on the tcp connection pool so we inform 
//        //the other end to bind this new connection to the rpc transport
//        t.addFromPool(conn.getId());
//        try {
//            remoteTransportService.allocateConnection(conn.getId(), t.getId());
//        } catch (Exception e) {
//        }
//        
//        //we do the same thing here
//        
//        //now the rpc service should have 2 connections on both ends
//    }
    
    @Override
    public final OutboundTcpConnection createTcpConnection(UUID targetTransport) throws Exception {
        log.debug("Creating outbound tcp connection to {} for target Transport {}", tcpServerSocketAddress, targetTransport);
        OutboundTcpConnection conn = new OutboundTcpConnection(this, tcpServerSocketAddress, targetTransport);
        log.debug("CREATED outbound tcp connection {} for {} calling ini()", conn, targetTransport);
        conn.init();
        return conn;
    }
    
    
    
    @Override
    protected void unregister() {
        log.debug("{} unregistering", this);
        LocalPeer.getInstance().getOutbound().remove(this);
        log.debug("{} unregistered. outboundPeers: " + LocalPeer.getInstance().getOutbound());
        for (RelayedPeer rp: new ArrayList<>(relayedPeers)) {
            log.debug("{} unregistering relayed peer {}" , this, rp);
            rp.unregister();
            log.debug("{} unregistered relayed peer {}" , this, rp);
        }
    }
    
    
    
    public static void main(String[] args) throws Exception {
        LocalPeer.getInstance();
        InetAddress localhost = Inet4Address.getLoopbackAddress();
        InetSocketAddress sa = new InetSocketAddress("localhost", 11711);
        OutboundPeer p1 = new OutboundPeer(sa);
        //p1.getPingClient().start();
    }
}
