/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.peer;

import java.io.Serializable;
import java.net.SocketAddress;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import static uno.anahata.satgyara.seq.UUIDUtils.tail;

/**
 *
 * @author priyadarshi
 */
@Getter
@Setter
public abstract class Peer implements Serializable{
    private UUID uuid;
    private String nickName;
    protected SocketAddress tcpServerSocketAddress;
    protected SocketAddress udpServerSocketAddress;

    public Peer() {
    }

    public Peer(UUID uuid) {
        this.uuid = uuid;
    }
    
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + "-" + tail(uuid);
    }
    
}
