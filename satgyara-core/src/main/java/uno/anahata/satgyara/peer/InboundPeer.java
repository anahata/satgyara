/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peer;

import java.util.UUID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import static uno.anahata.satgyara.peer.RemotePeer.CORE_RPC_TRANSPORT_UUID;
import uno.anahata.satgyara.transport.packet.PacketTransport;
import uno.anahata.satgyara.transport.tcp.InboundTcpConnection;
import uno.anahata.satgyara.transport.tcp.TcpConnection;
import uno.anahata.satgyara.transport.tcp.TcpConnectionPool;
import uno.anahata.satgyara.transport.RemoteTransportPeerlet;
import uno.anahata.satgyara.relay.RemoteRelayPeerlet;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class InboundPeer extends RemotePeer {
    
    public InboundPeer(UUID id) throws Exception {
        super(id);        
        tcpConnectionPool = new TcpConnectionPool(this);
        coreTransport = new PacketTransport(CORE_RPC_TRANSPORT_UUID, this.tcpConnectionPool);
        LocalPeer.getInstance().getInbound().add(this);
        super.initCoreServices();
    }
    
    

    /**
     * Asks the other end to open a connection via {@link RemoteTransportPeerlet}
     * 
     * @param forTransportID
     * @return
     * @throws Exception 
     */
    @Override
    public InboundTcpConnection createTcpConnection(UUID forTransportID) throws Exception {
        UUID uuid = remoteTransport.createConnection(forTransportID);
        return (InboundTcpConnection) getTransports().get(uuid).getSource().getLoaned().get(uuid);
    }
    
    @Override
    protected void unregister() {
        log.debug("{} unregistering", this);
        LocalPeer.getInstance().getInbound().remove(this);
        log.debug("{} unregistered", this);
    }
    

}
