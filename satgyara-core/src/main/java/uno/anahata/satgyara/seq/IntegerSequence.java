/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.seq;

import uno.anahata.satgyara.transport.packet.Packet;

/**
 *
 * @author priyadarshi
 */
public class IntegerSequence extends AbstractSequence<Integer>{

    public IntegerSequence() {
        seqNo = 0;
    }
    
    @Override
    public synchronized Integer nextSeqNo() {
        Integer ret = seqNo;
        if (seqNo == Integer.MAX_VALUE) {
            seqNo = Integer.MIN_VALUE;
        } else {
            seqNo++;
        }
        return ret;
    }
    
}
