/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.seq;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author priyadarshi
 */
public abstract class AbstractSequence<T extends Number> {
    @Getter
    @Setter
    protected T seqNo;

    protected abstract T nextSeqNo();
}
