/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.seq;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

/**
 *
 * @author priyadarshi
 */
public abstract class ObjectRegistry<T extends Number, U extends Object> {
    @Getter
    protected Map<T,U> instances = new HashMap<T,U>();
    @Getter
    protected AbstractSequence<T> seq;

    public ObjectRegistry(AbstractSequence<T> seq) {
        this.seq = seq;
    }
    
    public synchronized T register(U u) {
        T t = seq.nextSeqNo();
        instances.put(t, u);
        return t;
    }
    
    public synchronized U get(T t) {
        return instances.get(t);
    }
    
    public synchronized boolean unregister(T t, U u) {
        return instances.remove(t, u);
    }
}
