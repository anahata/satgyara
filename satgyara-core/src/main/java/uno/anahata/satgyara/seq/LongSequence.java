/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.seq;

import uno.anahata.satgyara.transport.packet.Packet;

/**
 *
 * @author priyadarshi
 */
public class LongSequence extends AbstractSequence<Long>{

    public LongSequence() {
        seqNo = Long.valueOf(0);
    }

    @Override
    public synchronized Long nextSeqNo() {
        Long ret = seqNo;
        if (seqNo == Long.MAX_VALUE) {
            seqNo = Long.MIN_VALUE;
        } else {
            seqNo++;
        }
        return ret;
    }
    
}
