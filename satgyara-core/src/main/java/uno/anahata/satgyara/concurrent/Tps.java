/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.concurrent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections4.queue.CircularFifoQueue;

/**
 *
 * @author priyadarshi
 */
public class Tps<T> {

    public static final long NPS = TimeUnit.SECONDS.toNanos(1);
    private long startTime = System.currentTimeMillis();
    private int totalTicks = 0;
    private long totalTickDurationNanos = 0;
    private int printStatsInterval = 10;
    private String name;
    private Tick<T> currTick;
    private boolean printStats = true;

    private CircularFifoQueue<Tick<T>> fifoQueue;

    public Tps() {
        this(100);
    }
    
    public Tps(String name) {
        this(100);
    }
    
    public Tps(int buffSize) {
        fifoQueue = new CircularFifoQueue(buffSize);
        printStatsInterval = buffSize;
    }

    public boolean isPrintStats() {
        return printStats;
    }

    public void setPrintStats(boolean printStats) {
        this.printStats = printStats;
    }

    public int getTotalTicks() {
        return totalTicks;
    }

    public int getPrintStatsInterval() {
        return printStatsInterval;
    }

    public void setPrintStatsInterval(int printStatsEveryTickCount) {
        this.printStatsInterval = printStatsEveryTickCount;
    }

    public List<Tick<T>> getTicksInBuffer() {
        return new ArrayList(fifoQueue);
    }
    
    
    public synchronized Tick<T> getOldestTick() {
        if (!fifoQueue.isEmpty()) {
            return fifoQueue.get(0);
        } else {
            return null;
        }
    }
    
    public synchronized Tick<T> getLatestTick() {
        if (!fifoQueue.isEmpty()) {
            return fifoQueue.get(fifoQueue.size() - 1);
        } else {
            return null;
        }
    }

    public void startTick() {
        startTick(null);
    }

    public synchronized void startTick(T info) {
        assert (currTick == null);
        currTick = new Tick<>(info);
    }

    public synchronized void endTick() {
        currTick.endTick();
        fifoQueue.add(currTick);
        totalTickDurationNanos += currTick.getTickDurationNanos();
        totalTicks++;
        currTick = null;
        
        if (printStats && totalTicks % printStatsInterval == 0) {
            String thread = Thread.currentThread().getName();
            String namePreffix = name != null ? name + "-" : "";
            System.out.println(getContiniousTicksPerSecondBigDecimal(1) + " (" + getTicksPerSecondBigDecimal(1) + ") tps " + namePreffix + thread + "" );
        }
    }

    public synchronized double getAverageTickDurationNanos() {
        ArrayList<Tick> queueCopy = new ArrayList(fifoQueue);
        long total = 0;
        for (Tick t : queueCopy) {
            total += t.getTickDurationNanos();
        }
        return total / queueCopy.size();
    }
    
    public synchronized double getContiniousTicksPerSecond() {
        ArrayList<Tick> queueCopy = new ArrayList(fifoQueue);
        if (queueCopy.isEmpty()) {            
            return 0;
        } else if (queueCopy.size() == 1) {            
            return (double) NPS / queueCopy.get(0).getTickDurationNanos();
        } else {            
            double totalNanos = 0;
            for (Tick t : queueCopy) {
                totalNanos += t.getTickDurationNanos();
            }
            return NPS / (totalNanos / queueCopy.size());
        }
    }
    
    public synchronized double getTicksPerSecond() {
        ArrayList<Tick> queueCopy = new ArrayList(fifoQueue);
        if (queueCopy.isEmpty()) {            
            return 0;
        } else if (queueCopy.size() == 1) {            
            return (double) NPS / queueCopy.get(0).getTickDurationNanos();
        } else {            
            Tick oldest = getOldestTick();
            Tick latest = getLatestTick();
            double ticks = queueCopy.size();
            double ellapsedNanos = latest.endTs - oldest.startTs;                        
            return NPS / (ellapsedNanos / ticks);
        }
    }
    
    public BigDecimal getTicksPerSecondBigDecimal(int scale) {
        return new BigDecimal(getTicksPerSecond()).setScale(scale, (RoundingMode.HALF_EVEN));
    }
    
    /**
     * Number of ticks per second if the ticks were to be continious (without gaps between ticks).
     * 
     * @param scale
     * @return 
     */
    public BigDecimal getContiniousTicksPerSecondBigDecimal(int scale) {
        return new BigDecimal(getContiniousTicksPerSecond()).setScale(scale, (RoundingMode.HALF_EVEN));
    }

    public class Tick<T> {

        private final long startTime = System.currentTimeMillis();
        private final long startTs = System.nanoTime();
        private long endTs;
        private final T tickInfo;

        public Tick(T tickInfo) {
            this.tickInfo = tickInfo;
        }

        public void endTick() {
            endTs = System.nanoTime();
        }

        public long getTickDurationNanos() {
            return endTs - startTs;
        }

        public double getTickDurationMillis() {
            return getTickDurationNanos() / 1000000;
        }

        @Override
        public String toString() {
            return "Tick{" + getTickDurationNanos() + " nanos " + getTickDurationMillis()+ " ms" + " startTime=" + startTime + ", startTs=" + startTs + ", endTs=" + endTs + ", tickInfo=" + tickInfo + '}';
        }
        
    }
}
