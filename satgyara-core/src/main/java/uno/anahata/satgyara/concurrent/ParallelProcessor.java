/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class ParallelProcessor<I, O> extends AbstractProcessorThread<I, O> {

    private final List<ParallelProcessorWorker> workers = new ArrayList<>();

    private volatile long inSeqNo = 0;

    private volatile long outSeqNo = 0;

    private final int threadCount;

    public ParallelProcessor(int inQueueSize, int outQueueSize, int threadCount) {
        super(inQueueSize, outQueueSize);
        this.threadCount = threadCount;
    }

    public int getThreadCount() {
        return threadCount;
    }
    
    @Override
    protected void onStartup() {
        for (int i = 0; i < threadCount; i++) {
            ParallelProcessorWorker ppw = new ParallelProcessorWorker(i);
            log.trace("onStartup() launcing worker " + ppw);
            workers.add(ppw);
            ppw.start();
        }
    }
    @Override
    protected void doLoop() throws Exception {
        synchronized(this) {
            wait();
        }
    }

    //todo this class shouldn't probably even extend thread
    @Override
    protected void onExit() throws Exception {
        log.debug("Interrupting {} workers ", workers.size());
        for (ParallelProcessorWorker ppw: workers) {
            log.debug("Interrupting {} " + ppw);
            ppw.interrupt();
            log.debug("Interrupted {} " + ppw);
        }
        log.debug("{} workers interrupted " + workers.size());
    }

//    @Override
//    protected void putOutput(O o) throws Exception {
//        //not the most realisitic measure but at least counts number of ticks
//        synchronized(this) {
//            tps.startTick();
//            super.putOutput(o); 
//            tps.endTick();
//        }
//    }
    
    


    /**
     * Thread that does that actual work, inQueue is workersIn and no outQueue as upon completion
     * it gets either added to the ParallelProcessors sendQueue or put in the pendingOut list
     */
    private class ParallelProcessorWorker extends AbstractProcessorThread<I, O> {

        public ParallelProcessorWorker(int id) {
            super(0, 0);
            setName(ParallelProcessor.this.getName() + "|w#" + id);
            tps.setPrintStats(false);
            inQueue = ParallelProcessor.this.inQueue;
            outQueue = ParallelProcessor.this.outQueue;
        }

        @Override
        protected I getInput() throws Exception {
            return ParallelProcessor.this.getInput();
        }
        
        @Override
        public O process(I t) throws Exception {
            O o  = ParallelProcessor.this.process(t);            
            return o;
        }
        
        @Override
        protected void doOutput(O o) throws Exception {
            ParallelProcessor.this.doOutput(o);
        }

        @Override
        protected void onExit() throws Exception {
            super.onExit(); 
            ParallelProcessor.this.interrupt();
        }
        
        
    }

    public static void main(String[] args) {
        final Random r = new Random();
        ParallelProcessor<Integer, String> pp = new ParallelProcessor<Integer, String>(2, 0, 2) {
            @Override
            public String process(Integer i) throws Exception {
                int random = r.nextInt(200);             
                Thread.sleep(random);
                String ret = i + "-" + random;
                System.out.println("processed " + ret);
                return ret;
            }

            @Override
            protected void doOutput(String o) throws Exception {
                System.out.println("Got output " + o);
            }
            
        };
        pp.start();

        int number = 0;
        while (true) {
            try {
                long ts = System.currentTimeMillis();
                number++;
                pp.getInQueue().put(number);
                ts = System.currentTimeMillis() - ts;
                System.out.println("Added " + number + " to inQueue in " + ts + " inQueue: " + pp.getInQueue());
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
