/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.concurrent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class AbstractProcessorThread<I, O> extends Thread {

    @Getter
    @Setter
    protected BlockingQueue<I> inQueue;
    @Getter
    @Setter
    protected BlockingQueue<O> outQueue;
    @Getter
    protected final Tps tps = new Tps();

    /**
     * The last element that was taken from the queue
     */
    @Getter
    private I lastInput;
    /**
     * The last input element that was processed
     */
    @Getter
    private I lastProcessedInput;

    /**
     * The output of the last processed input element
     */
    @Getter
    private O lastProcessedOutput;

    /**
     * The last input element whose output has been through doOutput
     */
    @Getter
    private I lastOutputtedInput;

    /**
     * The last processed element that has been through doOutput
     */
    @Getter
    private O lastOutputtedOutput;

    public AbstractProcessorThread(BlockingQueue<I> inQueue, BlockingQueue<O> outQueue) {
        this.inQueue = inQueue;
        this.outQueue = outQueue;
        setName(getClass().getSimpleName() + "@" + System.identityHashCode(this));
    }

    public AbstractProcessorThread(int inQueueSize, int outQueueSize) {
        this(inQueueSize > 0 ? new LinkedBlockingQueue<I>(inQueueSize) : null,
                outQueueSize > 0 ? new LinkedBlockingQueue<O>(outQueueSize) : null);
    }

    @Override
    public final void run() {
        log.trace(" calling onStartup()");

        try {
            onStartup();
            log.trace(" onStartup completed()");
        } catch (Exception e) {
            log.error(" interrupting due to exception in onStartup()", e);
            interrupt();
        }

        while (!isInterrupted()) {
            try {
                doLoop();
            } catch (Exception e) {
                e.printStackTrace();
                log.error(" interrupting due to exception in doLoop()");
                interrupt();
            }
        }

        log.trace("calling onExit()");
        try {
            onExit();
            log.trace("onExit() completed");
        } catch (Exception e) {
            interrupt();
        }
        
        log.debug("Exited");
    }

    protected void doLoop() throws Exception {
        I input = getInput();
        lastInput = input;
        tps.startTick();
        O output = process(input);
        tps.endTick();
        lastProcessedInput = input;
        lastProcessedOutput = output;

        doOutput(output);

        lastOutputtedInput = input;
        lastOutputtedOutput = output;
    }

    protected void onStartup() throws Exception {
    }

    protected void onExit() throws Exception {

    }

    protected I getInput() throws Exception {
        if (inQueue != null) {
            return inQueue.take();
        }
        return null;
    }

    protected void doOutput(O o) throws Exception {
        if (outQueue != null) {
            outQueue.put(o);
        }
    }

    public abstract O process(I t) throws Exception;

}
