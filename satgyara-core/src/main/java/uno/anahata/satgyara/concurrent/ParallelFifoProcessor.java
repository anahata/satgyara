/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class ParallelFifoProcessor<I, O> extends AbstractProcessorThread<I, O> {

    private final BlockingQueue<SequenceMapping> workersIn;

    private final List<SequenceMapping> awaitingOut = new ArrayList<>();

    private final List<ParallelFifoProcessorWorker> workers = new ArrayList<ParallelFifoProcessorWorker>();

    private volatile long inSeqNo = 0;

    private volatile long outSeqNo = 0;

    private int threadCount;
    
    public ParallelFifoProcessor(int inQueueSize, int outQueueSize, int threadCount) {
        super(inQueueSize, outQueueSize);
        this.threadCount = threadCount;
        this.workersIn = new LinkedBlockingQueue<>(inQueueSize);
    }

    public int getThreadCount() {
        return threadCount;
    }
    
    @Override
    protected void onStartup() {
        log.debug("onStartup() ");
        for (int i = 0; i < threadCount; i++) {
            ParallelFifoProcessorWorker ppw = new ParallelFifoProcessorWorker(i);
            log.trace(getName() + " onStartup() launcing worker " + ppw);
            workers.add(ppw);
            ppw.start();
        }
        log.debug("onStartup() completed");
    }
    
    
    //todo this class shouldn't probably even extend thread
    @Override
    public void onExit() {
        log.debug("Interrupting {} workers ", workers.size());
        for (ParallelFifoProcessorWorker ppw: workers) {
            log.debug("Interrupting {} " + ppw);
            ppw.interrupt();
            log.debug("Interrupted {} " + ppw);
            
        }
        log.debug("{} workers interrupted " + workers.size());
    }
    
    public int getAwaitingOutSize() {
        return awaitingOut.size();
    }
    
    @Override
    protected void doLoop() throws Exception {

        I i = getInput();

        tps.startTick();
        synchronized (workersIn) {
            long seqNo = inSeqNo;
            incInSeqNo();
            SequenceMapping sm = new SequenceMapping(seqNo, i);            
            workersIn.put(sm);
            //System.out.println("Added " + sm + " to workers in " + workersIn);
        }
        tps.endTick();

    }

    private long incInSeqNo() {
        synchronized (workersIn) {
            if (inSeqNo == Long.MAX_VALUE) {
                inSeqNo = Long.MIN_VALUE;
            } else {
                inSeqNo++;
            }
            return inSeqNo;
        }

    }

    private long incOutSeqNo() {
        synchronized (awaitingOut) {
            if (outSeqNo == Long.MAX_VALUE) {
                outSeqNo = Long.MIN_VALUE;
            } else {
                outSeqNo++;
            }
            return outSeqNo;
        }
    }

    private class SequenceMapping {

        private long seq;
        private I input;
        private O output;

        public SequenceMapping(long seq, I input) {
            this.seq = seq;
            this.input = input;
        }

        @Override
        public String toString() {
            return "SequenceMapping{" + "seq=" + seq + ", input=" + input + ", output=" + output + '}';
        }

    }

    /**
     * Thread that does that actual work, inQueue is workersIn and no outQueue as upon completion
     * it gets either added to the ParallelProcessors sendQueue or put in the pendingOut list
     */
    private class ParallelFifoProcessorWorker extends AbstractProcessorThread<SequenceMapping, SequenceMapping> {

        public ParallelFifoProcessorWorker(int idx) {
            super(workersIn, null);
            setName(ParallelFifoProcessor.this.getName() + "|w#" + idx);
            tps.setPrintStats(false);
        }

        @Override
        public SequenceMapping process(SequenceMapping t) throws Exception {
            t.output = ParallelFifoProcessor.this.process(t.input);            
            return t;
        }

        @Override
        protected void doOutput(SequenceMapping o) throws Exception {            
            synchronized (awaitingOut) {
                if (o.seq == outSeqNo) {                    
                    addToSendQueue(o);
                } else {
                    awaitingOut.add(o);
                }
            }
        }

        @Override
        protected void onExit() throws Exception {
            super.onExit(); 
            ParallelFifoProcessor.this.interrupt();
        }
        
        
    }

    private void addToSendQueue(SequenceMapping o) throws Exception {
        synchronized (awaitingOut) {
            awaitingOut.remove(o);//
            incOutSeqNo();
            doOutput(o.output);
            for (SequenceMapping sm : awaitingOut) {
                if (sm.seq == outSeqNo) {
                    addToSendQueue(sm);
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        final Random r = new Random();
        ParallelFifoProcessor<Integer, String> pp = new ParallelFifoProcessor<Integer, String>(2, 0, 2) {
            @Override
            public String process(Integer i) throws Exception {
                int random = r.nextInt(200);             
                Thread.sleep(random);
                String ret = i + "-" + random;
                System.out.println("processed " + ret);
                return ret;
            }

            @Override
            protected void doOutput(String o) throws Exception {
                System.out.println(getName() + " Got " + o + " awaiting out= " + this.getAwaitingOutSize());
            }
        };
        pp.start();

        int number = 0;
        while (true) {
            try {
                long ts = System.currentTimeMillis();
                number++;
                pp.getInQueue().put(number);
                ts = System.currentTimeMillis() - ts;
                System.out.println("Added " + number + " to inQueue in " + ts + " inQueue: " + pp.getInQueue());
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
