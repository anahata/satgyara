/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.rpc;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import uno.anahata.rpc.Rpc;

/**
 *
 * @author priyadarshi
 */
@Getter
@Setter
public class RpcRequestPacket extends RpcPacket{
    
    private Rpc request;
    
    private transient Object instance;

    public RpcRequestPacket(Rpc request, int instanceId) {
        super.instanceId = instanceId;
        this.request = request;
    }

    @Override
    public String toString() {
        return "RpcRequestPacket{" + " instanceId=" + instanceId + ", requestId=" + requestId + " request=" + request + ", instance=" + instance + '}';
    }
    
    
}
