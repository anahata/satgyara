/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.peerlet;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.rpc.RpcPacket;
import uno.anahata.satgyara.rpc.RpcRequestPacket;
import uno.anahata.satgyara.rpc.RpcResponsePacket;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.packet.NullPacket;

/**
 * Provides a server for Peerlets running on this machine and a client 
 * container for generating stubs to remote Peerlets
 * 
 * @author priyadarshi
 */
@Slf4j
public class PeerletService extends AbstractProcessorThread<RpcPacket, Void> {

    @Getter
    private RemotePeer peer;
    
    @Getter
    private Transport transport;
    
    /**
     * Peerlet instances (the actual implementation) that execute code on this machine.
     */
    @Getter
    private PeerletContainer container;
    
    /**
     * Client stubs (proxies) to remote peerlets
     */
    @Getter
    private PeerletClientContainer clientContainer;
    
    
    public PeerletService(Transport transport, RemotePeer peer) throws Exception {        
        super(0,0);
        this.peer = peer;
        this.transport = transport;
        inQueue = transport.getInQueue();
        //no outque as processed packets go either to the client or the server
        container = new PeerletContainer(this);    
        clientContainer = new PeerletClientContainer(this);
        setName("PeerletService|" + transport);        
        log.debug("{} constructed, inQueue={}", this, inQueue);
    }

    @Override
    protected void onStartup() throws Exception {
        log.debug("Starting PeerletContainer {}", container);
        container.start();
    }

    @Override
    protected void onExit() throws Exception {
        super.onExit(); 
        container.interrupt();
    }
    
    
    
    public <T> T createClient(Class<T> type) {
        return clientContainer.create(type);
    }
    
    @Override
    public Void process(RpcPacket t) throws Exception {
        log.debug("Received " + t);
        if (t instanceof RpcRequestPacket) {
            var req = (RpcRequestPacket) t;
            container.getInQueue().put(req);
            log.debug("Added to queue" + t);
        } else if (t instanceof RpcResponsePacket) {
            var respo = (RpcResponsePacket) t;
            PeerletClient proxy = clientContainer.getClient(respo.getInstanceId());
            proxy.reponseReceived(respo);
        } else {
            log.debug("Unexpected packet " + t);
            interrupt();
        }
        return null;
    }

}
