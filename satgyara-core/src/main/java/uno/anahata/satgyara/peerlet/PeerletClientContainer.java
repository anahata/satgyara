/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peerlet;

import java.lang.reflect.Proxy;
import lombok.RequiredArgsConstructor;
import uno.anahata.rpc.client.RpcInvocationHandler;
import uno.anahata.satgyara.seq.IntegerRegistry;
import uno.anahata.satgyara.transport.Transport;

/**
 * Creates an RPC client proxy and mantains a list of all created proxies so
 * responses can get mapped back.
 *
 * @author priyadarshi
 */
@RequiredArgsConstructor
public class PeerletClientContainer {

    private final PeerletService service;
    private final IntegerRegistry<PeerletClient> registry = new IntegerRegistry<>();

    public PeerletClient getClient(int id) {
        return registry.get(id);
    }

    public <T> T create(Class<T> type) {

        PeerletClient client = new PeerletClient(service.getTransport());
        int id = registry.register(client);
        client.setInstanceId(id);

        RpcInvocationHandler handler = new RpcInvocationHandler(type, client);
        client.setHandler(handler);

        @SuppressWarnings("unchecked")
        T proxy = (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{type}, handler);
        client.setProxy(proxy);

        return proxy;
    }

}
