/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.rpc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import uno.anahata.rpc.Rpc;
import uno.anahata.rpc.RpcResponse;

/**
 *
 * @author priyadarshi
 */
@Getter
@Setter
public class RpcResponsePacket extends RpcPacket{
    private RpcResponse response;

    public RpcResponsePacket(int instanceId, int requestId, RpcResponse response) {
        super(instanceId, requestId);
        this.response = response;
    }

    @Override
    public String toString() {
        return "RpcResponsePacket{instanceId=" + instanceId + " requestId=" + requestId + " response=" + response + '}';
    }
    
    
    
}
