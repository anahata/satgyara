/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peerlet;

import java.util.Date;
import java.util.concurrent.TimeoutException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.rpc.Rpc;
import uno.anahata.rpc.RpcResponse;
import uno.anahata.rpc.client.RpcClientTransport;
import uno.anahata.rpc.client.RpcInvocationHandler;
import uno.anahata.satgyara.seq.IntegerRegistry;
import uno.anahata.satgyara.rpc.RpcRequestPacket;
import uno.anahata.satgyara.rpc.RpcResponsePacket;
import uno.anahata.satgyara.transport.Transport;

/**
 *
 * @author priyadarshi
 */
@RequiredArgsConstructor
@Slf4j
public class PeerletClient implements RpcClientTransport {

    /**
     * The satgyara transport to be used for sendReceive.
     */
    private final Transport transport;

    /**
     * Each proxy gets assigned a unique instance id to map the responses back
     * to the proxy instance that originated the rpc call.
     */
    @Getter
    @Setter
    private int instanceId;

    /**
     * The generated dynamic proxy
     */
    @Getter
    @Setter
    private Object proxy;

    /**
     * The rpc invocation handler used by the proxy
     */
    @Getter
    @Setter
    private RpcInvocationHandler handler;

    /**
     * Pending requests for the proxy
     */
    private final IntegerRegistry<PendingRpcRequest> pending = new IntegerRegistry<PendingRpcRequest>();
    
    public void reponseReceived(RpcResponsePacket rrp) {
        PendingRpcRequest prr = pending.get(rrp.getRequestId());
        log.debug("PendingRpcRequest: {}", prr);
        if (prr == null) {
            log.error("Could not find pending rpc request for id {} received response packet: {} pending: {}", rrp.getRequestId(), rrp, pending);
            throw new IllegalStateException("No pending request for received response packet: " + rrp + " all pending:" + pending.getInstances() + " " + System.identityHashCode(pending));
        }
        pending.unregister(rrp.getRequestId(), prr);
        prr.setResponse(rrp.getResponse());
        if (prr.timedOutOn != null) {
            long timeout = prr.timedOutOn.getTime() - prr.createdOn.getTime();
            long responseTime = prr.responseReceivedOn.getTime() - prr.createdOn.getTime();
            log.warn("Response {} received after a {} ms. timeout. took {} ms", rrp.getResponse(), timeout, responseTime);
        }
    }
    
    @Override
    public RpcResponse sendReceive(Rpc req) throws Exception {
        
        RpcRequestPacket rrp = new RpcRequestPacket(req, instanceId);
        rrp.setInstanceId(instanceId);
        
        PendingRpcRequest prr = new PendingRpcRequest(rrp);
        int reqId = pending.register(prr);
        rrp.setRequestId(reqId);
        log.debug("Sending {} via {} pending {}", rrp, transport, System.identityHashCode(pending), pending.getInstances());
        transport.send(rrp);
        return prr.getResponse(10000);
    }
    
    @RequiredArgsConstructor
    private static class PendingRpcRequest {

        private final RpcRequestPacket rrp;
        private RpcResponse response;
        private final Object responseLock = new Object();
        private Date createdOn = new Date();
        private Date timedOutOn;
        private Date responseReceivedOn;
        
        public void setResponse(RpcResponse resp) {
            synchronized (responseLock) {
                responseReceivedOn = new Date();
                response = resp;
                responseLock.notify();
            }
        }
        
        public RpcResponse getResponse(int timeout) throws InterruptedException, TimeoutException {
            synchronized (responseLock) {
                if (response == null) {
                    responseLock.wait(timeout);
                    if (response == null) {
                        timedOutOn = new Date();
                        throw new TimeoutException("Response timed out after " + timeout + " ms." + " for " + rrp);
                    }
                }
                return response;
            }
        }
    }
    
}
