/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.peerlet;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.rpc.RpcError;
import uno.anahata.rpc.RpcResponse;
import uno.anahata.satgyara.concurrent.ParallelProcessor;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.rpc.RpcRequestPacket;
import uno.anahata.satgyara.rpc.RpcResponsePacket;
import uno.anahata.satgyara.transport.Transport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class PeerletContainer extends ParallelProcessor<RpcRequestPacket, RpcResponsePacket> {

    public final Map<Integer, Object> instances = new HashMap<Integer, Object>();

    @Getter
    private PeerletService service;
    
    public PeerletContainer(PeerletService service) {
        super(2, 0, 4);        
        this.service = service;
        outQueue = (BlockingQueue) service.getTransport().getOutQueue();
        setName("PeerletContainer|" + service.getPeer());
    }

    public Object getTargetInstance(RpcRequestPacket p) throws Exception {
        synchronized (instances) {
            Object o = instances.get(p.getInstanceId());
            if (o == null) {
                log.info("Creating peerlet {}", p.getRequest());
                Class iface = p.getRequest().getClazz();
                log.trace("Interface {}", iface);
                String className = iface.getSimpleName().replace("Remote", "");
                String fqClassName = iface.getPackage().getName() + "." + className;
                log.trace("Calculated instance name{}", fqClassName);
                Class type = Class.forName(fqClassName); 
                log.trace("Class {}", type);
                o = type.getDeclaredConstructor().newInstance();
                instances.put(p.getInstanceId(), o);
            }
            return o;
        }
    }

    @Override
    public RpcResponsePacket process(RpcRequestPacket t) throws Exception {
        ;
        RpcResponse response;
        Object target = null;
        try {
            target = getTargetInstance(t);
            log.trace("target instance = " + target);
            PeerletContext ctx = new PeerletContext(t, service);
            PeerletContext.context.set(ctx);
            response = t.getRequest().getResponse(target);
        } catch (Exception e) {
            log.error("Exception getting response from {} for {} for request {}", target, t, e);
            response = new RpcResponse(new RpcError(e));
        } finally {
            PeerletContext.context.remove();
        }
        

        return new RpcResponsePacket(t.getInstanceId(), t.getRequestId(), response);
    }

}
