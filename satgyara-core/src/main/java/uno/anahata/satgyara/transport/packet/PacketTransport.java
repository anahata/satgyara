/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport.packet;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.packet.PacketDeserializer;
import uno.anahata.satgyara.transport.packet.PacketSerializer;
import uno.anahata.satgyara.transport.AbstractConnection;
import uno.anahata.satgyara.transport.ConnectionPool;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class PacketTransport<T extends AbstractConnection> extends Transport<T>{

    @Getter
    protected PacketSerializer serializer;
    
    @Getter
    protected PacketDeserializer deserializer;

    public PacketTransport(UUID uuid, ConnectionPool<T> pool) {
        super(uuid, pool);
        init();
    }
    
    private void init() {
        serializer = new PacketSerializer(this);
        serializer.start();
        deserializer = new PacketDeserializer(this);
        deserializer.start();
    }

    @Override
    protected void init(T connection) throws Exception {
        log.debug("initConnection configuring input and output for {} ", connection);
        connection.setInput(serializer.getOutQueue());
        connection.setOutput(deserializer.getInQueue());
        log.debug("initConnection calling startIO for {} ", connection);
        connection.startIO();
        log.debug("initConnection startIO COMPLETED for {} ", connection);
    }

    @Override
    public BlockingQueue getOutQueue() {
        return serializer.getInQueue();
    }

    @Override
    public BlockingQueue getInQueue() {
        return deserializer.getOutQueue();
    }

    @Override
    public synchronized void close() throws Exception {        
        super.close();
        //nothing else going out
        serializer.interrupt();
        deserializer.interrupt();
        
        //Add now yes
        try {
            boolean interrupted = Thread.interrupted();
            getOutQueue().clear();
            getInQueue().clear();
            getInQueue().put(new NullPacket());
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
            log.debug("{} Null packet added to inQueue {}", this, getInQueue());
        } catch (Exception e) {
            log.error("{} Could not add Null packet to inQueue {}", this, getInQueue(), e);
        }
        
        log.debug("{} Closed", this);
        
    }
    
    
    
    
    
}
