/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.packet;

import java.nio.ByteBuffer;
import java.util.Date;

/**
 *
 * @author priyadarshi
 */
public class SerializedPacket {
    /**
     * The packet
     */
    public Packet packet;
    
    /**
     * the serialized packet
     */
    public byte[] data;
    
    /**
     * Arrival or departure time.
     */
    public Date time;

    /**
     * Constructor for outgoing packets
     * 
     * @param packet
     * @param data 
     */
    public SerializedPacket(Packet packet, byte[] data) {
        this.packet = packet;
        this.data = data;
    }

    /**
     * Constructor for inbound packets.
     * @param data
     * @param time 
     */
    public SerializedPacket(byte[] data, Date time) {
        this.data = data;
        this.time = time;
    }

    @Override
    public String toString() {
        String dataLength = data != null ? data.length + "b" : "null";
        return "SerializedPacket{" + "packet=" + packet + ", data=" + dataLength + ", time=" + time + '}';
    }
    
    
}
