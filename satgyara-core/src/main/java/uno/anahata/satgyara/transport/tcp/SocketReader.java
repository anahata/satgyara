/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.tcp;

import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.SocketException;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.transport.packet.SerializedPacket;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class SocketReader extends AbstractProcessorThread<byte[], SerializedPacket> {

    /**
     * The parent Connection
     */
    private ObjectInputStream ois;

    private volatile int packetsReceived = 0;
    private volatile double totalReadTime = 1;
    private volatile double totalBytesReceived = 0;

    private volatile double packetReadTime;
    private volatile BigDecimal packetSizeKBFormatted;
    private volatile BigDecimal averageReadSpeedMbpsFormatted;
    private volatile BigDecimal averageDownloadSpeedMbpsFormatted;

    //private final PacketDeserializer deserializerThread = new PacketDeserializer(this);
    private long startTime = System.currentTimeMillis();

    private TcpConnection connection;

    public SocketReader(TcpConnection conn) {
        super(0, 0);
        this.connection = conn;
        this.ois = conn.ois;
        setName(getClass().getSimpleName() + "|" + connection);

    }

    @Override
    protected void onStartup() throws Exception {
        super.onStartup();
        setName(getClass().getSimpleName() + "|" + connection);
        startTime = System.currentTimeMillis();
    }

    @Override
    protected byte[] getInput() throws Exception {
        //having everything in process() so the tps reflects the actual read time

        //connection.setQuickAck();
        double packetReadTs = System.currentTimeMillis();
        byte[] ret = (byte[]) ois.readObject();
        packetReadTs = System.currentTimeMillis() - packetReadTs;
        totalReadTime += packetReadTs;
        return ret;

    }

    @Override
    public SerializedPacket process(byte[] compressed) throws Exception {

        Date arrivalTime = new Date();

        double timeSinceStart = System.currentTimeMillis() - startTime + 1;

        totalBytesReceived += compressed.length;
        packetsReceived++;

        //double ellapsed = System.currentTimeMillis() - totalReadTime + 1;                
        double averageDownloadSpeedMbps = ((totalBytesReceived / 1024 * 8) / (timeSinceStart / 1000) / 1024);
        double averageReadSpeedMbps = ((totalBytesReceived / 1024 * 8) / (totalReadTime / 1000) / 1024);

        packetSizeKBFormatted = new BigDecimal((double) compressed.length / 1024).setScale(1, RoundingMode.HALF_EVEN);
        averageReadSpeedMbpsFormatted = new BigDecimal(averageReadSpeedMbps).setScale(2, RoundingMode.HALF_EVEN);
        averageDownloadSpeedMbpsFormatted = new BigDecimal(averageDownloadSpeedMbps).setScale(2, RoundingMode.HALF_EVEN);

        if (log.isTraceEnabled() || packetsReceived % 100 == 0) {
            log.debug(packetsReceived + " RECEIVED "
                    + " size: " + packetSizeKBFormatted + "KB "
                    + " read speed " + averageReadSpeedMbpsFormatted + " Mbps "
                    + " download speed " + averageDownloadSpeedMbpsFormatted + " Mbps "
                    + TcpSocketConfig.socketOptionsString(connection.getSocket()));
        }

        return new SerializedPacket(compressed, arrivalTime);

    }

    @Override
    protected void onExit() throws Exception {
        super.onExit();
        //connection.stop();        
        log.info("Exiting ");
        connection.close();
    }

    public BigDecimal getAverageDownloadSpeedMbpsFormatted() {
        return averageDownloadSpeedMbpsFormatted;
    }

    public BigDecimal getAverageReadSpeedMbpsFormatted() {
        return averageReadSpeedMbpsFormatted;
    }

    public int getPacketsReceived() {
        return packetsReceived;
    }

    public BigDecimal getPacketSizeKBFormatted() {
        return packetSizeKBFormatted;
    }

    public BigDecimal getDownloadSpeedMbpsFormatted() {
        return averageReadSpeedMbpsFormatted;
    }

    public double getPacketReadTime() {
        return packetReadTime;
    }

    public BigDecimal getTotalMBReceived() {
        return new BigDecimal((double) totalBytesReceived / 1024 / 1024).setScale(2, RoundingMode.HALF_EVEN);
    }

}
