/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.TreeMap;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import static uno.anahata.satgyara.transport.SocketConfig.KB;
import uno.anahata.satgyara.transport.packet.SerializedPacket;

/**
 *
 * @author priyadarshi
 */
public class UdpChunkReceiver extends AbstractProcessorThread<DatagramPacket, FragmentedUdpPacket> {

    /**
     * The parent connection.
     */
    private UdpConnection conn;

    private final byte[] buff = new byte[64 * KB];

    /**
     * The address where packets are coming from
     */
    private TreeMap<Long, FragmentedUdpPacket> fragmentedPackets = new TreeMap<>();

    //private PacketDeserializer deserializer = new PacketDeserializer(reader);
    /**
     * Last completed packet put in the out queue.
     */
    private Long outSeqNo = 0L;
    
    private int totalCompleted = 0;

    public UdpChunkReceiver(int inQueueSize, int outQueueSize) {
        super(inQueueSize, outQueueSize);
    }

    public UdpChunkReceiver(UdpConnection conn) {
        super(0, 0);
        this.conn = conn;
        //this.outQueue = conn.getTransport().getSerializer().getInQueue();
        setName(getName() + "|" + conn);
    }

    @Override
    protected void onStartup() throws Exception {
        super.onStartup();
    }

    @Override
    protected DatagramPacket getInput() throws Exception {
        DatagramPacket dp = new DatagramPacket(buff, buff.length);
        //System.out.println("Chunk receiver receive() on " + conn.getSocket().getLocalSocketAddress());
        conn.getSocket().receive(dp);
        byte[] copy = new byte[dp.getLength()];
        System.arraycopy(dp.getData(), dp.getOffset(), copy, 0, dp.getLength());
        dp.setData(copy);
        //System.out.println("Received Datagram: " + dp.getLength() + "b from " + dp.getSocketAddress()+ " ");
        return dp;
    }

    @Override
    public FragmentedUdpPacket process(DatagramPacket dp) throws Exception {

        ByteBuffer chunkData = ByteBuffer.wrap(dp.getData(), 0, dp.getLength());

        UdpPacket chunk = new UdpPacket(chunkData);
        //System.out.println("Converted datagram to udp chunk " + chunk);
        long pid = chunk.getPacketId();
        if (pid < outSeqNo) {
            System.out.println(getName() + " Discarding old chunk " + chunk);
            return null;
        }

        FragmentedUdpPacket fp = fragmentedPackets.get(pid);
        if (fp == null) {
            fp = new FragmentedUdpPacket(chunk);
            //System.out.println("New fragmented packet " + fp);
            if (!fp.isComplete()) {
                //System.out.println("Registering new fragmented packet " + fp);
                fragmentedPackets.put(pid, fp);
            }
        } else {
            //System.out.println("Adding chunk " + chunk + " to existing fp " + fp);
            fp.addChunk(chunk);
        }

        return fp;
    }
    
    public BigDecimal getPacketLoss() {
        
        float ratio = (float) totalCompleted / (outSeqNo + 1);
        float packetLossPct = 100f - (ratio * 100);        
        return new BigDecimal(packetLossPct).setScale(2, RoundingMode.HALF_EVEN);
    }

    @Override
    protected void doOutput(FragmentedUdpPacket fp) throws Exception {
        if (fp != null && fp.isComplete()) {
            
            //System.out.println("FragmentedUdpPacket complete " + fp);
            totalCompleted++;
            this.outSeqNo = fp.getPacketId();
            System.out.println(getPacketLoss() + " packet loss (" + totalCompleted + "/" + (outSeqNo + 1) + ") " + this);
            fragmentedPackets.remove(outSeqNo);

            var discarded = fragmentedPackets.headMap(outSeqNo, true);
            if (discarded.size() > 0) {
                System.out.println("Discarding " + discarded.size() + " obsolete fragmented packets: " + discarded);
            }

            var kept = fragmentedPackets.tailMap(outSeqNo, false);
            fragmentedPackets = new TreeMap<>(kept);
            if (kept.size() > 0) {
                System.out.println("Keeping " + fragmentedPackets.size() + " " + fragmentedPackets);
            }

            ByteBuffer payload = fp.getPayload();
            //System.out.println("Payload size: " + payload.remaining());
            byte[] barr = new byte[payload.remaining()];
            payload.get(barr);

            SerializedPacket baaat = new SerializedPacket(barr, new Date());
            System.out.println("Adding packet to deserializer queue");
            //conn.getTransport().getDeserializer().getInQueue().put(baaat);
        } else {
            //System.out.println("Fragmented packet not complete " + fp);
        }
    }
}
