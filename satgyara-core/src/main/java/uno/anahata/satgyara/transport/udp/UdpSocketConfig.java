/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import uno.anahata.satgyara.transport.SocketConfig;
import static uno.anahata.satgyara.transport.SocketConfig.TOS_LOW_DELAY;

/**
 *
 * @author priyadarshi
 */
public class UdpSocketConfig implements SocketConfig {

    private static final int SOCKET_BUF         = 64 * KB;  
    
    public static final void configure(DatagramSocket socket) throws SocketException, IOException {
        socket.setTrafficClass(TOS_LOW_DELAY);
        socket.setSendBufferSize(SOCKET_BUF);
        socket.setReceiveBufferSize(SOCKET_BUF);
    }
}
