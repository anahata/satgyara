/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport;

import uno.anahata.satgyara.transport.tcp.*;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 *
 * @author priyadarshi
 */
public interface SocketConfig {
    
    public static final int TOS_LOW_DELAY              = 0b11110000;
    public static final int TOS_HIGH_THROUGHPUT        = 0b11101000;
    public static final int TOS_HIGH_RELIABILITY       = 0b00000100;
    public static final int TOS_LOW_COST               = 0b00000010;
    
    public static final int EXPEDITED_DELIVERY         = 0b10111000;
    
    public static final int KB         = 1024;
    
    
    
}
