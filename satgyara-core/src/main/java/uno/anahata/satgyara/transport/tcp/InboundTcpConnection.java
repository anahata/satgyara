/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport.tcp;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peer.InboundPeer;
import uno.anahata.satgyara.peer.LocalPeer;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.handshake.ConnectionHandShake;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class InboundTcpConnection extends TcpConnection {

    public InboundTcpConnection(Socket s) throws SocketException, IOException{
        super.socket = s;
        initSocket();
    }

    @Override
    public void init() throws Exception {

        ConnectionHandShake cs = readHandShake();
        id = cs.fromConnectionId;
        peer = LocalPeer.getInstance().getInbound(cs.fromPeerId);
        boolean newPeer = peer == null;

        if (newPeer) {
            peer = new InboundPeer(cs.fromPeerId);
        }

        sendHandShake(cs.forTransportId);

        super.initIO();
        
        if (cs.forTransportId != null) {
            log.debug("forTransportId specified {}, enroling on new transport", cs.forTransportId);
            Transport t = peer.getTransports().get(cs.forTransportId);
            log.debug("got speficied transport {}", t);
            t.addNew(this);
            log.debug("added {} to transport {}", this, t);
        } else {
            log.debug("forTransportId not specified for {}, adding to pool as idle", this);
            peer.getTcpConnectionPool().addNew(this);
        }

    }
}
