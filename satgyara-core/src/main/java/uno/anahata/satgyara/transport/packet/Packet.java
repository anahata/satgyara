/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.packet;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.zip.Deflater;

/**
 *
 * @author priyadarshi
 */
public abstract class Packet implements Serializable {
    public long id;
    /**
     * This is actually the time before serialization starts as it is the last opportunity to 
     * set the field before it goes into the socket's queue
     */
    public Date departureTime;
    
    public transient Date arrivalTime;
    public transient long networkJitter;
    public transient long compressedSize;
    
    public long getNetworkLatency() {
        if (arrivalTime == null) {
            arrivalTime = new Date();
        }
        return arrivalTime.getTime() - departureTime.getTime();
    }
    
    public static class IdComparator implements Comparator<Packet> {

        @Override
        public int compare(Packet o1, Packet o2) {
            return ((Long)o1.id).compareTo(o2.id);
        }
    }
    
    public int getCompressionLevel() {
        return Deflater.DEFAULT_COMPRESSION;
    }
    
    public void preSerialize() throws Exception {
        
    }
    
    public void postDeserialize() throws Exception {
        
    }
    
}
