/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.concurrent.ParallelFifoProcessor;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.peer.LocalPeer;
import static uno.anahata.satgyara.transport.SocketConfig.KB;
import uno.anahata.satgyara.transport.packet.CompressionUtils;
import uno.anahata.satgyara.transport.udp.handshake.UdpHandShake;

/**
 *
 * @author priyadarshi
 */
public class UdpServerSocketListener extends AbstractProcessorThread<DatagramPacket, DatagramPacket> {

    private DatagramSocket serverSocket;
    private DatagramProcessor processor = new DatagramProcessor();
    

    //should only be retrieveing compressed serialized UdpHandShakes
    private final byte[] receiveBuffer = new byte[2 * KB];

    public UdpServerSocketListener() {
        super(0, 0);
        outQueue = processor.getInQueue();
    }

    @Override
    protected void onStartup() throws Exception {
        
        System.out.println("Creating Udp ServerSocket: ");
        serverSocket = new DatagramSocket(11711);        
        System.out.println("Udp ServerSocket bound to: " + serverSocket.getLocalSocketAddress());
        processor.start();
    }

    @Override
    protected DatagramPacket getInput() throws Exception {
        DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        System.out.println(this + " listening on " + serverSocket.getLocalSocketAddress());
        serverSocket.receive(receivePacket);
        System.out.println(this + " received packet on " + serverSocket.getLocalSocketAddress());
        return receivePacket;
    }

    @Override
    public DatagramPacket process(DatagramPacket dp) throws Exception {
        byte[] copy = new byte[dp.getLength()];
        System.arraycopy(dp.getData(), dp.getOffset(), copy, 0, dp.getLength());
        dp.setData(copy);
        return dp;
    }

//    @Override
//    protected void putOutput(UdpHandShake o) throws Exception {
//
//        ConnectedPeer peer = LocalPeer.getInstance().getPeer(o.getFromPeerUUID());
//        if (peer == null) {
//            System.out.println("HandShake received from");
//            return;
//        }
//        UdpChunkReceiver receiver = receivers.get(o.getSource());
//        if (receiver == null) {
//            receiver = new UdpChunkReceiver(o.getSource());
//            receivers.put(o.getSource(), receiver);
//        }
//
//        try {
//            receiver.getInQueue().add(o);
//        } catch (Exception e) {
//            System.err.println("Receiver full " + receiver);
//            e.printStackTrace();
//        }
//
//    }

    private static class DatagramProcessor extends ParallelFifoProcessor<DatagramPacket, Void> {

        public DatagramProcessor() {
            super(10, 0, 1);
        }

        @Override
        public Void process(DatagramPacket t) throws Exception {
            try {
                
                UdpHandShake hs = (UdpHandShake) CompressionUtils.decompressDeserialize(t.getData());
                hs.setFromAddress(t.getSocketAddress());
                RemotePeer cp = LocalPeer.getInstance().getInbound(hs.getFromPeerUUID());
                if (cp == null) {
                    System.err.println("Got " + hs + " but no peer registered for UUID " + hs.getFromPeerUUID());                    
                    return null;
                }
                
//                AbstractService as = cp.services.get(hs.getToServiceId());
//                if (as == null) {
//                    System.err.println("While processing " + hs + " could not find service " + hs.getToServiceId() + " on peer " + cp);                    
//                    return null;
//                }
//                
//                as.getUdpTransport(hs);
                                
            } catch (Exception e) {
                e.printStackTrace();                
            }
            
            return null;

        }

    }

}
