/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.packet;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import static uno.anahata.satgyara.transport.SocketConfig.KB;


/**
 *
 * @author priyadarshi
 */
@Slf4j
public class CompressionUtils {

    private static final ThreadLocal<ByteBuffer> byteBufferThreadLocal = new ThreadLocal<ByteBuffer>();
    
    public static byte[] compressSerialize(Serializable obj) throws IOException {
        return compressSerialize(obj, Deflater.DEFAULT_STRATEGY, Deflater.DEFAULT_COMPRESSION);
    }
    
    public static byte[] compressAsBlock(Serializable obj, int strategy, int compressionLevel) throws IOException {
        long ts = System.currentTimeMillis();
        byte[] serialized = SerializationUtils.serialize(obj);
        //System.out.println("Serialized = " + Arrays.toString(serialized));
        ByteBuffer bb = ByteBuffer.allocate(serialized.length);
        Deflater def = new Deflater(compressionLevel, false);
        def.setStrategy(strategy);
        def.setInput(serialized);
        def.finish();
        int compressedDataLength = def.deflate(bb);
        
        bb.rewind();
        byte[] compressed = new byte[compressedDataLength] ;
        bb.get(compressed);
        float ratio = 100 - ((compressed.length / serialized.length) * 100);
        ts = System.currentTimeMillis() - ts;
        
        log.debug("Compressed {} in {}ms. ratio={}% size={}", obj.getClass(), ts, (int) ratio, compressed.length);
        
        return compressed;
    }
    
    
    
    public static Serializable decompressAsBlock(byte[] serialized) throws Exception {
        long ts = System.currentTimeMillis();
        
        ByteBuffer bb = byteBufferThreadLocal.get();
        if (bb == null) {
            bb = ByteBuffer.allocate(4 * KB * KB);
            byteBufferThreadLocal.set(bb);
        } else {
            bb.clear();
        }
        
        Inflater def = new Inflater(false);
        def.setInput(serialized);
        int uncompressedDataLength = def.inflate(bb);
        
        byte[] uncompressed = new byte[uncompressedDataLength] ;
        bb.rewind();
        bb.get(uncompressed);
        
        Serializable s = SerializationUtils.deserialize(uncompressed);
        
        //log.debug("Compressed {} in {}ms. ratio={}% size={}", obj.getClass(), ts, (int) ratio, compressed.length);
        
        return s;
    }
    
    public static byte[] compressSerialize(Serializable obj, int strategy, int compressionLevel) throws IOException {
   
        long compressSerialTime = System.currentTimeMillis();
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 1024);
        Deflater def = new Deflater(compressionLevel);
        def.setStrategy(strategy);
        DeflaterOutputStream dos = new DeflaterOutputStream(baos, def, 1 * KB, true);
        try (ObjectOutputStream oos = new ObjectOutputStream(dos)) {
            oos.writeObject(obj);
        }

        byte[] compressed = baos.toByteArray();
        compressSerialTime = System.currentTimeMillis() - compressSerialTime;
        double compressedSizeKB = (double) compressed.length / 1024;
        //log.debug("compressSerial lvl=" + compressionLevel + " took " + compressSerialTime + " ms. " + compressedSizeKB + "Kb ");
        
        return compressed;
    }

    public static Serializable decompressDeserialize(byte[] barr) throws IOException, DataFormatException, ClassNotFoundException {
        InflaterInputStream dis = new InflaterInputStream(new ByteArrayInputStream(barr));
        ObjectInputStream ois = new ObjectInputStream(dis);
        Serializable ret = (Serializable) ois.readObject();
//                
//        byte[] serialized =  decompress(barr);
//        Serializable ret = SerializationUtils.deserialize(serialized);
        return ret;
    }
    
    private static byte[] compress(byte[] data) throws IOException {        
        Deflater deflater = new Deflater(7);
        deflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        deflater.finish();
        byte[] buffer = new byte[1024 * 1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer); // returns the generated code... index
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();
        return output;
    }


    private static byte[] decompress(byte[] data) throws IOException, DataFormatException {
        long ts = System.currentTimeMillis();
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024 * 1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();
        ts = System.currentTimeMillis() - ts; 
        //System.out.println("Deompressed: " + (data.length / 1024) + " Kb -> " + (output.length / 1024 )+ " Kb in " + ts);
        return output;
    }

    public static void main(String[] args) throws Exception {

        long size = 6 * 1024 * 1024;
        Random r = new Random();
        byte[] barr = new byte[(int)size];
        r.nextBytes(barr);
        System.out.println("Uncompressed " + barr.length);
        byte[] compressedFull = compressSerialize(barr, 2, Deflater.BEST_COMPRESSION);
        for (int i = 0; i < barr.length; i++) {
            //barr[i] = ImageUtils.fistroByte(barr[i], 4);
        }
        byte[] compressedFistro = compressSerialize(barr, 2, Deflater.BEST_COMPRESSION);
        
        System.out.println("Full " + compressedFull.length);
        System.out.println("Fistro " + compressedFistro.length);

    }
}
