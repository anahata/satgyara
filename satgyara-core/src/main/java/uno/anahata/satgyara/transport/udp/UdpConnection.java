/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import de.javawi.jstun.attribute.ChangeRequest;
import de.javawi.jstun.attribute.MappedAddress;
import de.javawi.jstun.attribute.MessageAttribute;
import de.javawi.jstun.header.MessageHeader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.BlockingQueue;
import java.util.zip.Deflater;
import lombok.extern.slf4j.Slf4j;
import static uno.anahata.satgyara.transport.SocketConfig.KB;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.packet.CompressionUtils;
import uno.anahata.satgyara.transport.AbstractConnection;
import uno.anahata.satgyara.transport.udp.handshake.UdpHandShake;
import uno.anahata.satgyara.transport.udp.handshake.UdpHandShakeResponse;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class UdpConnection extends AbstractConnection {

    private Integer remoteId;

    private boolean inbound;
    private DatagramSocket socket;
    private SocketAddress externalAddress;
    private SocketAddress remoteAddress;
    private Transport transport;
    private UdpChunkReceiver receiver;
    private UdpChunkTransmitter transmitter;

    @Override
    public void init() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startIO() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BlockingQueue getInput() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setInput(BlockingQueue bq) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BlockingQueue getOutput() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setOutput(BlockingQueue bq) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void close() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    /**
     * Outgoing
     *
     * @param t
     * @throws Exception
     */
    public UdpConnection(Transport t) throws Exception {
        inbound = false;
        this.transport = t;
        socket = new DatagramSocket();
        UdpSocketConfig.configure(socket);
        externalAddress = getExternalSocketAddress(socket);
//        var req  = new RequestUdpConnection(
//                t.getService().getRemoteId(), 
//                t.getId(), 
//                t.getRemoteId(), 
//                this.id,
//                externalAddress);
        
        //RequestUdpConnection response = transport.getService().getPeer().getControlService().requestUdpConnection(ucr);
        
        SocketAddress sa = null;//t.getService().getPeer().getUdpServerSocketAddress();
        //transmitter = new UdpChunkTransmitter();

        UdpHandShake ec = new UdpHandShake(this);
        byte[] serializedHandShake = CompressionUtils.compressSerialize(ec, Deflater.DEFAULT_STRATEGY, 9);
        transmitter = new UdpChunkTransmitter(this);

        DatagramPacket request = new DatagramPacket(serializedHandShake, serializedHandShake.length, sa);
        socket.send(request);

        byte[] buff = new byte[2 * KB];//should be enough for the ack
        DatagramPacket response = new DatagramPacket(buff, KB);
        socket.setSoTimeout(10000);
        socket.receive(response);
        socket.setSoTimeout(0);

        byte[] responseBarr = new byte[response.getLength()];
        System.arraycopy(buff, 0, responseBarr, 0, responseBarr.length);

        UdpHandShakeResponse uhsr = (UdpHandShakeResponse) CompressionUtils.decompressDeserialize(responseBarr);
//        if (uhsr.getToTransportId() != transport.getId()) {
//            throw new IllegalStateException("Invalid " + uhsr + " " + " toTransportId mismatch");
//        } else if (uhsr.getToServiceId() != transport.getService().getId()) {
//            throw new IllegalStateException("Invalid " + uhsr + " " + " toServiceId mismatch");
//        } else if (uhsr.getFromServiceId() != transport.getService().getRemoteId()) {
//            throw new IllegalStateException("Invalid " + uhsr + " " + " fromServiceId mismatch");
//        } else if (transport.getRemoteId() != null && uhsr.getFromTransportId() != transport.getRemoteId()) {
//            throw new IllegalStateException("Invalid " + uhsr + " " + " fromTransportId mismatch");
//        } else if (transport.getRemoteId() == null) {
//            System.out.println("Got remoteid for UdpTransport " + transport);
//            transport.setRemoteId(uhsr.getFromTransportId());
//        }

        remoteAddress = response.getSocketAddress();

        System.out.println("Got UDP ack " + uhsr);

        init();

    }

    /**
     * Incoming
     *
     * @param transport
     * @param uhs
     * @throws Exception
     */
    public UdpConnection(Transport transport, UdpHandShake uhs) throws Exception {
        inbound = true;
        this.transport = transport;
        this.remoteId = uhs.getFromConnectionId();
        socket = new DatagramSocket();
        UdpSocketConfig.configure(socket);
        remoteAddress = uhs.getFromAddress();
        UdpHandShakeResponse ec = new UdpHandShakeResponse(this);
        byte[] serializedResponse = CompressionUtils.compressSerialize(ec, Deflater.DEFAULT_STRATEGY, 9);
        DatagramPacket response = new DatagramPacket(serializedResponse, serializedResponse.length, remoteAddress);
        socket.send(response);
        //so here we either wait for some sort of connection established acknowledgement via tcp
        //or we assume it is all good       
        init();
    }

//    private void init() {
//
//        transmitter.start();
//        receiver = new UdpChunkReceiver(this);
//        receiver.start();
//    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public SocketAddress getRemoteAddress() {
        return remoteAddress;
    }

//    public UdpTransport getTransport() {
//        return transport;
//    }
//
//    public int getId() {
//        return id;
//    }

    public Integer getRemoteId() {
        return remoteId;
    }

    public UdpChunkReceiver getReceiver() {
        return receiver;
    }
    
    

    private static SocketAddress getExternalSocketAddress(DatagramSocket socket) throws Exception {

        MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
        // sendMH.generateTransactionID();

        // add an empty ChangeRequest attribute. Not required by the
        // standard,
        // but JSTUN server requires it
        ChangeRequest changeRequest = new ChangeRequest();
        sendMH.addMessageAttribute(changeRequest);

        byte[] data = sendMH.getBytes();

        DatagramSocket s = new DatagramSocket();
        //s.setReuseAddress(true);

        DatagramPacket p = new DatagramPacket(data, data.length, InetAddress.getByName("stun.l.google.com"), 19302);
        s.send(p);

        DatagramPacket rp;

        rp = new DatagramPacket(new byte[32], 32);

        s.receive(rp);
        MessageHeader receiveMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingResponse);
        // System.out.println(receiveMH.getTransactionID().toString() + "Size:"
        // + receiveMH.getTransactionID().length);

        receiveMH.parseAttributes(rp.getData());

        MappedAddress ma = (MappedAddress) receiveMH
                .getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
        System.out.println(ma.getAddress() + " " + ma.getPort());
        return new InetSocketAddress(ma.getAddress().getInetAddress(), ma.getPort());

    }
    
    
    
    public static void main(String[] args) throws Exception {
        DatagramSocket s = new DatagramSocket();
        System.out.println(getExternalSocketAddress(s));
    }

}
