/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp.handshake;

import java.io.Serializable;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import uno.anahata.satgyara.peer.LocalPeer;

/**
 *
 * @author priyadarshi
 */
@Setter
@Getter
public abstract class HandShake implements Serializable {
    private final UUID fromPeerUUID = LocalPeer.getInstance().getUuid();    
    private int fromServiceId;
    private int fromTransportId;
    private int fromConnectionId;
    
    
    public HandShake() {
        
    }

}
