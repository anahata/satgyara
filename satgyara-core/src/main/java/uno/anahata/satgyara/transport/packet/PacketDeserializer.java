/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.packet;

import org.apache.commons.lang3.SerializationUtils;
import uno.anahata.satgyara.concurrent.ParallelFifoProcessor;
import uno.anahata.satgyara.transport.Transport;

/**
 * Deserializes objects from the readQueue and passess them onto the appropiate
 * handler
 */
public class PacketDeserializer<P extends Packet> extends ParallelFifoProcessor<SerializedPacket, P> {

    private final Transport transport;

    private volatile long packetMinDepartureArrivalLatency;
    
    public PacketDeserializer(Transport t) {
        super(1, 1, 2);
        this.transport = t;
        setName(getClass().getSimpleName() + "|" + transport);
    }
    
    @Override
    public P process(SerializedPacket packetAndArrivalTime) throws Exception {
        P packet = (P) CompressionUtils.decompressDeserialize(packetAndArrivalTime.data);
        packet.arrivalTime = packetAndArrivalTime.time;
        packet.compressedSize = packetAndArrivalTime.data.length;        
        packet.postDeserialize();
        //packet.fromTransport = transport;
        
        return packet;
    }

    @Override
    protected synchronized void doOutput(P packet) throws Exception {
        long packetDepartureArrivalLatency = packet.getNetworkLatency();
        if (packetDepartureArrivalLatency <= packetMinDepartureArrivalLatency) {
            packetMinDepartureArrivalLatency = packetDepartureArrivalLatency;
        }
        packet.networkJitter = packetDepartureArrivalLatency - packetMinDepartureArrivalLatency;
        super.doOutput(packet);
    }
}
