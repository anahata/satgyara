/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp.handshake;

import java.net.SocketAddress;
import uno.anahata.satgyara.transport.udp.UdpConnection;

/**
 *
 * @author priyadarshi
 */
public class UdpHandShake extends HandShake{

    private int fromConnectionId;
    private int toServiceId;
    private transient SocketAddress fromAddress;
    
    public UdpHandShake(UdpConnection conn) {
//        fromConnectionId = conn.getId();
//        super.setFromTransportId(conn.getTransport().getId());
//        super.setFromServiceId(conn.getTransport().getService().getId());
//        toServiceId = conn.getTransport().getService().getRemoteId();
    }

    public SocketAddress getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(SocketAddress fromAddress) {
        this.fromAddress = fromAddress;
    }

    public int getToServiceId() {
        return toServiceId;
    }

    public void setToServiceId(int toServiceId) {
        this.toServiceId = toServiceId;
    }

    public int getFromConnectionId() {
        return fromConnectionId;
    }
    
    
    
    
    
}
