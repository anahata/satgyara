/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.tcp;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.transport.packet.SerializedPacket;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class SocketWriter extends AbstractProcessorThread<SerializedPacket, Void> {

    private final ObjectOutputStream oos;

    @Getter
    private long startTime;
    @Getter
    private volatile int packetsSent = 0;
    private volatile double totalWriteTime = 0;
    @Getter
    private volatile double totalBytesSent = 0;

    private volatile double packetWriteTime;
    
    @Getter
    private volatile BigDecimal packetSizeKBFormatted;
    @Getter
    private volatile BigDecimal packetWriteSpeedMbpsFormatted;
    @Getter
    private volatile BigDecimal averageWriteSpeedMbpsFormatted;
    @Getter
    private volatile BigDecimal averageUploadSpeedMbpsFormatted;

    private TcpConnection connection;

    /**
     * Th
     *
     * @param conn - the tcp connection this writer is for
     *
     */
    public SocketWriter(TcpConnection conn) {
        super(0, 0);
        this.connection = conn;
        this.oos = conn.oos;
        setName(getClass().getSimpleName() + "|" + connection);
    }

    @Override
    protected void onStartup() throws Exception {
        super.onStartup(); 
        setName(getClass().getSimpleName() + "|" + connection);
        startTime = System.currentTimeMillis();
    }

    @Override
    public Void process(SerializedPacket sp) throws Exception {

        byte[] compressed = sp.data;
        
        double packetWriteTs = System.currentTimeMillis();
        writeObject(compressed);
        packetWriteTime = System.currentTimeMillis() - packetWriteTs + 1;
        
        connection.getTransport().getLastSent().clear();
        connection.getTransport().getLastSent().offer(sp);

        packetsSent++;
        totalBytesSent += compressed.length;
        totalWriteTime += packetWriteTime;

        double timeSinceStart = System.currentTimeMillis() - startTime + 1;
        double packetSizeKB = (double) compressed.length / 1024;
        double packetWriteSpeedMbps = (compressed.length / 1024 / 1024 * 8) / (packetWriteTime / 1000);
        double averageWriteSpeedMbps = (totalBytesSent / 1024 / 1024 * 8) / (totalWriteTime / 1000);
        double averageUploadSpeedMbps = (totalBytesSent / 1024 / 1024 * 8) / (timeSinceStart / 1000);
        
        packetSizeKBFormatted = new BigDecimal(packetSizeKB).setScale(1, RoundingMode.HALF_EVEN);
        packetWriteSpeedMbpsFormatted = new BigDecimal(packetWriteSpeedMbps).setScale(2, RoundingMode.HALF_EVEN);
        averageWriteSpeedMbpsFormatted = new BigDecimal(averageWriteSpeedMbps).setScale(2, RoundingMode.HALF_EVEN);
        averageUploadSpeedMbpsFormatted = new BigDecimal(averageUploadSpeedMbps).setScale(2, RoundingMode.HALF_EVEN);


        //log.debug(packetsSent + " SENT " + averageWriteSpeedMbps + " Mbps Average " + so.packet + " on ");
        if (log.isTraceEnabled() || packetsSent % 100 == 0) {
            log.debug("{} size: avg upload: {} avg write: {} ", packetsSent, averageUploadSpeedMbpsFormatted, averageWriteSpeedMbpsFormatted);
        }

        return null;
    }

    private void writeObject(Serializable s) throws Exception {
        oos.writeObject(s);
        oos.flush();
        oos.reset();//to avoid the object outputstream tracking already sent objects for reconstructing an object graph        
    }

    @Override
    protected void onExit() throws Exception {
        super.onExit();
        //connection.stop();        
        log.info("Cakking Connection.close");
        connection.close();
        
    }

    public BigDecimal getTotalMBSent() {
        return new BigDecimal((double) totalBytesSent / 1024 / 1024).setScale(2, RoundingMode.HALF_EVEN);
    }

}
