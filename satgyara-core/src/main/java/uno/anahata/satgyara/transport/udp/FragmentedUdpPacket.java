/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.TreeSet;
import java.util.zip.Deflater;
//import uno.anahata.satgyara.service.speedtest.SpeedTestPacket;
import uno.anahata.satgyara.transport.packet.Packet;
import uno.anahata.satgyara.transport.packet.CompressionUtils;
import uno.anahata.satgyara.transport.packet.SerializedPacket;

/**
 *
 * @author priyadarshi
 */
public class FragmentedUdpPacket implements Comparable<FragmentedUdpPacket> {

    private final TreeSet<UdpPacket> chunks = new TreeSet<>();
    private final long packetId;
    private UdpPacket first;
    private int receivedPayloadLength;
    private Packet packet;
    private ByteBuffer payload;

    public FragmentedUdpPacket(UdpPacket chunk) {
        addChunk(chunk);
        packetId = chunk.getPacketId();
    }

    public FragmentedUdpPacket(int toConnectionId, SerializedPacket p, int mtu) {

        this.packet = p.packet;
        this.packetId = packet.id;
        ByteBuffer dataBuffer = ByteBuffer.wrap(p.data);
        this.payload = dataBuffer.duplicate();

        double totalChunksDouble = (float) p.data.length / mtu;
        int totalChunks = (int) totalChunksDouble;
        if (totalChunksDouble > totalChunks) {
            totalChunks++;
        }

        for (int chunkId = 0; chunkId < totalChunks; chunkId++) {

            int payloadStart = chunkId * mtu;
            int payloadEnd = (chunkId + 1) * mtu;

            if (payloadEnd > p.data.length) {
                payloadEnd = p.data.length;
            }

            //int payloadLength = payloadEnd - payloadStart;
            dataBuffer.position(payloadStart);
            dataBuffer.limit(payloadEnd);
            ByteBuffer chunkPayload = dataBuffer.slice();
            UdpPacket chunk = new UdpPacket(toConnectionId, p.packet.id, chunkId, totalChunks, chunkPayload);
            if (chunkId == 0) {
                first = chunk;
            }
            //System.out.println("Added chunk " + chunk);
            chunks.add(chunk);
        }

        System.out.println("Created FragmentedUdpPacket from " + p.packet + " " + this);
    }

    public TreeSet<UdpPacket> getChunks() {
        return chunks;
    }

    public Long getPacketId() {
        return chunks.first().getPacketId();
    }

    public final void addChunk(UdpPacket t) {

        if (chunks.add(t)) {
            receivedPayloadLength += t.getPayload().remaining();
            //System.out.println("Added chunk " + t.getPayload().remaining());
        } else {
            System.out.println("Duplicate chunk received: " + t);
        }

        if (t.isFirst()) {
            this.first = t;
        }
    }

    public boolean isComplete() {
        return first != null && first.getTotalChunks() == chunks.size();
    }

    public ByteBuffer getPayload() {
        if (isComplete()) {
            if (payload == null) {
                payload = ByteBuffer.allocate(receivedPayloadLength);
                for (UdpPacket chunk : chunks) {
                    payload.put(chunk.getPayload());
                }
                payload.rewind();
            }
            return payload.duplicate();
        } else {
            throw new IllegalStateException("Packet incomplete " + this);
        }
    }

    @Override
    public int compareTo(FragmentedUdpPacket o) {
        return getPacketId().compareTo(o.getPacketId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.packetId ^ (this.packetId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FragmentedUdpPacket other = (FragmentedUdpPacket) obj;
        if (this.packetId != other.packetId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FragmentedUdpPacket{" + "chunks=" + chunks.size() + ", packetId=" + packetId + ", first=" + first + ", receivedPayloadLength=" + receivedPayloadLength + ", packet=" + packet + ", payload=" + payload + '}';
    }

    public static void main(String[] args) {
        try {
//            Packet p = new SpeedTestPacket(2000);
//            byte[] serialized = CompressionUtils.compressSerialize(p, Deflater.DEFAULT_STRATEGY, 9);
//            SerializedPacket sp = new SerializedPacket(p, serialized);
//            new FragmentedUdpPacket(1, sp, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
