/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peer.RemotePeer;

/**
 *
 * @author priyadarshi
 */

@Getter
@Slf4j
public abstract class ConnectionPool<T extends AbstractConnection> {
    
    protected final RemotePeer peer;
    
    private final Map<UUID, T> idle = new HashMap<>();
    private final Map<UUID, T> loaned = new HashMap<>();

    protected ConnectionPool(RemotePeer peer) {
        this.peer = peer;
    }
    
    public synchronized void addNew(T conn) {
        log.debug("Adding new connection to pool as idle {}", conn);
        idle.put(conn.getId(), conn);
        log.debug("ADDED new connection to pool as idle {}. ", conn);
    }
    
    public synchronized boolean returnLoaned(T conn) throws Exception{
        loaned.remove(conn.id, conn);
        idle.put(conn.id, conn);
        //todo return true if everything wen well only
        return true;
    }
    
    public synchronized T borrowIdle(UUID uuid) throws Exception{
        T conn = idle.get(uuid);
        idle.remove(conn.getId(), conn);
        loaned.put(conn.getId(), conn);
        return conn;
    }
    
//    public synchronized T borrowIdle() throws Exception{
//        if (idle.isEmpty()) {
//            createConnection();
//        }
//        var conn = idle.entrySet().iterator().next().getValue();
//        idle.remove(conn.getId(), conn);
//        loaned.put(conn.getId(), conn);
//        return conn;
//    }
    
//    public abstract T createConnection() throws Exception;

    @Override
    public String toString() {
        return "ConnectionPool{" + "\npeer=" + peer + ",\nidle=" + idle.keySet() + ", \nloaned=" + loaned.keySet() + '}';
    }
    
    
    
}
