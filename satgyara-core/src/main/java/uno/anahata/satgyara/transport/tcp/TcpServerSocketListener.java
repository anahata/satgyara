/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.tcp;

//import com.dosse.upnp.UPnP;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.concurrent.ParallelFifoProcessor;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class TcpServerSocketListener extends Thread {
    
    public static final int SATGYARA_TCP_PORT_START = 11711;
    
    private ServerSocket tcpServerSocket;
    private SocketAddress externalAddress;
    private SocketWrapper socketWrapper = new SocketWrapper();
    
    public TcpServerSocketListener() {
        setName(getClass().getSimpleName());
    }

    @Override
    public void run() {
        
        //System.out.println("WAN IP " + UPnP.getWanIP());
        //System.out.println("Local IP " + UPnP.getLocalIP());
        
        socketWrapper.start();
        log.debug("started {} wrapper with {} threads", socketWrapper, socketWrapper.getThreadCount());
        initTCP();        
        while (true) {
            receiveTCP();
        }
    }

    private void initTCP() {
        int port = SATGYARA_TCP_PORT_START;
        while (true) {
            log.debug("Trying to create local TCP Server Socket " + port);
            try {
                tcpServerSocket = new ServerSocket();                
                log.debug("Created " + tcpServerSocket);
                TcpSocketConfig.configure(tcpServerSocket);
                tcpServerSocket.bind(new InetSocketAddress((InetAddress)null, port));
                
                break;
//                if (false/*UPnP.isUPnPAvailable()*/) {
//                    System.out.println("UPnp available Natting TCP port on router");
//                    boolean uPnPok = UPnP.openPortTCP(port);
//                    if (!uPnPok) {
//                        System.out.println("Could not nat port on router via UPnp, closing local TCP Server socket and trying next port");
//                        tcpServerSocket.close();
//                    } else {
//                        break;
//                    }
//                } else {
//                    System.out.println("UPNP not available, TCP port not natted");
//                    break;
//                }
            
            } catch (Exception e) {                                
                log.error("Could not create TCP socket on port " + port + " " + e);
                port++;
            }
            
        }
        //setName(getName());
    }

    private void receiveTCP() {
        try {
            log.debug("{} tcpServerSocket.accept(), queue size = {}", tcpServerSocket, socketWrapper.getInQueue().size());
            Socket s = tcpServerSocket.accept();
            log.debug("TCP connection received {} queue size = {}", s, socketWrapper.getInQueue().size());
            socketWrapper.getInQueue().put(s);
            log.debug("TCP connection received added to wrapper queue. queue size = {}", s, socketWrapper.getInQueue().size());
        } catch (Exception e) {
            log.error("Exception receiving incoming socket connection", e);
        }
    }
    
    private static class SocketWrapper extends ParallelFifoProcessor<Socket, InboundTcpConnection> {

        public SocketWrapper() {
            super(2, 0, 4);
            setName(getClass().getSimpleName());
        }

//        @Override
//        protected void onStartup() {
//            log.debug("External IP " + LocalPeer.getExternalIpAddress());
//        }
        
        @Override
        public InboundTcpConnection process(Socket t) throws Exception {
            
            try {
                log.debug("Wrapping socket {} in InboundTcpConnection", t);
                InboundTcpConnection ret = new InboundTcpConnection(t);
                ret.init();
                log.debug("WRAPPED socket {} in {}", t, ret);
                return null;
            } catch (Exception e) {
                log.error("Exception wrapping received socket connection:", e);
                return null;
            }
            
        }

        
    }

    
}
