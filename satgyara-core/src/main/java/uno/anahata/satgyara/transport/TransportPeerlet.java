/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peer.LocalPeer;
import uno.anahata.satgyara.peer.OutboundPeer;
import uno.anahata.satgyara.peer.RelayedPeer;
import uno.anahata.satgyara.peer.RemotePeer;
import uno.anahata.satgyara.peerlet.PeerletContext;
import uno.anahata.satgyara.transport.packet.PacketTransport;
import uno.anahata.satgyara.transport.tcp.OutboundTcpConnection;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class TransportPeerlet implements RemoteTransportPeerlet {
    
    @Override
    public UUID createConnection(UUID transportUUID) throws Exception {
        OutboundPeer caller = (OutboundPeer) PeerletContext.getCaller();
        OutboundTcpConnection conn = (OutboundTcpConnection) caller.createTcpConnection(transportUUID);
        return conn.getId();
    }

    @Override
    public void createPacketTransport(UUID transportUUID) {
        RemotePeer caller = PeerletContext.getCaller();
        PacketTransport pt = new PacketTransport(transportUUID, caller.tcpConnectionPool);
    }
    
    @Override
    public void createPacketTransportForRelayedPeer(UUID peerUUID, UUID transportUUID) {
        OutboundPeer caller = (OutboundPeer) PeerletContext.getCaller();
        PacketTransport pt = new PacketTransport(transportUUID, caller.tcpConnectionPool);
        RelayedPeer target = caller.getRelayedPeer(peerUUID);
        target.getTransports().put(pt.getId(), pt);
    }
    

    @Override
    public void createRelayedPeer(UUID peerUUID, UUID transportUUID) throws Exception {
        OutboundPeer caller = (OutboundPeer) PeerletContext.getCaller();
        log.debug("Creating RelayedPeer {} coreTransportUUID={} caller={}", peerUUID, transportUUID, caller);
        RelayedPeer rp = new RelayedPeer(peerUUID, caller, transportUUID);
    }
    
    

}
