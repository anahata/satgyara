/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp.handshake;

import uno.anahata.satgyara.transport.udp.UdpConnection;

/**
 *
 * @author priyadarshi
 */
public class UdpHandShakeResponse extends UdpHandShake{

    private int toConnectionId;
    private int toTransportId;
    
    public UdpHandShakeResponse(UdpConnection conn) {
        super(conn);
        toConnectionId = conn.getRemoteId();
        //toTransportId = conn.getTransport().getRemoteId();
    }

    public int getToTransportId() {
        return toTransportId;
    }

    public void setToTransportId(int toTransportId) {
        this.toTransportId = toTransportId;
    }
    
    
}
