/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peer.RemotePeer;
import static uno.anahata.satgyara.seq.UUIDUtils.tail;

/**
 *
 * @author priyadarshi
 * @param <T>
 */
@Slf4j
public abstract class Transport<T extends AbstractConnection> {

    @Getter
    private final UUID id;

    @Getter
    protected final ConnectionPool<T> source;

    @Getter
    protected final List<T> connections = new ArrayList();

    /**
     * The last object sent by this transport.
     */
    @Getter
    @Setter
    protected BlockingQueue lastSent = new LinkedBlockingQueue(1);

    private boolean closed = false;

    protected Transport(UUID id, ConnectionPool<T> source) {
        this.id = id;
        this.source = source;
        if (source != null) {
            source.getPeer().registerTransport(this);
        }
    }

    public RemotePeer getPeer() {
        return source != null ? source.getPeer() : null;
    }

    public synchronized final void addNew(T conn) throws Exception {
        //let the pool know that we are 'borrowing it'
        source.getLoaned().put(conn.getId(), conn);

        try {
            conn.transport = this;
            init(conn);
        } catch (Exception e) {
            log.error("Exception linking Connection {}, returning to pool {}", conn, source, e);
            source.returnLoaned(conn);
            conn.transport = null;
            log.error("Exception linking Connection {}, RETURNED to pool {}", conn, source, e);
            return;
        }
        connections.add(conn);
    }

    /**
     * Initializes a connection
     *
     * @param connection
     * @throws Exception
     */
    protected abstract void init(T connection) throws Exception;
    
    public synchronized final void discard(T conn) throws Exception {
        //let the pool know that we are not borrowing the connection any more
        source.getLoaned().remove(conn.getId(), conn);
        connections.remove(conn);
        if (connections.isEmpty()) {
            close();
        }
    }

    /**
     * The queue were the application can place outgoing data (send)
     *
     * @return
     */
    public abstract BlockingQueue getOutQueue();

    /**
     * The queue were the application can receive incoming data. (read)
     *
     * @return
     */
    public abstract BlockingQueue getInQueue();

    public void send(Serializable o) throws Exception {
        assertOpen();
        getOutQueue().put(o);
    }

    protected void assertOpen() {
        if (closed) {
            throw new IllegalStateException("Transport closed " + this);
        }
    }

    /**
     * Closes all connections and unregisters this transport
     *
     * @throws Exception
     */
    public synchronized void close() throws Exception {
        assertOpen();
        closed = true;
        log.debug("Closing {}", this);
        for (AbstractConnection c : new ArrayList<>(connections)) {
            try {
                c.close();
            } catch (Exception e) {
                log.error("Exception closing connection " + c, e);
            }
        }
        getPeer().unregisterTransport(this);
        log.debug("Closed {}", this);

    }

    @Override
    public String toString() {
        String sourceStr = source != null ? source.getPeer().toString() : "no peer";
        return getClass().getSimpleName() + "-" + tail(id) + "|" + sourceStr;
    }

}
