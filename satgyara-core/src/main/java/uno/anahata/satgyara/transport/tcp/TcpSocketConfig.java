/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketOption;
import java.net.StandardSocketOptions;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.transport.SocketConfig;
import static uno.anahata.satgyara.transport.SocketConfig.KB;

/**
 * 
 * @author priyadarshi
 */
@Slf4j
public class TcpSocketConfig implements SocketConfig {
    
    private static final int SOCKET_BUF         = 256 * KB;
    
    public static final void configure(Socket socket) throws SocketException, IOException{
        socket.setTcpNoDelay(true);
        socket.setKeepAlive(true);
        socket.setSendBufferSize(SOCKET_BUF);
        socket.setReceiveBufferSize(SOCKET_BUF);
        socket.setOption(StandardSocketOptions.IP_TOS, 0b00000010);
        
        ////System.out.println("TrafficClsas = " + TOS_LOW_DELAY);        
        //socket.setTrafficClass(TOS_LOW_DELAY);//Shirewark shows this does nothing
        //System.out.println("Current Priority " + socket.getOption(ExtendedSocketOptions.SO_FLOW_SLA).priority());
        //socket.getOption(ExtendedSocketOptions.SO_FLOW_SLA).priority(SocketFlow.HIGH_PRIORITY);
        //System.out.println("New Priority " + socket.getOption(ExtendedSocketOptions.SO_FLOW_SLA).priority());
        //socket.setOption(ExtendedSocketOptions.TCP_QUICKACK, true);
        //socket.getOption(ExtendedSocketOptions.SO_FLOW_SLA).priority(SocketFlow.HIGH_PRIORITY);
        
        //SocketFlow sf = socket.getOption(ExtendedSocketOptions.SO_FLOW_SLA).priority(SocketFlow.HIGH_PRIORITY);
        //socket.setOption(ExtendedSocketOptions.SO_FLOW_SLA, sf);
        
        
        log.debug("Configured " + socket + " " + socketOptionsString(socket));
    }
    
    
    public static final void configure(ServerSocket socket) throws SocketException, IOException{
        
        socket.setReceiveBufferSize(SOCKET_BUF);
        
        log.debug("Configured " + socket + " " + socketOptionsString(socket));
    }
    
    public static String socketOptionsString(Socket socket) throws IOException{
        StringBuilder sb = new StringBuilder();
        for (SocketOption so: socket.supportedOptions()) {
            sb.append(so);
            sb.append("=");
            sb.append(socket.getOption(so));
            sb.append(" ");
        }
        return sb.toString();
    }
    
    public static String socketOptionsString(ServerSocket socket) throws IOException{
        StringBuilder sb = new StringBuilder();
        for (SocketOption so: socket.supportedOptions()) {
            sb.append(so);
            sb.append("=");
            sb.append(socket.getOption(so));
            sb.append(" ");
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {
        Socket s = new Socket();
        
        configure(s);
    }
    
}
