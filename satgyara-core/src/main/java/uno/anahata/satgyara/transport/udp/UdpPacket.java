/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Objects;
import uno.anahata.satgyara.transport.packet.SerializedPacket;

/**
 *
 * @author priyadarshi
 */
public class UdpPacket implements Comparable<UdpPacket>{
    private Date arrivalTime;
    private long connectionId;
    private long packetId;
    private int chunkId;
    private int totalChunks;
    private ByteBuffer data;
    private int payloadLength;
    private ByteBuffer payload;
    
    
    /**
     * Constructor for parsing.
     *      
     * @param data 
     */
    public UdpPacket(ByteBuffer data) {
        arrivalTime = new Date();
        this.data = data;
        connectionId = data.getInt();
        packetId = data.getLong();
        chunkId = data.getInt();
        if (chunkId == 0) {
            totalChunks = data.getInt();
        }
        payload = data.slice();
        payloadLength = payload.limit();
    }
    
    /**
     * Constructor for writing.
     */
    public UdpPacket (int connectionId, Long packetId, int chunkId, int totalChunks, ByteBuffer payload) {
        this.connectionId = connectionId;
        this.packetId = packetId;
        this.chunkId = chunkId;        
        this.totalChunks = totalChunks;
        this.payload = payload;
        this.payloadLength = payload.limit();
        
        int dataSize = 16;//4 for connectionId, 8 for packetId 4 for chunkId
        if (chunkId == 0) {
            dataSize+=4;//4 for connectionId and totalChunks
        }        
        dataSize += payload.limit();
        data = ByteBuffer.allocate(dataSize);        
        data.putInt(connectionId);
        data.putLong(packetId);
        data.putInt(chunkId);
        if (chunkId == 0) {
            data.putInt(totalChunks);
        }
        
        data.put(payload);
        data.rewind();
    }
    
    public ByteBuffer getData() {
        return data;
    }

    public int getTotalChunks() {
        return totalChunks;
    }

    public long getPacketId() {
        return packetId;
    }

    public int getChunkId() {
        return chunkId;
    }
    
    public boolean isFirst() {
        return chunkId == 0;
    }

    public ByteBuffer getPayload() {
        return payload.duplicate();
    }
    
    public Date getArrivalTime() {
        return arrivalTime;
    }

    @Override
    public int compareTo(UdpPacket o) {
        return ((Integer)chunkId).compareTo(o.chunkId);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (int) (this.packetId ^ (this.packetId >>> 32));
        hash = 97 * hash + this.chunkId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UdpPacket other = (UdpPacket) obj;
        if (this.packetId != other.packetId) {
            return false;
        }
        if (this.chunkId != other.chunkId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UdpChunk{" + "arrivalTime=" + arrivalTime + ", packetId=" + packetId + ", chunkId=" + chunkId + ", totalChunks=" + totalChunks + ", data=" + data + ", payloadLength=" + payloadLength + ", payload=" + payload + '}';
    }
    
    
    
}
