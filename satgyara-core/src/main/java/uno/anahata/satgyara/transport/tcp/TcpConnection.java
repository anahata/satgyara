/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport.tcp;

import uno.anahata.satgyara.transport.AbstractConnection;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import static uno.anahata.satgyara.seq.UUIDUtils.tail;
import static uno.anahata.satgyara.transport.SocketConfig.KB;
import uno.anahata.satgyara.transport.handshake.ConnectionHandShake;
import uno.anahata.satgyara.transport.packet.CompressionUtils;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public abstract class TcpConnection extends AbstractConnection {

    @Getter
    protected Socket socket;
    protected ObjectOutputStream oos;
    protected ObjectInputStream ois;
    @Getter
    protected SocketWriter writer;
    @Getter
    protected SocketReader reader;

    @Getter
    private boolean quickAckSupported = false;

    private boolean closed = false;

    protected final void initSocket() throws IOException {
        TcpSocketConfig.configure(socket);
        quickAckSupported = socket.supportedOptions().contains(jdk.net.ExtendedSocketOptions.TCP_QUICKACK);
        setQuickAck();
    }

    void setQuickAck() throws IOException {
        if (quickAckSupported) {
            socket.setOption(jdk.net.ExtendedSocketOptions.TCP_QUICKACK, true);
        }
    }

    protected void sendHandShake(UUID forTransportUUID) throws IOException {

        ConnectionHandShake cs = new ConnectionHandShake();
        cs.fromConnectionId = getId();
        cs.forTransportId = forTransportUUID;
        if (peer != null && peer.getUuid() != null) {
            //this is for every case other than the initial connection
            //to that peer when we only know the peers ip address
            cs.toPeerId = peer.getUuid();
        }

        //oos = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream(), 64 * 1024));
        //oos = new ObjectOutputStream(new DeflaterOutputStream(socket.getOutputStream(), df, 4 * KB * KB, true));
        oos = new ObjectOutputStream(socket.getOutputStream());
        log.debug("Sending {} via {}", cs, socket);
        oos.writeObject(CompressionUtils.compressSerialize(cs));
        oos.flush();
        oos.reset();

        log.debug("SENT    {} via {}", cs, socket);
    }

    protected ConnectionHandShake readHandShake() throws IOException, ClassNotFoundException, DataFormatException {
        //ois = new ObjectInputStream(new InflaterInputStream(socket.getInputStream()));
        ois = new ObjectInputStream(socket.getInputStream());

        log.debug("Waiting for handshake on {}", socket);
        ConnectionHandShake chs = (ConnectionHandShake) CompressionUtils.decompressDeserialize((byte[]) ois.readObject());
        log.debug("RECEIVED handshake {} on {}", chs, socket);

        return chs;
    }

    protected void initIO() throws IOException {
        reader = new SocketReader(this);
        writer = new SocketWriter(this);
    }

    @Override
    public void startIO() throws Exception {
        reader.start();
        writer.start();
    }

    @Override
    public BlockingQueue getInput() {
        return writer.getInQueue();
    }

    @Override
    public void setInput(BlockingQueue bq) {
        writer.setInQueue(bq);
    }

    @Override
    public BlockingQueue getOutput() {
        return reader.getOutQueue();
    }

    @Override
    public void setOutput(BlockingQueue bq) {
        reader.setOutQueue(bq);
    }

    @Override
    public String toString() {
        String socketStr = "no socket";
        if (socket != null) {
            socketStr = socket.getLocalPort() + "<->" + socket.getRemoteSocketAddress();
        }
        return getClass().getSimpleName() + "-" + tail(id) + "|" + socketStr + "|" + transport;
    }

    @Override
    public void close() throws Exception {
        if (!closed) {            
            closed = true;
            transport.discard(this);
            log.debug("Closing {}", this);
            reader.interrupt();
            writer.interrupt();
            try {
                socket.close();
            } catch (Exception e) {
                log.error("Exception closing socket " + socket, e);
            }
            
        } 

    }

}
