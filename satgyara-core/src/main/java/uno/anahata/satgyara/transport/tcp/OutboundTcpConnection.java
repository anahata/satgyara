/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.transport.tcp;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import uno.anahata.satgyara.peer.LocalPeer;
import uno.anahata.satgyara.peer.OutboundPeer;
import uno.anahata.satgyara.transport.handshake.ConnectionHandShake;
import uno.anahata.satgyara.transport.tcp.TcpSocketConfig;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class OutboundTcpConnection extends TcpConnection{

    protected SocketAddress remoteAddress;
    protected UUID forTransportID;
    
    public OutboundTcpConnection(OutboundPeer peer, SocketAddress remoteAddress, UUID forTransportId) throws SocketException, IOException {
        super.peer = peer;
        this.remoteAddress = remoteAddress;
        this.id = UUID.randomUUID();
        this.forTransportID = forTransportId;
    }
    
    
    @Override
    public void init() throws Exception {
        socket = new Socket();
        log.debug("Created local socket for outbound connection " + this);
        initSocket();
        log.debug("Connecting to remote host.... {} {}", socket, remoteAddress);
        socket.connect(remoteAddress);
        log.debug("CONNECTED to remote host.... {}" + socket);
        
        sendHandShake(forTransportID);
        ConnectionHandShake chs = readHandShake();        
        
        Validate.isTrue(chs.toPeerId.equals(LocalPeer.getInstance().getUuid()), "Received toPeerUUID doesn't match local peer received=%s local=%s", chs.fromPeerId, LocalPeer.getInstance().getUuid());
        Validate.isTrue(chs.fromConnectionId.equals(getId()), "Received fromConnectionId doesn't match local connection id received=%s local=%s", chs.fromConnectionId, id);
        if (peer.getUuid() == null) {
            peer.setUuid(chs.fromPeerId);
            log.debug("Got remote peer UUID{} ", peer.getUuid());
        } else {
            Validate.isTrue(chs.fromPeerId.equals(peer.getUuid()), "Received fromPeerUUID doesn't match connected peer UUID \n"
                    + "received=%s \n"
                    + "local   =%s", 
                    chs.fromPeerId, peer.getUuid());
        }
        
        super.initIO();
        
        if (forTransportID != null) {
            //register directly with transport 
            log.debug("forTransportId specified, registering {} as new for transport {}", this, peer.getTransports().get(forTransportID));
            peer.getTransports().get(forTransportID).addNew(this);
            log.debug("forTransportId specified, REGSITERED {} as new for transport {}", this, peer.getTransports().get(forTransportID));
        } else {
            log.debug("forTransportId not specified adding {} to idle pool {}", this, peer.getTcpConnectionPool());
            peer.getTcpConnectionPool().addNew(this);
            log.debug("forTransportId not specified ADDED {} to idle pool {}", this, peer.getTcpConnectionPool());
        }
        
        
        log.debug("init() complete for {} ", this);
        
    }
    
}
