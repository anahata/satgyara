/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.transport.packet.SerializedPacket;

/**
 *
 * @author priyadarshi
 */
public class UdpChunkTransmitter extends AbstractProcessorThread<SerializedPacket, FragmentedUdpPacket> {

    private UdpConnection conn;
    
    public UdpChunkTransmitter(UdpConnection conn) {
        super(0, 0);
        this.conn = conn;
        //inQueue = conn.getTransport().getSerializer().getOutQueue();
    }

    @Override
    public FragmentedUdpPacket process(SerializedPacket t) throws Exception {
        FragmentedUdpPacket fp = new FragmentedUdpPacket(conn.getRemoteId(), t, 1400);
        return fp;
    }

    @Override
    protected void doOutput(FragmentedUdpPacket o) throws Exception {
        DatagramSocket s = conn.getSocket();
        //System.out.println("Sending fragmented packet " + o.getChunks());
        for (UdpPacket chunk: o.getChunks()) {
            byte[] data = chunk.getData().array();
            DatagramPacket dp = new DatagramPacket(data, data.length, conn.getRemoteAddress());
            //System.out.println("Sending chunk " + chunk + " to " + conn.getRemoteAddress());
            s.send(dp);
            //System.out.println("SENT chunk " + chunk + " to " + conn.getRemoteAddress());
        }
    }
    
    
    
}
