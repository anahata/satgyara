/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.satgyara.transport.packet;

import java.util.Date;
import java.util.zip.Deflater;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.SerializationUtils;
import uno.anahata.satgyara.concurrent.AbstractProcessorThread;
import uno.anahata.satgyara.seq.LongSequence;

/**
 *
 * @author priyadarshi
 */
public class PacketSerializer<P extends Packet> extends AbstractProcessorThread<P, SerializedPacket> {

    private final PacketTransport transport;
    @Getter
    private LongSequence sequence = new LongSequence();
    @Getter
    @Setter
    private volatile int compressionLevel = Deflater.BEST_COMPRESSION;
    
    public PacketSerializer(PacketTransport transport) {
        super(1, 1);
        this.transport = transport;
        //this.transport = transport;
        setName(getClass().getSimpleName() + "|" + transport);
    }

    @Override
    protected P getInput() throws Exception {
        P p = super.getInput();         
        p.id = sequence.nextSeqNo();
        return p;
    }
    
    @Override
    public SerializedPacket process(P p) throws Exception {
        p.departureTime = new Date();
        p.preSerialize();
        byte[] compressed = CompressionUtils.compressSerialize(p, Deflater.DEFAULT_STRATEGY, p.getCompressionLevel());
        return new SerializedPacket(p, compressed);
    }

    public int getCompressionLevel() {
        return compressionLevel;
    }

    public void setCompressionLevel(int compressionLevel) {
        this.compressionLevel = compressionLevel;
    }
    
    

}
