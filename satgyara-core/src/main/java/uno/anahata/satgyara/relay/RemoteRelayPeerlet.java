/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.relay;

import java.util.Set;
import java.util.UUID;

/**
 *
 * @author priyadarshi
 */
public interface RemoteRelayPeerlet {
//    
////    /**
////     * Craetes a new Transport
////     * 
////     * @param transportId
////     * @return
////     * @throws Exception 
////     */
////    public void createTransport(UUID transportId) throws Exception ;
////    
//    /**
//     * Craetes a new tcp connection and allocates it to transport with UUID transportId
//     * 
//     * @param transportId
//     * @return
//     * @throws Exception 
//     */
//    public UUID createTcpConnection(UUID transportId) throws Exception ;
//    
//    public void allocateConnection(UUID connectionId, UUID transportID) throws Exception ;
    
    //public boolean canRelayTo(UUID peer);
    
    public void relayTo(UUID transportUUID, UUID targetUUID) throws Exception ;
    
    public void createRelayedPacketTransport(UUID transportUUID, UUID targetPeerUUID);
    
    public Set<UUID> getInboundPeers();
}
