/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.relay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peer.InboundPeer;
import uno.anahata.satgyara.transport.Transport;
import uno.anahata.satgyara.transport.AbstractConnection;
import uno.anahata.satgyara.transport.ConnectionPool;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class RelayTransport extends Transport {

    @Getter
    @Setter
    private RelayTransport target;

    /**
     * An object used to synchronize code when joining connections.
     */
    private final Object commonLock;

    private final List<AbstractConnection> pending = new ArrayList();

    private final Map<AbstractConnection, AbstractConnection> mappings = new HashMap();

    public RelayTransport(UUID id, ConnectionPool source, Object lock) {
        super(id, source);
        this.commonLock = lock;
    }

    public List<AbstractConnection> getUnmappedConnections() {
        ArrayList al = new ArrayList<>(connections);
        al.removeAll(mappings.keySet());
        return connections;
    }

    @Override
    public BlockingQueue getOutQueue() {
        throw new UnsupportedOperationException("No OutQueue on relayed transport.");
    }

    @Override
    public BlockingQueue getInQueue() {
        throw new UnsupportedOperationException("No InQueue on relayed transport.");
    }

    @Override
    protected void init(AbstractConnection connection) throws Exception {
        log.debug("initConnection {} {}.{}<-> {}.XXX ", getPeer(), connection, target.getPeer());
        log.debug("initConnection {} getting connection from target {}", connection, target);

        pending.add(connection);

        if (target.pending.isEmpty()) {
            log.debug("Target transport {} doesn't have any pending connections, requesting connectio via remote transport peerlet", target.getId());
            UUID connectionUUID = ((InboundPeer) target.source.getPeer()).getRemoteTransport().createConnection(target.getId());
            log.debug("remote transport peerlet returned {}", connectionUUID);
        }

        synchronized (commonLock) {

            if (!pending.contains(connection)) {
                log.debug("Connection mapped by the other end{}", connection);
                return;
            }

            AbstractConnection targetConnection = target.pending.get(0);

            log.debug("GOT {} ", targetConnection);

            connection.setOutput(new LinkedBlockingQueue(1));
            targetConnection.setInput(connection.getOutput());

            targetConnection.setOutput(new LinkedBlockingQueue(1));
            connection.setInput(targetConnection.getOutput());

            mappings.put(connection, targetConnection);
            target.mappings.put(targetConnection, connection);

            connection.startIO();
            targetConnection.startIO();

            log.debug("initConnection {} succeeded {}.{}<-> {}.{}", source.getPeer(), connection, target.getSource().getPeer(), targetConnection);

            pending.remove(connection);
            target.pending.remove(targetConnection);
        }

    }

}
