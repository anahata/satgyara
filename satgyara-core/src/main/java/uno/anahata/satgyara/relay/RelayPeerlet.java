/*
 * The MIT License
 *
 * Copyright 2020 Anahata.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uno.anahata.satgyara.relay;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.satgyara.peer.InboundPeer;
import uno.anahata.satgyara.peer.LocalPeer;
import uno.anahata.satgyara.peerlet.PeerletContext;
import uno.anahata.satgyara.relay.RelayTransport;
import uno.anahata.satgyara.transport.packet.PacketTransport;

/**
 *
 * @author priyadarshi
 */
@Slf4j
public class RelayPeerlet implements RemoteRelayPeerlet {

    @Override
    public Set<UUID> getInboundPeers() {

        //RemotePeer callerPeer = PeerletContext.getCaller();        
        return LocalPeer.getInstance().getInbound().stream().map(p -> p.getUuid()).collect(Collectors.toSet());
    }

    private boolean canRelayTo(UUID targetPeerUUID) {
        InboundPeer targetPeer = (InboundPeer) LocalPeer.getInstance().getInbound(targetPeerUUID);
        
        return targetPeer != null;
    }

    @Override
    public void createRelayedPacketTransport(UUID transportUUID, UUID targetPeerUUID) {
        
        InboundPeer callerPeer = (InboundPeer) PeerletContext.getCaller();
        InboundPeer targetPeer = (InboundPeer) LocalPeer.getInstance().getInbound(targetPeerUUID);
            
        if (canRelayTo(targetPeerUUID)) {
            createRelayTransport(transportUUID, callerPeer, targetPeer);
            targetPeer.getRemoteTransport().createPacketTransportForRelayedPeer(callerPeer.getUuid(), transportUUID);
        }
        
    }

    @Override
    public void relayTo(UUID transportUUID, UUID targetPeerUUID) throws Exception {

        InboundPeer callerPeer = (InboundPeer) PeerletContext.getCaller();
        InboundPeer targetPeer = (InboundPeer) LocalPeer.getInstance().getInbound(targetPeerUUID);
            
        if (canRelayTo(targetPeerUUID)) {

            createRelayTransport(transportUUID, callerPeer, targetPeer);

            targetPeer.getRemoteTransport().createRelayedPeer(callerPeer.getUuid(), transportUUID);

        } else {
            throw new IllegalStateException("Cannot relay to " + targetPeerUUID);
        }
    }
    
    private void createRelayTransport(UUID transportUUID, InboundPeer callerPeer, InboundPeer targetPeer) {
        

        log.debug("Creating relay transport with UUID {} between {}<->{} ", transportUUID, callerPeer, targetPeer);
        //creating a realy transport on the caller peer with the 

        Object commonLock = new Object();
        RelayTransport callerRt = new RelayTransport(transportUUID, callerPeer.getTcpConnectionPool(), commonLock);
        RelayTransport targetRt = new RelayTransport(transportUUID, targetPeer.getTcpConnectionPool(), commonLock);
        callerRt.setTarget(targetRt);
        targetRt.setTarget(callerRt);

        log.debug("Relay transports created with UUID {} with {}<->{} ", transportUUID, callerRt, targetRt);

    }

//    @Override
//    public void allocateConnection(UUID connectionId, UUID toTransportId) throws Exception {
//        RemotePeer rp = PeerletContext.getCaller();
//        Transport t = rp.getTransports().get(toTransportId);
//        log.debug("Allocating idle connection {} with peer {} to transport {} ", connectionId, rp, t);
//        t.addFromPool(connectionId);
//        log.debug("ALLOCATED idle connection {} with peer {} to transport {} ", connectionId, rp, t);
//    }
//
//    @Override
//    public UUID createTcpConnection(UUID transportId) throws Exception {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
